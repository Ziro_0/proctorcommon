/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  NetSocketDataTypes

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.data {

  public final class NetSocketDataTypes {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //-----------------------------------------------------------------
    //Messages of these types are dispatched from server to client.
    //-----------------------------------------------------------------
    
    /**
    *During handshake, server is requesting client's user data.
    */
    public static const SERVER_REQUESTS_USER_DATA:uint = 1;
    
    /**
    *Handshake between server and client is complete.
    */
    public static const HANDSHAKE_COMPLETE:uint = 2;
    
    /**
    *Server's state has changed.
    */
    public static const STATE:uint = 3;
    
    //-----------------------------------------------------------------
    //Messages of these types are dispatched from client to server.
    //-----------------------------------------------------------------
    
    /**
    *The client is sending its user data.
    */
    public static const CLIENT_SENDS_USER_DATA:uint = 100;
    
    //-----------------------------------------------------------------
    //Messages of these types are dispatched by both client and server.
    //-----------------------------------------------------------------
    
    /**
    *A message of specific instruction. Messages are originally sent to the server from a client.
    * The server processes the message, then may forward the massage to all clients.
    */
    public static const MESSAGE:uint = 200;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}