/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  NetRegisterClassAliases

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.utils {
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.UserData;
  import engine.common.utils.themeImagesDataLoader.ThemeImagesData;
  import flash.net.registerClassAlias;
  import flib.utils.FDataBlock;
  import net.data.NetClientData;
  import net.data.NetSocketData;

  public final class NetRegisterClassAliases {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // Run
    //==================================================================
    public static function Run():void {
      registerClassAlias("NetClientData", NetClientData);
      registerClassAlias("NetSocketData", NetSocketData);
      registerClassAlias("UserData", UserData);
      registerClassAlias("FDataBlock", FDataBlock);
      registerClassAlias("PlayerGameProgress", PlayerGameProgress);
      registerClassAlias("ThemeImagesData", ThemeImagesData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}