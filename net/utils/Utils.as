/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  Utils

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-27-2017
  ActionScript:	3.0
  Description:	
  History:
    05-27-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.utils {
  import flash.events.Event;
  import flash.text.TextField;
  import flash.utils.ByteArray;

  public final class Utils {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // ObjectToString
    //==================================================================
    public static function ObjectToString(o_data:Object, s_headerMessage:String = ""):String {
      var s_data:String;
      
      if (isPrimitiveType(o_data)) {
        s_data = o_data.toString();
      } else {
        if (o_data is ByteArray) {
          s_data = "[ByteArray]";
        } else {
          s_data = ToKeyPairValuesString(o_data);
        }
      }
      
      return(s_headerMessage + s_data);
    }
    
    //==================================================================
    // ToKeyPairValuesString
    //==================================================================
    public static function ToKeyPairValuesString(o_data:Object):String {
      var s_data:String = "";
      var b_openedBrace:Boolean;
      
      for (var s_key:String in o_data) {
        if (s_data.length == 0) {
          s_data += "{";
          b_openedBrace = true;
        } else {
          s_data += ", ";
        }
        
        s_data += s_key + "=" + ObjectToString(o_data[s_key]);
      }
      
      if (b_openedBrace) {
        s_data += "}";
      }
      
      if (s_data.length == 0) {
        if (o_data != null) {
          s_data += String(o_data);
        }
      }
      
      return(s_data);
    }
    
    //==================================================================
    // TraceAndLog
    //==================================================================
    public static function TraceAndLog(s_message:String, o_textField:TextField = null,
    b_useDateTime:Boolean = true):void {
      var s_output:String = buildLogMessage(s_message, b_useDateTime);
      trace(s_output);
      logToTextField(o_textField, s_output);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // buildLogMessage
    //==================================================================
    private static function buildLogMessage(s_message:String, b_useDateTime:Boolean):String {
      var s_dateTime:String;
      
      if (b_useDateTime) {
        var o_date:Date = new Date();
        s_dateTime = o_date.toLocaleString() + " ";
      } else {
        s_dateTime = "";
      }
      
      return(s_dateTime + s_message);
    }
    
    //==================================================================
    // isPrimitiveType
    //==================================================================
    private static function isPrimitiveType(o_data:Object):Boolean {
      return(
        o_data is Number ||
        o_data is String ||
        o_data is Boolean
      );
    }
    
    //==================================================================
    // logToTextField
    //==================================================================
    private static function logToTextField(o_textField:TextField, s_output:String):void {
      if (o_textField.length == 0) {
        o_textField.text = s_output;
      } else {
        o_textField.appendText("\n" + s_output);
      }
      
      o_textField.dispatchEvent(new Event(Event.CHANGE));
    }
  }
}