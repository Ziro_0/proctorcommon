/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  NetClientSocket

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.client {
  import engine.common.data.UserData;
  import flash.errors.EOFError;
  import flash.events.ErrorEvent;
  import flash.events.Event;
  import flash.events.EventDispatcher;
  import flash.events.IEventDispatcher;
  import flash.events.IOErrorEvent;
  import flash.events.ProgressEvent;
  import flash.events.SecurityErrorEvent;
  import flash.events.TimerEvent;
  import flash.net.Socket;
  import flash.utils.ByteArray;
  import flash.utils.Timer;
  import net.data.NetClientData;
  import net.data.NetHandshakes;
  import net.data.NetSocketData;
  import net.data.NetSocketDataTypes;
  import net.data.NetSocketMessageTypes;
  import net.events.NetClientSocketEvent;
  import net.utils.NetRegisterClassAliases;
  import net.utils.Utils;
  
  /**
  * Dispatched when the socket connection is closed by the server.
  * 
  * @eventType flash.events.Event.CLOSE
  */
  [Event(name="close", type="flash.events.Event")]
  
  /**
  * Dispatched after the socket has successfully connected to the server socked and then successfully completed its handshake with the server.
  * 
  * @eventType flash.events.Event.CONNECT
  */
  [Event(name="connect", type="flash.events.Event")]
  
  /**
  * Dispatched when an error occurred with the socket.
  * 
  * @eventType flash.events.ErrorEvent.ERROR
  */ 
  [Event(name="error", type="flash.events.ErrorEvent")]
  
  /**
  * Dispatched when the socket is attempting to connect to the server.
  * This event is also dispatched on retry attempts.
  * 
  * @eventType flash.events.Event.PREPARING
  */ 
  [Event(name="preparing", type="flash.events.Event")]
  
  /**
  * Dispatched when the socket receives new state data from the server.
  * 
  * @eventType net.events.NetClientSocketEvent.STATE
  */ 
  [Event(name = "state", type = "net.events.NetClientSocketEvent")]
  
  /**
  * Dispatched when the socket receives new message data from the server.
  * 
  * @eventType net.events.NetClientSocketEvent.MESSAGE
  */ 
  [Event(name = "message", type = "net.events.NetClientSocketEvent")]
  
  /**
  * Dispatched once per second during each second of a retry attempt. The message property of the
  * NetSocketData contains the number of seconds remaining until the next retry attempt.
  * 
  * @eventType net.events.NetClientSocketEvent.RETRY_TIMER
  */ 
  [Event(name = "retryTimer", type = "net.events.NetClientSocketEvent")]
  
  
  public class NetClientSocket extends EventDispatcher {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const RETRY_CONNECT_DELAY_DEFAULT_SECONDS:uint = 5;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_clientData:NetClientData;
    
    private var mo_socket:Socket;
    private var mo_socketMessageBytes:ByteArray;
    
    private var mo_retryConnectTimer:Timer;
    
    private var ms_userId:String;
    private var ms_host:String;
    private var mi_port:int;
    private var mu_messageLength:uint;
    private var mu_retryConnectDelaySeconds:uint;
    private var mb_isConnected:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function NetClientSocket(o_target:IEventDispatcher = null):void {
      super(o_target);
      
      //register some data classes for AMF encoding
      NetRegisterClassAliases.Run();
      
      mo_socketMessageBytes = new ByteArray();
      
      retryConnectDelaySeconds = RETRY_CONNECT_DELAY_DEFAULT_SECONDS;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clientData (get)
    //==================================================================
    /**
     * Returns a copy of the client data If the socket has not been supplied with any data, null is returned.
     * @return A clone of the socket's NetClientData object.
    */
    public function get clientData():NetClientData {
      return(mo_clientData ? mo_clientData.clone() : null);
    }
    
    //==================================================================
    // close
    //==================================================================
    /**
     * Closes the Socket.
    */
    public function close():void {
      if (!mo_socket) {
        return;
      }
      
      try {
        mo_socket.close();
      } catch (o_error:Error) {
      }
      
      mb_isConnected = false;
    }
    
    //==================================================================
    // connect
    //==================================================================
    /**
     * Creates a Socket and attempts the establish a connection with server at the specified host and port.
     * If the connection attempt succeeds, a series of initial communications between the server and take place. The client will eventually dispatch <code>Event.CONNECT</code>.
     * If the connection attempt failed, this object will make another attempt after the retry time has passed.
     * If some other error occurred, <code>ErrorEvent.ERROR</code> is dispatched.
    */
    public function connect(s_host:String, i_port:int, o_data:NetClientData):void {
      if (mo_socket) {
        return; //already connected
      }
      
      //create socket and attach event listeners
      mo_socket = new Socket(); 
      mo_socket.addEventListener(Event.CONNECT, onSockectConnect, false, 0, true);
      mo_socket.addEventListener(Event.CLOSE, onSocketClose, false, 0, true);
      mo_socket.addEventListener(IOErrorEvent.IO_ERROR, onSocketError, false, 0, true);
      mo_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocketError, false, 0, true);
      mo_socket.addEventListener(ProgressEvent.SOCKET_DATA, onSocketData, false, 0, true);
      
      ms_host = s_host;
      mi_port = i_port;
      mo_clientData = o_data;
      
      if (!connectToServer()) {
        initRetryConnectTimer();
      }
    }
    
    //==================================================================
    // isConnected (get)
    //==================================================================
    /**
     * Returns true if the Socket is connected to the server.
    */
    public function get isConnected():Boolean {
      return(mo_socket && mo_socket.connected);
    }
    
    //==================================================================
    // retryConnectDelaySeconds (get)
    //==================================================================
    /**
     * Returns the current time, the socket will wait before retrying after failed connection
     * attempt.
     * @return Time, in seconds.
    */
    public function get retryConnectDelaySeconds():uint {
      return(mu_retryConnectDelaySeconds);
    }
    
    //==================================================================
    // retryConnectDelaySeconds (set)
    //==================================================================
    /**
     * Sets the current time, the socket will wait before retrying after failed connection attempt.
     * @param u_value (uint) The time, in seconds. If zero is specified, no retry attempt is made.
    */
    public function set retryConnectDelaySeconds(u_value:uint):void {
      mu_retryConnectDelaySeconds = u_value;
    }
    
    //==================================================================
    // sendMessageToServer
    //==================================================================
    /**
     * Sends a message from the client to the server.
     * @param o_stateData (Object) The message data to send.
     * @return True if the request was sent to the server, or false if the socket has not been initialized, there was an error sending data to the server, or no data was specified.
    */
    public function sendMessageToServer(o_message:Object):Boolean {
      if (!mo_socket) {
        trace("NetClientSocket.sendMessageToServer. Warning: socket not initialized.");
        return(false);
      }
      
      if (o_message == null) {
        trace("NetClientSocket.sendMessageToServer. No state data specified.");
        return(false);
      }
      
      var o_sendData:NetSocketData = new NetSocketData(NetSocketDataTypes.MESSAGE, mo_clientData, o_message);
      return(writeSocketData(o_sendData));
    }
    
    //==================================================================
    // sendUserData
    //==================================================================
    /**
     * Send a client's user data to the server.
     * @param o_userData (UserData) The new user data to send.
     * @return True if the request was sent to the server, or false if the socket has not been initialized, there was an error sending data to the server, or no client data was specified.
    */
    public function sendUserData(o_userData:UserData):Boolean {
      if (!mo_socket) {
        trace("NetClientSocket.sendClientData. Warning: socket not initialized.");
        return(false);
      }
      
      if (!o_userData) {
        trace("NetClientSocket.sendClientData. No client data specified.");
        return(false);
      }
      
      return(sendUserDataInner(o_userData));
    }
    
    //==================================================================
    // uninit
    //==================================================================
    /**
     * Uninitializes and closes the socket.
    */
    public function uninit():void {
      uninitSocket();
      uninitRetryConnectTimer();
    }
    
    //==================================================================
    // userId (get)
    //==================================================================
    /**
     * Returns the user ID established by the server when the client connects.
    */
    public function get userId():String {
      return(ms_userId);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // connectToServer
    //==================================================================
    private function connectToServer():Boolean {
      try {
        mo_socket.connect(ms_host, mi_port);
      } catch(o_error:Error) {
        trace("NetClientSocket.connectToServer. Error: " + o_error.message);
        uninitSocket();
        dispatchErrorNotification(o_error.message);
        return(false);
      }
      
      trace("NetClientSocket.connectToServer. Attempting to connect with host " +
        ms_host + " using port " + mi_port + ".");
      dispatchEvent(new Event(Event.PREPARING));
      return(true);
    }
    
    //==================================================================
    // dispatchErrorNotification
    //==================================================================
    private function dispatchErrorNotification(s_error:String):void {
      dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, s_error));
    }
    
    //==================================================================
    // handleSocketDataMessage
    //==================================================================
    private function handleSocketDataMessage():void {
      var o_data:NetSocketData;
      try {
        o_data = mo_socketMessageBytes.readObject() as NetSocketData;
      } catch (o_error:EOFError) {
        trace("NetClientSocket.handleSocketDataMessage. Error: " + o_error);
        return;
      }
      
      if (!o_data) {
        trace("NetClientSocket.handleSocketDataMessage. Warning: Expected NetSocketData to be read from socket." );
        return;
      }
      
      switch (o_data.type) {
        case NetSocketDataTypes.HANDSHAKE_COMPLETE:
          trace("NetClientSocket.handleSocketDataMessage. Type: HANDSHAKE_COMPLETE. " +
            Utils.ObjectToString(o_data.clientData.userData));
          processHandshakeComplete(o_data);
          break;
          
        case NetSocketDataTypes.SERVER_REQUESTS_USER_DATA:
          trace("NetClientSocket.handleSocketDataMessage. Type: SERVER_REQUESTS_USER_DATA. " +
            Utils.ObjectToString(o_data.message));
          processRequestUserData();
          break;
          
        case NetSocketDataTypes.STATE:
          trace("NetClientSocket.handleSocketDataMessage. Type: STATE " +
            Utils.ObjectToString(o_data.clientData.state));
          processState(o_data);
          break;
          
        case NetSocketDataTypes.MESSAGE:
          trace("NetClientSocket.handleSocketDataMessage. Type: MESSAGE " +
            Utils.ObjectToString(o_data.message));
          processMessage(o_data);
          break;
          
        default:
          trace("NetClientSocket.handleSocketDataMessage. Unknown type: " + o_data.type);
          break;
      }
    }
    
    //==================================================================
    // initRetryConnectTimer
    //==================================================================
    private function initRetryConnectTimer():void {
      if (mu_retryConnectDelaySeconds == 0 || mo_retryConnectTimer) {
        return;
      }
      
      const TIME_ONE_SECOND_MS:uint = 1000;
      mo_retryConnectTimer = new Timer(TIME_ONE_SECOND_MS, mu_retryConnectDelaySeconds);
      mo_retryConnectTimer.addEventListener(TimerEvent.TIMER, onRetryConnectTimer, false, 0, true);
      mo_retryConnectTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onRetryConnectTimerComplete,
        false, 0, true);
      mo_retryConnectTimer.start();
      
      notifyRetryTimer();
    }
    
    //==================================================================
    // notifyRetryTimer
    //==================================================================
    private function notifyRetryTimer():void {
      var o_data:NetSocketData = new NetSocketData();
      o_data.message = mo_retryConnectTimer.repeatCount - mo_retryConnectTimer.currentCount;
      dispatchEvent(new NetClientSocketEvent(NetClientSocketEvent.RETRY_TIMER, false, false, o_data));
    }
    
    //==================================================================
    // onRetryConnectTimer
    //==================================================================
    private function onRetryConnectTimer(o_event:TimerEvent):void {
      notifyRetryTimer();
    }
    
    //==================================================================
    // onRetryConnectTimerComplete
    //==================================================================
    private function onRetryConnectTimerComplete(o_event:TimerEvent):void {
      uninitRetryConnectTimer();
      connectToServer();
    }
    
    //==================================================================
    // onSocketClose
    //==================================================================
    private function onSocketClose(o_event:Event):void {
      trace("NetClientSocket.onSocketClose");
      mb_isConnected = false;
      dispatchEvent(new Event(Event.CLOSE));
    }
    
    //==================================================================
    // onSockectConnect
    //==================================================================
    private function onSockectConnect(o_event:Event):void {
      trace("NetClientSocket.onSockectConnect. Connected with host " + ms_host + " using port " + mi_port + ".");
      trace("NetClientSocket.onSockectConnect. Attempting handshake with host.");
      
      uninitRetryConnectTimer();
      
      if (!writeString(NetHandshakes.REQUEST_HANDSHAKE)) {
        uninitSocket();
        dispatchErrorNotification("NetClientSocket.onSockectConnect. Error: Unable to initiate handshake.");
        return;
      }
      
      mb_isConnected = true;
    }
    
    //==================================================================
    // onSocketData
    //==================================================================
    private function onSocketData(o_event:ProgressEvent):void {
      readSocketData();
    }
    
    //==================================================================
    // onSocketError
    //==================================================================
    private function onSocketError(o_event:ErrorEvent):void {
      trace("NetClientSocket.onSocketError. " + o_event);
      
      dispatchErrorNotification(o_event.text);
      
      if (!mb_isConnected) {
        initRetryConnectTimer();
      }
    }
    
    //==================================================================
    // processHandshakeComplete
    //==================================================================
    private function processHandshakeComplete(o_data:NetSocketData):void {
      ms_userId = o_data.clientData.userData.id;
      mo_clientData.userData.copyFrom(o_data.clientData.userData);
      dispatchEvent(new Event(Event.CONNECT));
    }
    
    //==================================================================
    // processMessage
    //==================================================================
    private function processMessage(o_data:NetSocketData):void {
      dispatchEvent(new NetClientSocketEvent(NetClientSocketEvent.MESSAGE, false, false, o_data));
    }
    
    //==================================================================
    // processRequestUserData
    //==================================================================
    private function processRequestUserData(o_data:NetSocketData = null):void {
      var o_userData:UserData = o_data ? o_data.clientData.userData : mo_clientData.userData;
      sendUserDataInner(o_userData);
    }
    
    //==================================================================
    // processSocketData
    //==================================================================
    private function processSocketData(u_messageType:uint):Boolean {
      switch (u_messageType) {
        case NetSocketMessageTypes.SOCKET_DATA:
          handleSocketDataMessage();
          break;
          
        default:
          trace("NetClientSocket.processSocketData. Warning: Unknown message type: " + u_messageType);
          return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // processState
    //==================================================================
    private function processState(o_data:NetSocketData):void {
      dispatchEvent(new NetClientSocketEvent(NetClientSocketEvent.STATE, false, false, o_data))
    }
    
    //==================================================================
    // readSocketData
    //==================================================================
    private function readSocketData():void {
      var o_data:NetSocketData;
      
      while (mo_socket.bytesAvailable >= 4) {
        //while there is at least enough data to read the message size header
        
        if (mu_messageLength == 0) {
          //this is the start of a new message block, so read the message length header
          mu_messageLength = mo_socket.readUnsignedInt();
        }
        
        //is there a full message in the socket?
        if (mu_messageLength <= mo_socket.bytesAvailable) {
          //read the message type
          mo_socket.readBytes(mo_socketMessageBytes, 0, mu_messageLength);
          
          //process the message based on the message type
          var u_messageType:uint = mo_socketMessageBytes.readUnsignedInt();
          var b_wasMessageProcessed:Boolean = processSocketData(u_messageType);
          
          if (!b_wasMessageProcessed) {
            trace("NetClientSocket.readSocketData. Warning: Unsupported message type: " + u_messageType);
          }
          
          mu_messageLength = 0; //finished reading this message
          mo_socketMessageBytes.clear(); //clear the buffer for the next message
        } else {
          //The current message isn't complete -- wait for the socketData event and try again
          trace("NetClientSocket.readSocketData. Partial message. " +
            "Bytes available in socket: " + mo_socket.bytesAvailable +
            ", message length: " + mu_messageLength + ".");
          break;
        }
      }
    }
    
    //==================================================================
    // sendUserDataInner
    //==================================================================
    private function sendUserDataInner(o_data:UserData):Boolean {
      var o_sendData:NetSocketData = new NetSocketData(NetSocketDataTypes.CLIENT_SENDS_USER_DATA);
      o_sendData.clientData.userData.copyFrom(o_data);
      return(writeSocketData(o_sendData));
    }
    
    //==================================================================
    // uninitRetryConnectTimer
    //==================================================================
    private function uninitRetryConnectTimer():void {
      if (mo_retryConnectTimer) {
        mo_retryConnectTimer.stop();
        mo_retryConnectTimer.removeEventListener(TimerEvent.TIMER, onRetryConnectTimer);
        mo_retryConnectTimer = null;
      }
    }
    
    //==================================================================
    // uninitSocket
    //==================================================================
    private function uninitSocket():void {
      if (!mo_socket) {
        return;
      }
      
      mo_socket.removeEventListener(Event.CONNECT, onSockectConnect);
      mo_socket.removeEventListener(Event.CLOSE, onSocketClose);
      mo_socket.removeEventListener(IOErrorEvent.IO_ERROR, onSocketError);
      mo_socket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocketError);
      mo_socket.removeEventListener(ProgressEvent.SOCKET_DATA, onSocketData);
      
      close();
      
      mo_socket = null;
    }
    
    //==================================================================
    // writeSocketData
    //==================================================================
    private function writeSocketData(o_data:NetSocketData):Boolean {
      try {
        mo_socket.writeObject(o_data);
        mo_socket.flush();
      } catch (o_error:Error) {
        trace("NetClientSocket.writeSocketData. Error: " + o_error.message);
        dispatchErrorNotification(o_error.message);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // writeString
    //==================================================================
    private function writeString(s_string:String, b_flush:Boolean = true):Boolean {
      try {
        mo_socket.writeUTFBytes(s_string);
        mo_socket.flush();
        
        if (b_flush) {
          mo_socket.flush();
        }
      } catch (o_error:Error) {
        trace("NetClientSocket.writeString. " + o_error);
        return(false);
      }
      
      return(true);
    }
  }
}