/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_NetServerCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-22-2017
  ActionScript:	3.0
  Description:	
  History:
    05-22-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.server {
  import net.data.NetClientData;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_NetServerCallbacks {
    function netServerOnClientConnect(o_netServer:NetServer, s_clientId:String):void;
    function netServerOnClientClose(o_netServer:NetServer, s_clientId:String):void;
    function netServerOnClientMessage(o_netServer:NetServer, s_clientId:String, o_message:Object):void;
    function netServerOnClientState(o_netServer:NetServer, s_clientId:String, o_clientData:NetClientData):void;
  }
}