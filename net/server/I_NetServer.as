/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_NetServer

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-22-2017
  ActionScript:	3.0
  Description:	
  History:
    05-22-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.server {
  import net.data.NetSocketData;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_NetServer {
    function clientSocketOnClientMessage(o_clientSocketService:NetClientSocketService, o_data:NetSocketData):void;
    function clientSocketOnClientState(o_clientSocketService:NetClientSocketService, o_data:NetSocketData):void;
    function clientSocketOnConnectionComplete(o_clientSocketService:NetClientSocketService):void;
    function getClientDataIds(vs_clientIds_out:Vector.<String> = null):Vector.<String>;
  }
}