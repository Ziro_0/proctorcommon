/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  NetClientSocketService

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.server {
  import flash.events.ErrorEvent;
  import flash.events.Event;
  import flash.events.EventDispatcher;
  import flash.events.IEventDispatcher;
  import flash.events.IOErrorEvent;
  import flash.events.ProgressEvent;
  import flash.events.SecurityErrorEvent;
  import flash.net.Socket;
  import flash.text.TextField;
  import flash.utils.ByteArray;
  import net.data.NetClientData;
  import net.data.NetConnectData;
  import net.data.NetHandshakes;
  import net.data.NetSocketData;
  import net.data.NetSocketDataTypes;
  import net.data.NetSocketMessageTypes;
  import net.events.NetClientSocketEvent;
  import net.utils.Utils;

  [Event(name="close", type="flash.events.Event")]
  [Event(name="ioError", type="flash.events.IOErrorEvent")]
  [Event(name="securityError", type="flash.events.SecurityErrorEvent")]
  [Event(name="state", type="net.events.NetClientSocketEvent")]
  
  internal class NetClientSocketService extends EventDispatcher {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_server:I_NetServer;
    private var mo_clientData:NetClientData;
    
    private var mo_clientSocket:Socket;
    
    private var mo_txfLog:TextField;
    
    private var ms_policyResponse:String;
    private var ms_id:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function NetClientSocketService(o_server:I_NetServer, o_clientSocket:Socket,
    s_policyResponse:String, o_target:IEventDispatcher = null):void {
      super(o_target);
      
      mo_server = o_server;
      
      ms_id = createClientId();
      
      mo_clientSocket = o_clientSocket;
      mo_clientSocket.addEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake,
        false, 0, true);
      mo_clientSocket.addEventListener(Event.CLOSE, onClientSocketClose, false, 0, true);
      mo_clientSocket.addEventListener(IOErrorEvent.IO_ERROR, onClientSocketError, false, 0, true);
      mo_clientSocket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onClientSocketError,
        false, 0, true);
        
      ms_policyResponse = s_policyResponse;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clientData (get)
    //==================================================================
    public function get clientData():NetClientData {
      return(mo_clientData);
    }
    
    //==================================================================
    // disconnect
    //==================================================================
    public function disconnect():void {
      if (!mo_clientSocket) {
        return;
      }
      
      //close socket
      if (mo_clientSocket.connected) {
        try {
          mo_clientSocket.close();
        } catch (o_error:Error) {
          trace("NetClientSocketService.disconnect. " + o_error.message);
        }
      }
      
      //remove all event handlers
      mo_clientSocket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake);
      mo_clientSocket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake2);
      mo_clientSocket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData);
      mo_clientSocket.removeEventListener(Event.CLOSE, onClientSocketClose);
      mo_clientSocket.removeEventListener(IOErrorEvent.IO_ERROR, onClientSocketError);
      mo_clientSocket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onClientSocketError);
      mo_clientSocket = null;
      
      mo_server = null;
    }
    
    //==================================================================
    // flush
    //==================================================================
    public function flush():Boolean {
      if (!mo_clientSocket) {
        return(false);
      }
      
      try {
        mo_clientSocket.flush();
      } catch (o_error:Error) {
        trace("NetClientSocketService.flush. Error: " + o_error);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // hasSocket
    //==================================================================
    public function hasSocket(o_socket:Socket):Boolean {
      return(o_socket && o_socket == mo_clientSocket);
    }
    
    //==================================================================
    // id (get)
    //==================================================================
    public function get id():String {
      return(ms_id);
    }
    
    //==================================================================
    // localAddress (get)
    //==================================================================
    public function get localAddress():String {
      return(mo_clientSocket ? mo_clientSocket.localAddress : "");
    }
    
    //==================================================================
    // localPort (get)
    //==================================================================
    public function get localPort():int {
      return(mo_clientSocket ? mo_clientSocket.localPort : 0);
    }
    
    //==================================================================
    // logTextField (set)
    //==================================================================
    public function set logTextField(o_value:TextField):void {
      mo_txfLog = o_value;
    }
    
    //==================================================================
    // sendMessageToClient
    //==================================================================
    public function sendMessageToClient(o_message:Object):Boolean {
      var o_data:NetSocketData = createNetSocketData(NetSocketDataTypes.MESSAGE, o_message);
      return(writeSocketData(o_data));
    }
    
    //==================================================================
    // sendStateToClient
    //==================================================================
    public function sendStateToClient(o_state:Object):Boolean {
      var o_data:NetSocketData = createNetSocketData(NetSocketDataTypes.STATE);
      o_data.clientData.state = o_state;
      return(writeSocketData(o_data));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // buildPolicyResponse
    //==================================================================
    private function buildPolicyResponse():String {
      var s_policy:String = ms_policyResponse || NetConnectData.DEFAULT_CROSS_DOMAIN_POLICY;
      s_policy = s_policy.replace(/{port}/g, localPort);
      return(s_policy);
    }
    
    //==================================================================
    // createClientId
    //==================================================================
    private static function createClientId():String {
      return(new Date().time.toString());
    }
    
    //==================================================================
    // createNetSocketData
    //==================================================================
    private function createNetSocketData(u_type:uint, o_message:Object = null):NetSocketData {
      return(new NetSocketData(u_type, mo_clientData, o_message));
    }
    
    //==================================================================
    // handleHandshakePolicyRequest
    //==================================================================
    private function handleHandshakePolicyRequest(o_socket:Socket):void {
      var s_policy:String = buildPolicyResponse();
      trace("NetClientSocketService.handleHandshakePolicyRequest. Sending policy: " + s_policy);
      writeString(s_policy);
      o_socket.close();
    }
    
    //==================================================================
    // handleHandshakeRequest
    //==================================================================
    private function handleHandshakeRequest():void {
      //remove client socket data handler for initial handshake, and add client data handler for
      // handling data sent during phase 2 of handshake
      mo_clientSocket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake);
      mo_clientSocket.addEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake2,
        false, 0, true);
      var o_data:NetSocketData = createNetSocketData(NetSocketDataTypes.SERVER_REQUESTS_USER_DATA);
      writeSocketData(o_data);
    }
    
    //==================================================================
    // handleReceivedClientMessage
    //==================================================================
    private function handleReceivedClientMessage(o_data:NetSocketData):void {
      trace("NetClientSocketService.handleReceivedClientMessage. Data: " + Utils.ObjectToString(o_data));
      if (mo_server) {
        mo_server.clientSocketOnClientMessage(this, o_data);
      }
    }
    
    //==================================================================
    // handleReceivedClientState
    //==================================================================
    private function handleReceivedClientState(o_data:NetSocketData):void {
      trace("NetClientSocketService.handleReceivedClientState. Data: " + Utils.ObjectToString(o_data));
      if (mo_server) {
        mo_server.clientSocketOnClientState(this, o_data);
      }
    }
    
    //==================================================================
    // handleSendUserData
    //==================================================================
    private function handleSendUserData(o_data:NetSocketData):void {
      trace("NetClientSocketService.handleSendUserData. New client data: " + o_data.clientData);
      trace("NetClientSocketService.handleSendUserData. Current client data: " + mo_clientData);
      mo_clientData = o_data.clientData;
    }
    
    //==================================================================
    // handleSendUserDataOnHandshake
    //==================================================================
    private function handleSendUserDataOnHandshake(o_data:NetSocketData):void {
      trace("NetClientSocketService.handleSendUserDataOnHandshake. New client data: " + o_data.clientData);
      
      //remove client socket data handler for phase 2 of handshake, and add client data handler for
      // normal socket data transmissions
      mo_clientSocket.removeEventListener(ProgressEvent.SOCKET_DATA, onClientSocketDataHandshake2);
      mo_clientSocket.addEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData, false, 0, true);
      
      mo_clientData = o_data.clientData;
      mo_clientData.userData.id = ms_id;
      
      trace("NetClientSocketService.handleSendUserDataOnHandshake. New client id: " +
        mo_clientData.userData.id);
      
      var o_completeData:NetSocketData = createNetSocketData(NetSocketDataTypes.HANDSHAKE_COMPLETE);
      writeSocketData(o_completeData);
      
      notifyConnectionComplete();
    }
    
    //==================================================================
    // onClientSocketClose
    //==================================================================
    private function onClientSocketClose(o_event:Event):void {
      trace("NetClientSocketService.onClientSocketClose. Connection to client " +
        mo_clientSocket.localAddress + ":" + mo_clientSocket.localPort + " closed." +
        " client data: " + mo_clientData);
      dispatchEvent(o_event);
    }
    
    //==================================================================
    // onClientSocketData
    //==================================================================
    private function onClientSocketData(o_event:ProgressEvent):void {
      //data received from a client socket
      
      var o_data:NetSocketData = readSocketData();
      if (!o_data) {
        trace("NetClientSocketService.onClientSocketData. No valid socket data read.");
        return;
      }
      
      switch (o_data.type) {
        case NetSocketDataTypes.CLIENT_SENDS_USER_DATA:
          handleSendUserData(o_data);
          break;
          
        case NetSocketDataTypes.STATE:
          handleReceivedClientState(o_data);
          break;
        
        case NetSocketDataTypes.MESSAGE:
          handleReceivedClientMessage(o_data);
          break;
        
        default:
          traceAndLog("NetClientSocketService.onClientSocketData. Unknown type: " + o_data.type);
          break;
      }
    }
    
    //==================================================================
    // onClientSocketDataHandshake
    //==================================================================
    private function onClientSocketDataHandshake(o_event:ProgressEvent):void {
      var s_message:String = mo_clientSocket.readUTFBytes(mo_clientSocket.bytesAvailable);
      
      trace("NetClientSocketService.onClientSocketDataHandshake. Message: " + s_message);
      
      switch (s_message) {
        case NetHandshakes.POLICY_REQUEST:
          handleHandshakePolicyRequest(o_event.target as Socket);
          break;
          
        case NetHandshakes.REQUEST_HANDSHAKE:
          handleHandshakeRequest();
          break;
          
        default:
          traceAndLog("NetClientSocketService.onClientSocketDataHandshake. Warning: unknown message.");
          break;
      }
    }
    
    //==================================================================
    // onClientSocketDataHandshake2
    //==================================================================
    private function onClientSocketDataHandshake2(o_event:ProgressEvent):void {
      //data received from a client socket during handshake
      
      var o_data:NetSocketData = readSocketData();
      if (!o_data) {
        traceAndLog("NetClientSocketService.onClientSocketDataHandshake2. No valid socket data read.");
        return;
      }
      
      switch (o_data.type) {
        case NetSocketDataTypes.CLIENT_SENDS_USER_DATA:
          handleSendUserDataOnHandshake(o_data);
          break;
        
        default:
          traceAndLog("NetClientSocketService.onClientSocketDataHandshake2. Unknown type: " + o_data.type);
          break;
      }
    }
    
    //==================================================================
    // onClientSocketError
    //==================================================================
    private function onClientSocketError(o_event:ErrorEvent):void {
      traceAndLog("NetClientSocketService.onClientSocketError. Error: " + o_event.text);
      dispatchEvent(o_event);
      mo_clientSocket.close();
    }
    
    //==================================================================
    // notifyConnectionComplete
    //==================================================================
    private function notifyConnectionComplete():void {
      if (mo_server) {
        mo_server.clientSocketOnConnectionComplete(this);
      }
    }
    
    //==================================================================
    // readSocketData
    //==================================================================
    private function readSocketData():NetSocketData {
      try {
        return(mo_clientSocket.readObject() as NetSocketData);
      } catch (o_error:Error) {
        traceAndLog("NetClientSocketService.readSocketData. Error: " + o_error);
        dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, o_error.message));
      }
      
      return(null);
    }
    
    //==================================================================
    // traceAndLog
    //==================================================================
    private function traceAndLog(s_message:String):void {
      const USE_DATE_TIME:Boolean = false;
      Utils.TraceAndLog(s_message, mo_txfLog, USE_DATE_TIME);
    }
    
    //==================================================================
    // writeSocketData
    //==================================================================
    private function writeSocketData(o_data:NetSocketData, b_flush:Boolean = true,
    u_socketMessageType:uint = 0):Boolean {
      if (o_data == null) {
        traceAndLog("NetClientSocketService.writeSocketData. Warning: No NetSocketData provided.");
        return(false);
      }
      
      //write the message type and data as a chunk of bytes to the socket stream
      var o_bytes:ByteArray = new ByteArray();
      o_bytes.writeUnsignedInt(u_socketMessageType || NetSocketMessageTypes.SOCKET_DATA);
      o_bytes.writeObject(o_data);
      o_bytes.position = 0;
      
      try {
        //prepend the message type and data with the bytesize of the message type and data
        mo_clientSocket.writeUnsignedInt(o_bytes.length);
        mo_clientSocket.writeBytes(o_bytes);
        
        if (b_flush) {
          mo_clientSocket.flush();
        }
      } catch (o_error:Error) {
        traceAndLog("NetClientSocketService.writeSocketData. " + o_error);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // writeString
    //==================================================================
    private function writeString(s_string:String, b_flush:Boolean = true):Boolean {
      try {
        mo_clientSocket.writeUTFBytes(s_string);
        mo_clientSocket.flush();
        
        if (b_flush) {
          mo_clientSocket.flush();
        }
      } catch (o_error:Error) {
        traceAndLog("NetClientSocketService.writeString. " + o_error);
        return(false);
      }
      
      return(true);
    }
  }
}