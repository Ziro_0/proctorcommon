/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  NetServer

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package net.server {
  import flash.events.Event;
  import flash.events.ServerSocketConnectEvent;
  import flash.net.ServerSocket;
  import flash.net.Socket;
  import flash.text.TextField;
  import flash.utils.Dictionary;
  import net.data.NetClientData;
  import net.data.NetSocketData;
  import net.utils.NetRegisterClassAliases;
  import net.utils.Utils;

  public class NetServer implements I_NetServer {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (client id)
    //data: NetClientSocketService
    private var mo_clientSocketServices:Dictionary;
    
    private var mo_callbacks:I_NetServerCallbacks;
    private var mo_serverSocket:ServerSocket;
    
    private var ms_policyResponse:String;
    private var mo_txfLog:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function NetServer(o_callbacks:I_NetServerCallbacks = null):void {
      super();
      
      //register some data classes for AMF encoding
      NetRegisterClassAliases.Run();
      
      mo_clientSocketServices = new Dictionary();
      callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_NetServerCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_NetServerCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // clientSocketOnClientState
    //==================================================================
    public function clientSocketOnClientMessage(o_clientSocketService:NetClientSocketService,
    o_data:NetSocketData):void {
      if (mo_callbacks) {
        mo_callbacks.netServerOnClientMessage(this, o_clientSocketService.id, o_data.message);
      }
    }
    
    //==================================================================
    // clientSocketOnClientState
    //==================================================================
    public function clientSocketOnClientState(o_clientSocketService:NetClientSocketService,
    o_data:NetSocketData):void {
      if (mo_callbacks) {
        mo_callbacks.netServerOnClientState(this, o_clientSocketService.id, o_data.clientData);
      }
    }
    
    //==================================================================
    // clientSocketOnConnectionComplete
    //==================================================================
    public function clientSocketOnConnectionComplete(o_clientSocketService:NetClientSocketService):void {
      if (mo_callbacks) {
        mo_callbacks.netServerOnClientConnect(this, o_clientSocketService.id);
      }
      
      trace("NetServer.clientSocketOnConnectionComplete. New client connection id: " +
        o_clientSocketService.id);
      traceAndLog(o_clientSocketService.clientData.userData.name + " joined.");
    }
    
    //==================================================================
    // connect
    //==================================================================
    public function connect(i_serverPort:int, s_serverAddress:String, s_policyResponse:String = null):Boolean {
      if (mo_serverSocket) {
        traceAndLog("NetServer.connect. Already connected.");
        return(true); //already connected
      }
      
      trace("NetServer.connect. Attempting to bind and listen on address: " + s_serverAddress +
        ", using port: " + i_serverPort + "...");
      
      ms_policyResponse = s_policyResponse;
      
      try {
        mo_serverSocket = new ServerSocket();
        mo_serverSocket.addEventListener(ServerSocketConnectEvent.CONNECT, onServerSocketConnect,
          false, 0, true);
        mo_serverSocket.addEventListener(Event.CLOSE, onServerSocketClose, false, 0, true);
        
        mo_serverSocket.bind(i_serverPort, s_serverAddress);
        mo_serverSocket.listen();
      } catch (o_error:Error) {
        trace("NetServer.connect. Error: " + o_error.message);
        traceAndLog("Unable to start server: Error: " + o_error.message);
        mo_serverSocket = null;
        return(false);
      }
      
      trace("NetServer.connect. Listening on address: " + mo_serverSocket.localAddress +
        ", using port: " + mo_serverSocket.localPort);
      traceAndLog("Server started.");
      return(true);
    }
    
    //==================================================================
    // disconnect
    //==================================================================
    public function disconnect():void {
      if (!mo_serverSocket) {
        return;
      }
      
      disconnectAllClients();
      
      try {
        mo_serverSocket.close();
        trace("NetServer.disconnect.");
        traceAndLog("Server stopped.");
      } catch (o_error:Error) {
        traceAndLog("NetServer.disconnect. Error: " + o_error.message);
      }
      
      mo_serverSocket.removeEventListener(ServerSocketConnectEvent.CONNECT, onServerSocketConnect);
      mo_serverSocket.removeEventListener(Event.CLOSE, onServerSocketClose);
      mo_serverSocket = null;
    }
    
    //==================================================================
    // disconnectAllClients
    //==================================================================
    public function disconnectAllClients():void {
      var vs_clientIds:Vector.<String> = getClientDataIds();
      for each (var s_clientId:String in vs_clientIds) {
        disconnectClient(s_clientId);
      }
    }
    
    //==================================================================
    // disconnectClient
    //==================================================================
    public function disconnectClient(s_clientId:String):void {
      var o_clientSocketService:NetClientSocketService = getClientSocketService(s_clientId);
      if (!o_clientSocketService) {
        return;
      }
      
      trace("NetServer.disconnectClient. Client ID: " + s_clientId);
      traceAndLog(o_clientSocketService.clientData.userData.name + " left.");
      
      o_clientSocketService.disconnect();
      delete mo_clientSocketServices[s_clientId];
      
      if (mo_callbacks) {
        mo_callbacks.netServerOnClientClose(this, s_clientId);
      }
    }
    
    //==================================================================
    // getClientData
    //==================================================================
    public function getClientData(s_clientId:String):NetClientData {
      var o_clientSocketService:NetClientSocketService = getClientSocketService(s_clientId);
      return(o_clientSocketService ? o_clientSocketService.clientData : null);
    }
    
    //==================================================================
    // getClientDataIds
    //==================================================================
    public function getClientDataIds(vs_clientIds_out:Vector.<String> = null):Vector.<String> {
      if (!vs_clientIds_out) {
        vs_clientIds_out = new Vector.<String>();
      }
      
      vs_clientIds_out.length = 0;
      for (var s_clientId:String in mo_clientSocketServices) {
        vs_clientIds_out.push(s_clientId);
      }
      
      return(vs_clientIds_out);
    }
    
    //==================================================================
    // isValidClientId
    //==================================================================
    public function isValidClientId(s_clientId:String):Boolean {
      return(mo_clientSocketServices && s_clientId && s_clientId in mo_clientSocketServices);
    }
    
    //==================================================================
    // numClients (get)
    //==================================================================
    public function get numClients():uint {
      return(getClientDataIds().length);
    }
    
    //==================================================================
    // logTextField (set)
    //==================================================================
    public function set logTextField(o_value:TextField):void {
      mo_txfLog = o_value;
      setLogTextFieldToClicneSockets();
    }
    
    //==================================================================
    // sendMessageToClient
    //==================================================================
    public function sendMessageToClient(s_clientId:String, o_message:Object):void {
      if (o_message == null) {
        return;
      }
      
      var o_clientSocketService:NetClientSocketService = getClientSocketService(s_clientId);
      if (o_clientSocketService) {
        o_clientSocketService.sendMessageToClient(o_message);
      }
    }
    
    //==================================================================
    // sendMessageToClients
    //==================================================================
    public function sendMessageToClients(o_message:Object):void {
      if (o_message == null) {
        return;
      }
      
      for each (var o_clientSocketService:NetClientSocketService in mo_clientSocketServices) {
        o_clientSocketService.sendMessageToClient(o_message);
      }
    }
    
    //==================================================================
    // sendStateToClient
    //==================================================================
    public function sendStateToClient(s_clientId:String, o_state:Object):void {
      var o_clientSocketService:NetClientSocketService = getClientSocketService(s_clientId);
      if (o_clientSocketService) {
        o_clientSocketService.sendStateToClient(o_state);
      }
    }
    
    //==================================================================
    // sendStateToClients
    //==================================================================
    public function sendStateToClients(o_state:Object):void {
      for each (var o_clientSocketService:NetClientSocketService in mo_clientSocketServices) {
        o_clientSocketService.sendStateToClient(o_state);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getClientSocketService
    //==================================================================
    private function getClientSocketService(s_clientId:String):NetClientSocketService {
      return(s_clientId in mo_clientSocketServices ?
        mo_clientSocketServices[s_clientId] as NetClientSocketService :
        null);
    }
    
    //==================================================================
    // getSocketServiceBySocket
    //==================================================================
    private function getSocketServiceBySocket(o_socket:Socket):NetClientSocketService {
      for each (var o_clientSocketService:NetClientSocketService in mo_clientSocketServices) {
        if (o_clientSocketService.hasSocket(o_socket)) {
          return(o_clientSocketService);
        }
      }
      
      return(null);
    }
    
    //==================================================================
    // onClientSocketServiceClose
    //==================================================================
    private function onClientSocketServiceClose(o_event:Event):void {
      var o_socketService:NetClientSocketService = o_event.currentTarget as NetClientSocketService;
      o_socketService.removeEventListener(Event.CLOSE, onClientSocketServiceClose);
      
      delete mo_clientSocketServices[o_socketService.id];
      
      trace("NetServer.onClientSocketServiceClose. Client socket closed using ID: " + o_socketService.id);
      traceAndLog(o_socketService.clientData.userData.name + " left.");
      
      if (mo_callbacks) {
        mo_callbacks.netServerOnClientClose(this, o_socketService.id);
      }
    }
    
    //==================================================================
    // onServerSocketClose
    //==================================================================
    private function onServerSocketClose(o_event:Event):void {
      trace("NetServer.onServerSocketClose.");
      mo_serverSocket.removeEventListener(ServerSocketConnectEvent.CONNECT, onServerSocketConnect);
      mo_serverSocket.removeEventListener(Event.CLOSE, onServerSocketClose);
      mo_serverSocket = null;
    }
    
    //==================================================================
    // onServerSocketConnect
    //==================================================================
    private function onServerSocketConnect(o_event:ServerSocketConnectEvent):void {
      var o_clientSocketService:NetClientSocketService = new NetClientSocketService(this, o_event.socket,
        ms_policyResponse);
      o_clientSocketService.logTextField = mo_txfLog;
      
      var s_msg:String = "NetServer.onServerSocketConnect. Received new client connection from " +
        o_clientSocketService.localAddress + ":" + o_clientSocketService.localPort;
      trace(s_msg);
      
      o_clientSocketService.addEventListener(Event.CLOSE, onClientSocketServiceClose, false, 0, true);
      mo_clientSocketServices[o_clientSocketService.id] = o_clientSocketService;
    }
    
    //==================================================================
    // setLogTextFieldToClicneSockets
    //==================================================================
    private function setLogTextFieldToClicneSockets():void {
      for each (var o_clientSocketService:NetClientSocketService in mo_clientSocketServices) {
        o_clientSocketService.logTextField = mo_txfLog;
      }
    }
    
    //==================================================================
    // traceAndLog
    //==================================================================
    private function traceAndLog(s_message:String):void {
      const USE_DATE_TIME:Boolean = false;
      Utils.TraceAndLog(s_message, mo_txfLog, USE_DATE_TIME);
    }
  }
}