/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  FButton

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:         01-01-2015
  ActionScript:  3.0
  Description:  Base class for custom buttons. Also fixes flickering glitches that exist on
    SimpleButton when changing mouse states.
    
  History:
    09-10-2012:  Started.
    12-05-2012: Added CreateFromSimpleButton static method.
    02-01-2013: Added isOver and isDown properties.
    01-01-2015:
      - Extended FDisplay for consistance with the other flip ui composition display classes.
      - Changed CreateFromSimpleButton to accept a display name or a simple button, and a click callback.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.ui.button {
  import flib.utils.ExtUtils;
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.display.SimpleButton;
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.MouseEvent;
  import flash.events.TouchEvent;
  import flib.ui.FDisplay;
  
  public class FButton extends FDisplay {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static var smo_btnCapturedMouse:FButton;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_imageNorm:DisplayObject;
    private var mo_imageOver:DisplayObject;
    private var mo_imageDown:DisplayObject;
    private var mo_imageDisabled:DisplayObject;
    private var mo_activeImage:DisplayObject;
    
    private var mb_isOver:Boolean;
    private var mb_isDown:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function FButton(o_imageNorm:DisplayObject = null,
    o_imageOver:DisplayObject = null, o_imageDown:DisplayObject = null,
    o_imageDisabled:DisplayObject = null):void {
      super(Sprite);
      
      display.focusRect = false;
      display.mouseChildren = false;
      display.useHandCursor = display.buttonMode = true;
      
      init(o_imageNorm, o_imageOver, o_imageDown, o_imageDisabled);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // CreateFromSimpleButton
    //==================================================================
    public static function CreateFromSimpleButton(o_simpleButtonOrDisplayName:Object,
    o_imageDisabled:DisplayObject = null, f_clickCallback:Function = null,
    o_container:DisplayObjectContainer = null, b_simpleButtonIsPlaceholder:Boolean = true):FButton {
      var o_button:SimpleButton;
      
      if (o_simpleButtonOrDisplayName is SimpleButton) {
        o_button = o_simpleButtonOrDisplayName as SimpleButton;
      } else if (o_simpleButtonOrDisplayName is String) {
        if (o_container) {
          o_button = o_container.getChildByName(o_simpleButtonOrDisplayName as String) as SimpleButton;
        }
      }
      
      if (!o_button) {
        return(null);
      }
      
      var o_fbutton:FButton = new FButton(o_button.upState, o_button.overState, o_button.downState,
        o_imageDisabled);
      if (b_simpleButtonIsPlaceholder) {
        o_fbutton.display.transform = o_button.transform;
        o_fbutton.display.alpha = o_button.alpha;
        o_fbutton.display.blendMode = o_button.blendMode;
        o_fbutton.display.filters = o_button.filters;
        o_fbutton.display.name = o_button.name;
        o_fbutton.visible = o_button.visible;
        
        o_container ||= o_button.parent;
        if (o_container) {
          o_container.addChildAt(o_fbutton.display, o_container.getChildIndex(o_button));
          o_container.removeChild(o_button);
        }
      }
      
      if (f_clickCallback != null) {
        o_fbutton.initClickableObject(o_fbutton.display, f_clickCallback);
      }
      
      return(o_fbutton);
    }
    
    //==================================================================
    // enabled (set)
    //==================================================================
    public override function set enabled(b_value:Boolean):void {
      mb_enabled = b_value;
      display.mouseEnabled = b_value;
      updateDisabledDisplay();
      updateDisplay();
    }
    
    //==================================================================
    // getCaptureButton
    //==================================================================
    public function getCaptureButton():FButton {
      return(smo_btnCapturedMouse);
    }
    
    //==================================================================
    // imageDisabled (get)
    //==================================================================
    public function get imageDisabled():DisplayObject {
      return(mo_imageDisabled);
    }
    
    //==================================================================
    // imageDisabled (set)
    //==================================================================
    public function set imageDisabled(o_image:DisplayObject):void {
      mo_imageDisabled = o_image;
      updateDisplay();
    }
    
    //==================================================================
    // imageDown (get)
    //==================================================================
    public function get imageDown():DisplayObject {
      return(mo_imageDown);
    }
    
    //==================================================================
    // imageDown (set)
    //==================================================================
    public function set imageDown(o_image:DisplayObject):void {
      mo_imageDown = o_image;
      updateDisplay();
    }
    
    //==================================================================
    // imageNorm (get)
    //==================================================================
    public function get imageNorm():DisplayObject {
      return(mo_imageNorm);
    }
    
    //==================================================================
    // imageNorm (set)
    //==================================================================
    public function set imageNorm(o_image:DisplayObject):void {
      mo_imageNorm = o_image;
      
      if (mo_imageDown == mo_imageNorm) {
        mo_imageDown = mo_imageNorm;
      }
      
      if (mo_imageOver == mo_imageNorm) {
        mo_imageOver = mo_imageNorm;
      }
      
      if (mo_imageDisabled == mo_imageNorm) {
        mo_imageDisabled = mo_imageNorm;
      }
      
      updateDisplay();
    }
    
    //==================================================================
    // imageOver (get)
    //==================================================================
    public function get imageOver():DisplayObject {
      return(mo_imageOver);
    }
    
    //==================================================================
    // imageOver (set)
    //==================================================================
    public function set imageOver(o_image:DisplayObject):void {
      mo_imageOver = o_image;
      updateDisplay();
    }
    
    //==================================================================
    // init
    //==================================================================
    public function init(o_imageNorm:DisplayObject, o_imageOver:DisplayObject,
    o_imageDown:DisplayObject, o_imageDisabled:DisplayObject):void {
      
      if (ExtUtils.isMobile()) {
        display.addEventListener(TouchEvent.TOUCH_BEGIN, _onMouseDown, false, 0, true);
        display.addEventListener(TouchEvent.TOUCH_OUT, _onMouseOut, false, 0, true);
        display.addEventListener(TouchEvent.TOUCH_OVER, _onMouseOver, false, 0, true);
        display.addEventListener(TouchEvent.TOUCH_END, _onMouseUp, false, 0, true);
      } else {
        display.addEventListener(MouseEvent.MOUSE_DOWN, _onMouseDown, false, 0, true);
        display.addEventListener(MouseEvent.MOUSE_OUT, _onMouseOut, false, 0, true);
        display.addEventListener(MouseEvent.MOUSE_OVER, _onMouseOver, false, 0, true);
        display.addEventListener(MouseEvent.MOUSE_UP, _onMouseUp, false, 0, true);
      }
      
      mo_imageNorm = o_imageNorm;
      mo_imageOver = o_imageOver || mo_imageNorm;
      mo_imageDown = o_imageDown || mo_imageNorm;
      mo_imageDisabled = o_imageDisabled || mo_imageNorm;
      
      updateDisplay();
    }
    
    //==================================================================
    // isDown (get)
    //==================================================================
    public function get isDown():Boolean {
      return(mb_isDown);
    }
    
    //==================================================================
    // isOver (get)
    //==================================================================
    public function get isOver():Boolean {
      return(mb_isOver);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      if (ExtUtils.isMobile()) {
        display.removeEventListener(TouchEvent.TOUCH_END, _onMouseDown);
        display.removeEventListener(TouchEvent.TOUCH_OUT, _onMouseOut);
        display.removeEventListener(TouchEvent.TOUCH_OVER, _onMouseOver);
        display.removeEventListener(TouchEvent.TOUCH_END, _onMouseUp);
      } else {
        display.removeEventListener(MouseEvent.MOUSE_DOWN, _onMouseDown);
        display.removeEventListener(MouseEvent.MOUSE_OUT, _onMouseOut);
        display.removeEventListener(MouseEvent.MOUSE_OVER, _onMouseOver);
        display.removeEventListener(MouseEvent.MOUSE_UP, _onMouseUp);
      }
      
      if (this == smo_btnCapturedMouse) {
        releaseMouseCpture();
        
        mb_isDown = false;
        updateDisplay();
        onMouseUp();
      }
      
      mo_imageDisabled = mo_imageDown = mo_imageNorm = mo_imageOver = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onMouseDown
    //==================================================================
    protected function onMouseDown():void {
    }
    
    //==================================================================
    // onMouseOut
    //==================================================================
    protected function onMouseOut():void {
    }
    
    //==================================================================
    // onMouseOver
    //==================================================================
    protected function onMouseOver():void {
    }
    
    //==================================================================
    // onMouseUp
    //==================================================================
    protected function onMouseUp():void {
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // captureMouse
    //==================================================================
    private static function captureMouse(o_button:FButton):void {
      if (o_button.display.stage && o_button.mb_isDown) {
        smo_btnCapturedMouse = o_button;
        
        //button assumes focus when capturing mouse
        o_button.display.stage.focus = o_button.display;
        
        o_button.display.addEventListener(Event.REMOVED_FROM_STAGE, o_button.onRemovedFromStage,
          false, 0, true);
        
        if (ExtUtils.isMobile()) {
          o_button.display.stage.addEventListener(TouchEvent.TOUCH_END, onStageMouseUp, false, 0, true);
        } else {
          o_button.display.stage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp, false, 0, true);
        }
      }
    }
    
    //==================================================================
    // _onMouseDown
    //==================================================================
    private function _onMouseDown(o_event:Object):void {
      mb_isDown = true;
      
      if (ExtUtils.isMobile()) {
        mb_isOver = true;
      }
      
      updateDisplay();
      captureMouse(this);
      onMouseDown();
    }
    
    //==================================================================
    // _onMouseOut
    //==================================================================
    private function _onMouseOut(o_event:Object):void {
      mb_isOver = false;
      updateDisplay();
      onMouseOut();
    }
    
    //==================================================================
    // _onMouseOver
    //==================================================================
    private function _onMouseOver(o_event:Object):void {
      if (smo_btnCapturedMouse && smo_btnCapturedMouse != this) {
        return;
      }
      
      mb_isOver = true;
      updateDisplay();
      onMouseOver();
    }
    
    //==================================================================
    // _onMouseUp
    //==================================================================
    private function _onMouseUp(o_event:Object):void {
      if (smo_btnCapturedMouse && smo_btnCapturedMouse != this) {
        smo_btnCapturedMouse._onMouseUp(null);
        releaseMouseCpture();
        _onMouseOver(null);
        return;
      }
      
      mb_isDown = false;
      
      if (ExtUtils.isMobile()) {
        mb_isOver = false;
      }
      
      updateDisplay();
      onMouseUp();
    }
    
    //==================================================================
    // onRemovedFromStage
    //==================================================================
    private function onRemovedFromStage(o_event:Event):void {
      var b_isDown:Boolean = mb_isDown;
      var b_isOver:Boolean = mb_isOver;
      var b_update:Boolean = mb_isDown || mb_isOver;
      
      display.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
      
      if (smo_btnCapturedMouse == this) {
        releaseMouseCpture();
      }
      
      mb_isOver = mb_isDown = false;
      
      if (b_update) {
        if (b_isOver) {
          onMouseOut();
        }
        
        if (b_isDown) {
          onMouseUp();
        }
        
        updateDisplay();
      }
    }
    
    //==================================================================
    // onStageMouseUp
    //==================================================================
    private static function onStageMouseUp(o_event:Object):void {
      var o_button:FButton = releaseMouseCpture();
      if (o_button) {
        o_button.mb_isDown = false;
        
        if (ExtUtils.isMobile()) {
          o_button.mb_isOver = false;
        }
        
        o_button.updateDisplay();
        o_button.onMouseUp();
      }
    }
    
    //==================================================================
    // releaseMouseCpture
    //==================================================================
    private static function releaseMouseCpture():FButton {
      var o_button:FButton = smo_btnCapturedMouse;
      o_button.removeEventListener(Event.REMOVED_FROM_STAGE, o_button.onRemovedFromStage);
      
      if (smo_btnCapturedMouse.display.stage) {
        smo_btnCapturedMouse.display.stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
        smo_btnCapturedMouse.display.stage.removeEventListener(TouchEvent.TOUCH_END, onStageMouseUp);
        smo_btnCapturedMouse = null;
      }
      
      return(o_button);
    }
    
    //==================================================================
    // updateDisplay
    //==================================================================
    private function updateDisplay():void {
      var o_newImage:DisplayObject;
      
      if (!enabled) {
        o_newImage = mo_imageDisabled;
      } else {
        if (mb_isDown) {
          o_newImage = mb_isOver ? mo_imageDown : mo_imageNorm;
        } else if (mb_isOver) {
          o_newImage = mb_isDown ? mo_imageDown : mo_imageOver;
        } else {
          o_newImage = mo_imageNorm;
        }
      }
      
      if (o_newImage != mo_activeImage) {
        if (mo_activeImage && display.contains(mo_activeImage)) {
          display.removeChild(mo_activeImage);
        }
        
        mo_activeImage = o_newImage;
        
        if (mo_activeImage) {
          display.addChild(mo_activeImage);
        }
      }
    }
  }
}