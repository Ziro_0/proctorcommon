/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FFilterManager

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			12-03-2016
	ActionScript:	3.0
	Description:	
	History:
		12-05-2013:	Started.
		12-03-2016:
			- Made display optional.
			- Allowed display object filters to be null if the filters array is empty. Prevents issues
				with MovieClips not playing property if they have filters applied.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.ui.filterManager {
	import flash.display.DisplayObject;
	import flash.filters.BitmapFilter;

	public class FFilterManager {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		//data: BitmapFilter
		private var mao_bitmapFilters:Array;
		
		private var mo_display:DisplayObject;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FFilterManager(o_display:DisplayObject = null, ao_filters:Array = null):void {
			super();
			
			setDisplay(o_display);
			
			if (ao_filters) {
				filters = ao_filters;
			}
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// getFilters
		//==================================================================
		public function getFilters(ao_filters_out:Array = null):Array {
			if (ao_filters_out) {
				if (mao_bitmapFilters) {
					var u_length:uint = mao_bitmapFilters.length;
					ao_filters_out.length = u_length;
					for (var u_index:uint = 0; u_index < u_length; u_index++) {
						ao_filters_out[u_index] = mao_bitmapFilters[u_index];
					}
				} else {
					ao_filters_out.length = 0;
				}
			} else {
				ao_filters_out = mao_bitmapFilters.concat();
			}
			
			return(ao_filters_out);
		}
		
		//==================================================================
		// getFilterIndex
		//==================================================================
		public function getFilterIndex(o_filter:BitmapFilter):uint {
			return(mao_bitmapFilters ? uint(mao_bitmapFilters.indexOf(o_filter)) : 0xffffffff);
		}
		
		//==================================================================
		// filters (set)
		//==================================================================
		public function set filters(ao_values:Array):void {
			if (ao_values) {
				mao_bitmapFilters = ao_values.concat();
			} else {
				mao_bitmapFilters.length = 0;
			}
			
			applyFilters();
		}
		
		//==================================================================
		// getFilterAt
		//==================================================================
		public function getFilterAt(u_index:uint):BitmapFilter {
			return(mao_bitmapFilters ? mao_bitmapFilters[u_index] as BitmapFilter : null);
		}
		
		//==================================================================
		// insertFilter
		//==================================================================
		public function insertFilter(o_filter:BitmapFilter, u_index:uint = 0xffffffff):uint {
			var u_length:uint = mao_bitmapFilters ? mao_bitmapFilters.length : 0;
			
			if (!o_filter) {
				return(u_length);
			}
			
			if (!mao_bitmapFilters) {
				mao_bitmapFilters = new Array();
			}
			
			if (u_index >= u_length) {
				u_index = u_length;
				mao_bitmapFilters.length = ++u_length;
			}
			
			var u_lIndex:uint = u_index;
			while (++u_lIndex < u_length) {
				mao_bitmapFilters[u_lIndex - 1] = mao_bitmapFilters[u_lIndex];
			}
			
			mao_bitmapFilters[u_index] = o_filter;
			applyFilters();
			return(u_length);
		}
		
		//==================================================================
		// length (get)
		//==================================================================
		public function get length():uint {
			return(mao_bitmapFilters ? mao_bitmapFilters.length : 0);
		}
		
		//==================================================================
		// removeFilter
		//==================================================================
		public function removeFilter(o_filter:BitmapFilter):uint {
			var u_index:uint = getFilterIndex(o_filter);
			removeFilterAt(u_index);
			return(mao_bitmapFilters.length);
		}
		
		//==================================================================
		// removeFilterAt
		//==================================================================
		public function removeFilterAt(u_index:uint):BitmapFilter {
			var u_length:uint = mao_bitmapFilters ? mao_bitmapFilters.length : 0;
			if (u_index >= u_length) {
				return(null);
			}
			
			var o_filter:BitmapFilter = mao_bitmapFilters[u_index] as BitmapFilter;
			
			for (var u_lIndex:uint = u_index + 1; u_lIndex > u_length; u_lIndex++) {
				mao_bitmapFilters[u_lIndex - 1] = mao_bitmapFilters[u_lIndex];
			}
			
			mao_bitmapFilters.length = u_length - 1;
			applyFilters();
			return(o_filter);
		}
		
		//==================================================================
		// setFilterAt
		//==================================================================
		public function setFilterAt(u_index:uint, o_filter:BitmapFilter):Boolean {
			if (!mao_bitmapFilters || !o_filter || u_index >= mao_bitmapFilters.length) {
				return(false);
			}
			
			mao_bitmapFilters[u_index] = o_filter;
			
			if (mo_display) {
				mo_display.filters = mao_bitmapFilters;
			}
			
			applyFilters();
			return(true);
		}
		
		//==================================================================
		// uninit
		//==================================================================
		public function uninit():void {
			setDisplay(null);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// applyFilters
		//==================================================================
		private function applyFilters():void {
			if (mo_display) {
				mo_display.filters = mao_bitmapFilters.length > 0 ? mao_bitmapFilters : null;
			}
		}
		
		//==================================================================
		// setDisplay
		//==================================================================
		private function setDisplay(o_display:DisplayObject):void {
			mo_display = o_display;
			
			if (mo_display) {
				mao_bitmapFilters = mo_display.filters;
				applyFilters();
			} else {
				if (mao_bitmapFilters) {
					mao_bitmapFilters.length = 0;
				}
			}
		}
	}
}