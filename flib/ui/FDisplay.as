/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FDisplay

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/freelancers/~0180f72de0fb06b175
	Date:   			03-22-2017
	ActionScript:	3.0
	Description:	Case class for composition-based display object classes.
	History:
		11-09-2012:	Started.
		03-07-2013: Added getMovieClip method.
		03-23-2013: Added validity check for name strings on the protected get methods.
		05-09-2013: Modified initClickableObject method to accept an InteractiveObject, as well
			as the name of one.
		11-01-2013: Added visible g/s methods.
		12-03-2013:
			- Added g/s enabled properties.
			- Added protected updateDisabledDisplay method.
			- Added g/s disabledFilter properties.
			- First three updates allow moer flexibility with showing the disabled state
			- Chagned interactive table to IEvent dispatchers, since CLICK handler ultimately implement the
				IEventDispatcher interface, and an FDisplay object could dispatch a CLICK event from its sub-display.
		12-05-2013:
			- Added FFilter class for more flexible modifications of the internal display's bitmap filters.
		10-14-2014:
			- Change constructor parameters from a sprite or a sprite class to one object parameter, that
				acceps either a sprite or a sprite class.
		01-02-2016: Added uninitClickableObject.
		12-06-2016:
			- Changed get... methods for initializing display objects from protected final to public.
			- No longer need to treat this class as abstract.
			- Refactored code.
    03-22-2017: Removed final from updatedDisabledDisplay.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.ui {
	import com.quasimondo.geom.ColorMatrix;
  import flib.utils.ExtUtils;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
  import flash.events.TouchEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flib.ui.filterManager.FFilterManager;

	public class FDisplay extends EventDispatcher {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		private static var smao_childList:Array;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		protected var mo_display:Sprite;
		
		protected var mb_enabled:Boolean;
		
		//key: SimpleButton
		//data: Function
		private var mo_buttonCallbacks:Dictionary;
		
		//key: IEventDispatcher
		//data: Function (CLICK event callback, or null if no CLICK function)
		protected var mo_clickableCallbacks:Dictionary;
		
		private var mo_filterManager:FFilterManager;
		
		private var mo_disabledFilter:ColorMatrixFilter;
		
		private var mb_showDisabledFilter:Boolean;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FDisplay(o_spriteOrSpriteClass:Object) {
			super();
			
			if (o_spriteOrSpriteClass is Sprite) {
				mo_display = o_spriteOrSpriteClass as Sprite;
			} else if (o_spriteOrSpriteClass is Class) {
				mo_display = new o_spriteOrSpriteClass() as Sprite;
			}
			
			if (!mo_display) {
				throw(ArgumentError("FDisplay constructor. Error: Construction must resolve to a valid Sprite."));
			}
			
			mo_clickableCallbacks = new Dictionary();
			
			mb_enabled = mb_showDisabledFilter = true;
			mo_display.focusRect = false;
			
			mo_buttonCallbacks = new Dictionary();
			mo_filterManager = new FFilterManager(mo_display);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// disabledFilter (get)
		//==================================================================
		public function get disabledFilter():ColorMatrixFilter {
			return(mo_disabledFilter);
		}
		
		//==================================================================
		// disabledFilter (set)
		//==================================================================
		public function set disabledFilter(o_value:ColorMatrixFilter):void {
			mo_disabledFilter = o_value;
		}
		
		//==================================================================
		// display (get)
		//==================================================================
		public function get display():Sprite {
			return(mo_display);
		}
		
		//==================================================================
		// enabled (get)
		//==================================================================
		public function get enabled():Boolean {
			return(mb_enabled);
		}
		
		//==================================================================
		// enabled (set)
		//==================================================================
		public function set enabled(b_value:Boolean):void {
			mb_enabled = b_value;
			
			var o_display:Sprite = mo_display;
			if (!o_display) {
				return;
			}
			
			if (o_display is DisplayObjectContainer) {
				var o_container:DisplayObjectContainer = o_display as DisplayObjectContainer
				o_container.mouseChildren = o_container.tabChildren = mb_enabled;
			}
			
			if (o_display is SimpleButton) {
				var o_btn:SimpleButton = o_display as SimpleButton;
				o_btn.enabled = o_btn.mouseEnabled = mb_enabled;
			}
			
			if (o_display is InteractiveObject) {
				var o_interactive:InteractiveObject = o_display as InteractiveObject;
				o_interactive.mouseEnabled = o_interactive.tabEnabled = mb_enabled;
			}
			
			updateDisabledDisplay();
		}
		
		//==================================================================
		// filterManager (get)
		//==================================================================
		public function get filterManager():FFilterManager {
			return(mo_filterManager);
		}
		
		//==================================================================
		// getMovieClip
		//==================================================================
		public function getMovieClip(s_name:String,
		o_container:DisplayObjectContainer = null):MovieClip {
			if (!s_name) {
				return(null);
			}
			
			o_container ||= mo_display as DisplayObjectContainer;
			if (!o_container) {
				return(null);
			}
			
			return(o_container.getChildByName(s_name) as MovieClip);
		}
		
		//==================================================================
		// getSimpleButton
		//==================================================================
		public function getSimpleButton(s_name:String, f_onClick:Function = null,
		o_container:DisplayObjectContainer = null):SimpleButton {
			if (!s_name) {
				return(null);
			}
			
			o_container ||= mo_display as DisplayObjectContainer;
			if (!o_container) {
				return(null);
			}
			
			var o_btn:SimpleButton = o_container.getChildByName(s_name) as SimpleButton;
			if (o_btn) {
				o_btn.focusRect = false;
				if (f_onClick != null) {
					o_btn.addEventListener(MouseEvent.CLICK, f_onClick, false, 0, true);
					mo_buttonCallbacks[o_btn] = f_onClick;
				}
			}
			
			return(o_btn);
		}
		
		//==================================================================
		// getSprite
		//==================================================================
		public function getSprite(s_name:String,
		o_container:DisplayObjectContainer = null):Sprite {
			if (!s_name) {
				return(null);
			}
			
			o_container ||= mo_display as DisplayObjectContainer;
			if (!o_container) {
				return(null);
			}
			
			return(o_container.getChildByName(s_name) as Sprite);
		}
		
		//==================================================================
		// getTextField
		//==================================================================
		public function getTextField(s_name:String, s_text:String = null,
		o_container:DisplayObjectContainer = null):TextField {
			if (!s_name) {
				return(null);
			}
			
			o_container ||= mo_display as DisplayObjectContainer;
			if (!o_container) {
				return(null);
			}
			
			var o_txf:TextField = o_container.getChildByName(s_name) as TextField;
			if (o_txf) {
				if (s_text != null) {
					o_txf.text = s_text;
				}
			}
			
			return(o_txf);
		}
		
		//==================================================================
		// initClickableObject
		//==================================================================
		public function initClickableObject(o_nameOrIEventDispatcherObject:Object,
		f_click:Function, o_container:DisplayObjectContainer = null):IEventDispatcher {
			o_container ||= mo_display;
			
			var o_dispatcher:IEventDispatcher;
			
			if (o_nameOrIEventDispatcherObject is String) {
				var s_name:String = String(o_nameOrIEventDispatcherObject);
				if (!s_name) {
					return(null);
				}
				o_dispatcher = o_container.getChildByName(s_name) as IEventDispatcher;
			} else if (o_nameOrIEventDispatcherObject is IEventDispatcher) {
				o_dispatcher = o_nameOrIEventDispatcherObject as IEventDispatcher;
			}
			
			if (o_dispatcher) {
				if (o_dispatcher is InteractiveObject) {
					var o_interactive:InteractiveObject = o_dispatcher as InteractiveObject;
					o_interactive.focusRect = false;
				}
				
				if (f_click != null) {
          if (ExtUtils.isMobile()) {
            o_dispatcher.addEventListener(TouchEvent.TOUCH_TAP, f_click, false, 0, true);
          } else {
            o_dispatcher.addEventListener(MouseEvent.CLICK, f_click, false, 0, true);
          }
				}
				
				mo_clickableCallbacks[o_dispatcher] = f_click;
			}
			
			return(o_dispatcher);
		}
		
		//==================================================================
		// LocalToStagePoint
		//==================================================================
		public static function LocalToStagePoint(o_display:DisplayObject,
		o_ptInOut:Point = null):Point {
			if (!o_display)  {
				return(o_ptInOut);
			}
			
			var o_stage:Stage = o_display.stage;
			if (!o_stage) {
				return(o_ptInOut);
			}
			
			if (!o_ptInOut) {
				o_ptInOut = new Point(o_display.x, o_display.y);
			}
			
			var o_parent:DisplayObjectContainer = o_display.parent;
			while (o_parent != o_stage) {
				o_ptInOut.offset(o_parent.x, o_parent.y);
				o_parent = o_parent.parent;
			}
			
			return(o_ptInOut);
		}
		
		//==================================================================
		// showDisabledFilter (get)
		//==================================================================
		public function get showDisabledFilter():Boolean {
			return(mb_showDisabledFilter);
		}
		
		//==================================================================
		// showDisabledFilter (set)
		//==================================================================
		public function set showDisabledFilter(b_value:Boolean):void {
			mb_showDisabledFilter = b_value;
			updateDisabledDisplay();
		}
		
		//==================================================================
		// SortChildrenByY
		//==================================================================
		public static function SortChildrenByY(o_container:DisplayObjectContainer):void {
			if (!o_container) {
				return;
			}
			
			var i_length:int = o_container.numChildren;
			if (i_length == 0) {
				return;
			}
			
			var ao_childList:Array = smao_childList;
			if (ao_childList) {
				ao_childList.length = i_length;
			} else {
				ao_childList = smao_childList = new Array(i_length);
			}
			
			// first put all children in an array
			var i_idx:int = i_length - 1;
			do {
				ao_childList[i_idx] = o_container.getChildAt(i_idx);
			} while (--i_idx > 0);
			
			// next, sort the array based on each element's y property
			ao_childList.sortOn("y", Array.NUMERIC);
			
			// now match the arrangement of the array
			// with the arrangement of the display list
			i_idx = i_length - 1;
			do {
				// if the child at position i_idx in the array
				// does not match the child at position
				// i_idx in the display list, set that child
				// to the i_idx position in the display list
				var o_display:DisplayObject = ao_childList[i_idx] as DisplayObject;
				if (o_display != o_container.getChildAt(i_idx)) {
					o_container.setChildIndex(o_display, i_idx);
				}
			} while (--i_idx > 0);
		}
		
		//==================================================================
		// uninit
		//==================================================================
		public function uninit():void {
			for (var o_key:Object in mo_buttonCallbacks) {
				var o_btn:SimpleButton = o_key as SimpleButton;
				var f_callback:Function = mo_buttonCallbacks[o_key] as Function;
				o_btn.removeEventListener(MouseEvent.CLICK, f_callback);
				delete mo_buttonCallbacks[o_key];
			}
			
			uninitInteractives();
			mo_filterManager.uninit();
		}
		
		//==================================================================
		// visible (get)
		//==================================================================
		public function get visible():Boolean {
			return(mo_display.visible);
		}
		
		//==================================================================
		// visible (set)
		//==================================================================
		public function set visible(b_value:Boolean):void {
			mo_display.visible = b_value;
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// uninitBitmap
		//==================================================================
		protected final function uninitBitmap(o_bitmap:Bitmap, b_dispose:Boolean):Bitmap {
			if (o_bitmap) {
				if (b_dispose && o_bitmap.bitmapData) {
					o_bitmap.bitmapData.dispose();
				}
				o_bitmap.bitmapData = null;
				
				if (mo_display && o_bitmap.parent) {
					o_bitmap.parent.removeChild(o_bitmap);
				}
			}
			
			return(null);
		}
		
		//==================================================================
		// uninitClickableObject
		//==================================================================
		protected final function uninitClickableObject(o_nameOrIEventDispatcherObject:Object,
		o_container:DisplayObjectContainer = null):void {
			o_container ||= mo_display;
			
			var o_dispatcher:IEventDispatcher;
			
			if (o_nameOrIEventDispatcherObject is String) {
				var s_name:String = String(o_nameOrIEventDispatcherObject);
				if (!s_name) {
					return;
				}
				o_dispatcher = o_container.getChildByName(s_name) as IEventDispatcher;
			} else if (o_nameOrIEventDispatcherObject is IEventDispatcher) {
				o_dispatcher = o_nameOrIEventDispatcherObject as IEventDispatcher;
			}
			
			if (o_dispatcher in mo_clickableCallbacks) {
				var f_click:Function = mo_clickableCallbacks[o_dispatcher] as Function;
				if (f_click != null) {
					o_dispatcher.removeEventListener(MouseEvent.CLICK, f_click);
				}
				
				delete mo_clickableCallbacks[o_dispatcher];
			}
		}
		
		//==================================================================
		// updateDisabledDisplay
		//==================================================================
		protected function updateDisabledDisplay():void {
			var b_applyDisabledFilter:Boolean = !mb_enabled && mb_showDisabledFilter;
			
			if (b_applyDisabledFilter) {
				var u_index:uint = mo_filterManager.getFilterIndex(mo_disabledFilter);
				if (u_index == 0xffffffff) {
					if (!mo_disabledFilter) {
						var o_cmx:ColorMatrix = new ColorMatrix();
						o_cmx.adjustSaturation(0);
						o_cmx.adjustBrightness( -50);
						
						mo_disabledFilter = o_cmx.filter;
					}
					
					mo_filterManager.insertFilter(mo_disabledFilter);
				}
			} else {
				mo_filterManager.removeFilter(mo_disabledFilter);
			}
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// uninitInteractives
		//==================================================================
		private function uninitInteractives():void {
			for (var o_key:Object in mo_clickableCallbacks) {
				uninitClickableObject(o_key);
			}
		}
	}
}