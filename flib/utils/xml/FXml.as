/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FXml

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			08-08-2014
	ActionScript:	3.0
	Description:	Static class for parsing data from an XML source.
	History:
		11-27-2011:	Started.
		11-15-2013: Updated GetBoolean to accept any type (*) so that undefined could be checked, which will return
			the default parameter.
		08-08-2014: Added GetTriBoolean.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.xml {
	import flash.errors.IllegalOperationError;

	public class FXml {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FXml() {
			super();
			throw(new IllegalOperationError("FXml ctor. This class must not be instantiated."));
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// GetBoolean
		//==================================================================
		public static function GetBoolean(o_value:*, b_default:Boolean = false):Boolean {
			return(o_value != null && o_value != undefined ? String(o_value) == "true" : b_default);
		}
		
		//==================================================================
		// GetInt
		//==================================================================
		public static function GetInt(o_value:Object, i_default:int = 0,
		i_min:int = int.MIN_VALUE, i_max:int = int.MAX_VALUE):int {
			var n_value:Number = parseInt(String(o_value));
			var i_value:int = isNaN(n_value) ? i_default : int(n_value);
			
			if (i_value > i_max) {
				i_value = i_max;
			}
			
			if (i_value < i_min) {
				i_value = i_min;
			}
			
			return(i_value);
		}
		
		//==================================================================
		// GetNumber
		//==================================================================
		public static function GetNumber(o_value:Object, n_default:Number = 0,
		n_min:Number = NaN, n_max:Number = NaN):Number {
			var n_value:Number = parseFloat(String(o_value));
			
			if (isNaN(n_value)) {
				return(n_default);
			}
			
			if (!isNaN(n_min)) {
				n_value = Math.max(n_value, n_min);
			}
			
			if (!isNaN(n_max)) {
				n_value = Math.min(n_value, n_max);
			}
			
			return(n_value);
		}
		
		//==================================================================
		// GetString
		//==================================================================
		public static function GetString(o_value:Object, s_default:String = ""):String {
			var s_value:String = String(o_value);
			return(s_value || s_default);
		}
		
		//==================================================================
		// GetTriBoolean
		//==================================================================
		public static function GetTriBoolean(o_value:*, o_bDefault:Object = null):Object {
			if (o_value == null || o_value == undefined) {
				return(o_bDefault is Boolean ? Boolean(o_bDefault) : null);
			}
			
			return(String(o_value) == "true");
		}
		
		//==================================================================
		// GetUint
		//==================================================================
		public static function GetUint(o_value:Object, u_default:uint = 0,
		u_min:uint = 0, u_max:uint = 0xffffffff):uint {
			var n_value:Number = parseInt(String(o_value));
			var u_value:uint = isNaN(n_value) ? u_default : uint(n_value);
			
			if (u_value > u_max) {
				u_value = u_max;
			}
			
			if (u_value < u_min) {
				u_value = u_min;
			}
			
			return(u_value);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
	}

}