/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FTimerSystem

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			11-07-2017
	ActionScript:	3.0
	Description:	Class for containing multiple FTimer objects.
	History:
		03-18-2012:	Started.
		11-15-2013: Fixed bug in removeTimer, where check for null mo_timers was not begin made.
    11-07-2017: Added addOneTimer as a quick shortcut for adding timers designed to fire only once.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.timers {
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;

	public class FTimerSystem {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		private static var smvo_pool:Vector.<FTimerSystem> = new Vector.<FTimerSystem>();
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		//key: FTimer
		//data: FTimer
		private var mo_timers:Dictionary;
		
		//key: FTimer
		//data: Function
		private var mo_completeCallbacks:Dictionary;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FTimerSystem():void {
			super();
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// addOneTimer
		//==================================================================
		public function addOneTimer(i_delay:Number, f_timerCallbackComplete:Function = null,
		b_autoStart:Boolean = true, b_removeWhenComplete:Boolean = true):FTimer {
      return(addTimer(i_delay, 1, null, f_timerCallbackComplete, b_autoStart, b_removeWhenComplete));
    }
    
		//==================================================================
		// addTimer
		//==================================================================
		public function addTimer(i_delay:Number, i_repeatCount:int = 0,
		f_timerCallback:Function = null, f_timerCallbackComplete:Function = null,
		b_autoStart:Boolean = true, b_removeWhenComplete:Boolean = true):FTimer {
			mo_timers ||= new Dictionary();
			mo_completeCallbacks ||= new Dictionary();
			
			var o_timer:FTimer = new FTimer(i_delay, i_repeatCount, f_timerCallback,
				onTimerComplete, b_autoStart, b_removeWhenComplete);
			mo_timers[o_timer] = o_timer;
			
			if (f_timerCallbackComplete != null) {
				mo_completeCallbacks[o_timer] = f_timerCallbackComplete;
			}
			
			return(o_timer);
		}
		
		//==================================================================
		// GetTimerSystem
		//==================================================================
		public static function GetTimerSystem():FTimerSystem {
			return(smvo_pool.pop() || new FTimerSystem());
		}
		
		//==================================================================
		// getTimers
		//==================================================================
		public function getTimers():Vector.<FTimer> {
			var vo_timers:Vector.<FTimer> = new Vector.<FTimer>();
			for each (var o_timer:FTimer in mo_timers) {
				vo_timers.push(o_timer);
			}
			return(vo_timers);
		}
		
		//==================================================================
		// pauseTimers
		//==================================================================
		public function pauseTimers(b_paused:Boolean):void {
			for each (var o_timer:FTimer in mo_timers) {
				o_timer.paused = b_paused;
			}
		}
		
		//==================================================================
		// removeAllTimers
		//==================================================================
		public function removeAllTimers():void {
			for each (var o_timer:FTimer in mo_timers) {
				removeTimer(o_timer);
			}
		}
		
		//==================================================================
		// removeTimer
		//==================================================================
		public function removeTimer(o_timer:FTimer):FTimer {
			if (!mo_timers) {
				return(null);
			}
			
			if (mo_timers[o_timer]) {
				delete mo_timers[o_timer];
				o_timer.remove();
				
				delete mo_completeCallbacks[o_timer];
			}
			
			return(null);
		}
		
		//==================================================================
		// RemoveTimerSystem
		//==================================================================
		public static function RemoveTimerSystem(o_system:FTimerSystem):FTimerSystem {
			if (o_system) {
				o_system.removeAllTimers();
				smvo_pool.push(o_system);
			}
			
			return(null);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// onTimerComplete
		//==================================================================
		private function onTimerComplete(o_event:TimerEvent):void {
			var o_timer:FTimer = o_event.currentTarget as FTimer;
			var f_callback:Function = mo_completeCallbacks[o_timer] as Function;
			
			if (f_callback != null) {
				f_callback(o_event);
			}
			
			if (o_timer.removeWhenComplete) {
				removeTimer(o_timer);
			}
		}
	}
}