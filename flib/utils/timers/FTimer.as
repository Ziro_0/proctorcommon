﻿/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FLib - FTimer
	
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			08-10-2012
	ActionScript:	3.0
	Description:	Timer class with extended funtionality. Extends the Timer class.
	History:
		02-05-2009:	Started.
		02-10-2012:
			- Added ability to store timer related callbacks, so they can be cleaned up by the
				FTimer object, instead of the caller.
			- Added removeWhenComplete property, which allows an FTimer to remove itself
				when it catches the TimerEvent.TIMER_COMPLETE event.
		03-11-2012:
			- Added Create method.
		08-10-2012:
			- Bugfix to start method. Removed paused check.
			
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.timers {
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getTimer;

	public class FTimer extends Timer {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//Key and data: FTimer - keeps track of all FTimers
		private static var smo_timers:Dictionary = new Dictionary();
		
		//Key and data: FTimer - keeps track of all running FTimers
		private static var smo_running:Dictionary = new Dictionary();
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		//key: Function
		//data : Function
		private var mo_timerCallbacks:Dictionary;
		
		//key: Function
		//data : Function
		private var mo_completeCallbacks:Dictionary;
		
		private var mn_startTime:Number;
		private var mn_delay:Number;  
		
		private var mb_paused:Boolean;
		private var mb_removeWhenComplete:Boolean;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FTimer(i_delay:Number, i_repeatCount:int = 0,
		f_timerCallback:Function = null, f_timerCallbackComplete:Function = null,
		b_autoStart:Boolean = true, b_removeWhenComplete:Boolean = true):void {
			super(i_delay, i_repeatCount);  
			
			smo_timers[this] = this;
			
			mo_timerCallbacks = new Dictionary();
			mo_completeCallbacks = new Dictionary();
			
			mn_delay = i_delay;  
			
			super.addEventListener(TimerEvent.TIMER, onTimer, false, 0, true);  
			removeWhenComplete = b_removeWhenComplete;
			
			addTimerCallback(f_timerCallback);
			addTimerCompleteCallback(f_timerCallbackComplete);
			
			if (b_autoStart) {
				start();
			}
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// addEventListener
		//==================================================================
		public override function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			if (type == TimerEvent.TIMER) {
				addTimerCallback(listener, useCapture, priority, useWeakReference);
			} else if (type == TimerEvent.TIMER_COMPLETE) {
				addTimerCompleteCallback(listener, useCapture, priority, useWeakReference);
			} else {
				super.addEventListener(type, listener, useCapture, priority, useWeakReference);
			}
		}
		
		//==================================================================
		// addTimerCallback
		//==================================================================
		public function addTimerCallback(f_listener:Function, b_useCapture:Boolean = false,
		i_priority:int = 0, b_useWeakReference:Boolean = true):void {
			if (f_listener != null) {
				mo_timerCallbacks[f_listener] = f_listener;
				super.addEventListener(TimerEvent.TIMER, f_listener, b_useCapture, i_priority,
					b_useWeakReference);
			}
		}
		
		//==================================================================
		// addTimerCompleteCallback
		//==================================================================
		public function addTimerCompleteCallback(f_listener:Function, b_useCapture:Boolean = false,
		i_priority:int = 0, b_useWeakReference:Boolean = true):void {
			if (f_listener != null) {
				mo_completeCallbacks[f_listener] = f_listener;
				super.addEventListener(TimerEvent.TIMER_COMPLETE, f_listener, b_useCapture, i_priority,
					b_useWeakReference);
			}
		}
		
		//==================================================================
		// Create
		//==================================================================
		public static function Create(i_delay:Number, i_repeatCount:int = 0,
		f_timerCallback:Function = null, f_timerCallbackComplete:Function = null,
		b_autoStart:Boolean = true, b_removeWhenComplete:Boolean = true):FTimer {
			return(new FTimer(i_delay, i_repeatCount, f_timerCallback,
				f_timerCallbackComplete, b_autoStart, b_removeWhenComplete));
		}
		
		//==================================================================
		// delay (get)
		//==================================================================
		override public function get delay():Number {
			return(mn_delay);
		}
		
		//==================================================================
		// delay (set)
		//==================================================================
		override public function set delay(i_delay:Number):void {
			if (!isNaN(i_delay) && (i_delay >= 0)) {
				mn_delay = i_delay;
			}
			super.delay = i_delay;
		}
		
		//==================================================================
		// PauseAll
		//==================================================================
		public static function PauseAll(b_pause:Boolean):void {
			for each (var o_timer:FTimer in smo_running) {
				o_timer.paused = b_pause;
			}
		}
		
		//==================================================================
		// paused (get)
		//==================================================================
		public function get paused():Boolean {
			return(mb_paused);
		}
		
		//==================================================================
		// paused (set)
		//==================================================================
		public function set paused(b_paused:Boolean):void {
			if (b_paused) {
				if (super.running) {
					super.stop();
					
					//Calculate a "partial" delay, if paused while the timer
					// is running. When unpaused, the timer resumes on the
					// partial delay for only one timer event, then the delay
					// is set to its original delay value.
					var n_delay:Number = mn_delay - (getTimer() - mn_startTime);
					super.delay = n_delay < 0 ? mn_delay : n_delay;
					
					//Update mb_paused AFTER these two super calls, since running is
					// overridden (which uses mb_paused), and super calls use
					// super.running.
					mb_paused = true;
				}
			} else if (mb_paused) {
				start();
			}
		}
		
		//==================================================================
		// remove
		//==================================================================
		public function remove():FTimer {
			var f_callback:Function;
			
			for each (f_callback in mo_timerCallbacks) {
				removeTimerCallback(f_callback);
			}
			
			for each (f_callback in mo_completeCallbacks) {
				removeTimerCompleteCallback(f_callback);
			}
			
			stop();
			delete smo_timers[this];
			
			super.removeEventListener(TimerEvent.TIMER, onTimer);
			super.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			
			return(null);
		}
		
		//==================================================================
		// RemoveAll
		//==================================================================
		public static function RemoveAll():void {
			for each (var o_timer:FTimer in smo_timers) {
				o_timer.remove();
			}
		}
		
		//==================================================================
		// removeEventListener
		//==================================================================
		public override function removeEventListener(type:String, listener:Function,
		useCapture:Boolean = false):void {
			if (type == TimerEvent.TIMER) {
				removeTimerCallback(listener);
			} else if (type == TimerEvent.TIMER_COMPLETE) {
				removeTimerCompleteCallback(listener);
			} else {
				super.removeEventListener(type, listener, useCapture);
			}
		}
		
		//==================================================================
		// removeTimerCallback
		//==================================================================
		public function removeTimerCallback(f_listener:Function):void {
			if (mo_timerCallbacks[f_listener] != null) {
				delete mo_timerCallbacks[f_listener];
				super.removeEventListener(TimerEvent.TIMER, f_listener);
			}
		}
		
		//==================================================================
		// removeTimerCompleteCallback
		//==================================================================
		public function removeTimerCompleteCallback(f_listener:Function):void {
			if (f_listener != null) {
				delete mo_completeCallbacks[f_listener];
				super.removeEventListener(TimerEvent.TIMER_COMPLETE, f_listener);
			}
		}
		
		//==================================================================
		// removeWhenComplete (get)
		//==================================================================
		public function get removeWhenComplete():Boolean {
			return(mb_removeWhenComplete);
		}
		
		//==================================================================
		// removeWhenComplete (set)
		//==================================================================
		public function set removeWhenComplete(b_value:Boolean):void {
			if (mb_removeWhenComplete != b_value) {
				if (b_value) {
					super.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete, false, 0, true);
				} else {
					super.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				}
				
				mb_removeWhenComplete = b_value;
			}
		}
		
		//==================================================================
		// restart
		//==================================================================
		public function restart():void {
			reset();
			start();
		}
		
		//==================================================================
		// running (get)
		//==================================================================
		override public function get running():Boolean {
			return(mb_paused ? true : super.running);
		}
		
		//==================================================================
		// start
		//==================================================================
		override public function start():void {
			var i_repeatCount:int = repeatCount;
			
			mb_paused = false;
			
			//need to check 'repeatCount == 0', so that the timer will still
			// start if the repeat is infinite
			if ((currentCount < i_repeatCount) || (i_repeatCount == 0)) {
				mn_startTime = getTimer();
				super.start();
				smo_running[this] = this;
			}
		}
		
		//==================================================================
		// stop
		//==================================================================
		override public function stop():void {
			mb_paused = false;
			delete smo_running[this];
			super.stop();
		}
		
		//==================================================================
		// Timers (get)
		//==================================================================
		public static function get Timers():Dictionary {
			return(smo_timers);
		}
		
		//==================================================================
		// TimersRunning (get)
		//==================================================================
		public static function get TimersRunning():Dictionary {
			return(smo_running);
		}
		
		//==================================================================
		// UninitTimer
		//==================================================================
		public static function UninitTimer(o_timer:FTimer):FTimer {
			if (o_timer) {
				o_timer.remove();
				o_timer = null;
			}
			
			return(null);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// onTimer
		//==================================================================
		private function onTimer(o_event:TimerEvent):void  {  
			mn_startTime = getTimer();
			super.delay = mn_delay;
		}
		
		//==================================================================
		// onTimerComplete
		//==================================================================
		private function onTimerComplete(o_event:TimerEvent):void {
			if (mb_removeWhenComplete) {
				remove();
			}
		}
	}
}