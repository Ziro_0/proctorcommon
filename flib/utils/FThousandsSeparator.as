/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FThousandsSeparator

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			04-03-2017
	ActionScript:	3.0
	Description:	Regular expression for parsing numbers to produce strings with the thousands
		separator.
	History:
		05-23-2013:	Started.
		01-22-2014: Added support for decimal places.
    04-03-2017: Added useTrailingZeros property. If true (default) values will look like this: "123.45".
      If false, values will look like: "123".
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils {
	import flash.errors.MemoryError;
	import flash.globalization.LastOperationStatus;
	import flash.globalization.LocaleID;
	import flash.globalization.NumberFormatter;

	public final class FThousandsSeparator {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		private const DEFAULT_DELIM:String = ",";
		private const REPLACE_BASE:String = "$&";
		
		private var mo_regEx:RegExp;
		private var ms_replace:String;
		private var ms_delimiter:String;
    private var mb_useTrailingZeros:Boolean;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FThousandsSeparator():void {
			mo_regEx = /\d{1,3}(?=(\d{3})+(?!\d))/g;
			ms_delimiter = DEFAULT_DELIM;
			ms_replace = REPLACE_BASE + ms_delimiter;
      mb_useTrailingZeros = true;
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// delimiter (get)
		//==================================================================
		public function get delimiter():String {
			return(ms_delimiter);
		}
		
		//==================================================================
		// delimiter (set)
		//==================================================================
		public function set delimiter(s_value:String):void {
			ms_delimiter = s_value || DEFAULT_DELIM;
			ms_replace = REPLACE_BASE + ms_delimiter;
		}
		
		//==================================================================
		// parse
		//==================================================================
		public function parse(n_value:Number):String {
			var o_formatter:NumberFormatter;
			var s_value:String;
			
			if (isNaN(n_value)) {
				return("");
			}
			
			o_formatter = new NumberFormatter(LocaleID.DEFAULT);
			o_formatter.leadingZero = true;
			o_formatter.trailingZeros = mb_useTrailingZeros;
			o_formatter.useGrouping = true;
			o_formatter.groupingPattern = "3;*";
			
			try {
				s_value = o_formatter.formatNumber(n_value);
			} catch (o_error:MemoryError) {
				return(n_value.toString());
			}
			
			if (o_formatter.lastOperationStatus != LastOperationStatus.NO_ERROR) {
				return(n_value.toString());
			}
			
			return(s_value);
		}
		
    //==================================================================
    // useTrailingZeros (get)
    //==================================================================
    public function get useTrailingZeros():Boolean {
      return(mb_useTrailingZeros);
    }
    
    //==================================================================
    // useTrailingZeros (set)
    //==================================================================
    public function set useTrailingZeros(b_value:Boolean):void {
      mb_useTrailingZeros = b_value;
    }
    
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
	}
}