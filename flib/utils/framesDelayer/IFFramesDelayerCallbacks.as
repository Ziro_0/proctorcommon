/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	IFFramesDelayerCallbacks

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			07-15-2015
	ActionScript:	3.0
	Description:	
	History:
		05-14-2015:	Started.
		07-15-2015: Added data param to frameDelayerOnComplete
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.framesDelayer {
	
	////////////////////////////////////////////////////////////////////
	//==================================================================
	// interface
	//==================================================================
	////////////////////////////////////////////////////////////////////
	public interface IFFramesDelayerCallbacks {
		function frameDelayerOnComplete(u_id:uint, o_data:Object):void;
	}
}