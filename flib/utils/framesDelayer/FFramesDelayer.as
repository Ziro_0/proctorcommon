/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  FFramesDelayer

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:         07-15-2015
  ActionScript:  3.0
  Description:  
  History:
    05-15-2015: Started.
    07-15-2015: Added data param to ctor.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.framesDelayer {
  import flash.display.Shape;
  import flash.events.Event;

  public class FFramesDelayer {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FRAME_COUNT:uint = 4;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:IFFramesDelayerCallbacks;
    private var mo_shape:Shape;
    private var mu_id:uint;
    private var mu_frameCount:uint;
    private var mo_data:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function FFramesDelayer(u_id:uint, o_callbacks:IFFramesDelayerCallbacks,
    u_frameCount:uint = 0, o_data:Object = null):void {
      super();
      
      mu_id = u_id;
      mu_frameCount = u_frameCount > 0 ? u_frameCount : FRAME_COUNT;
      mo_callbacks = o_callbacks;
      mo_data = o_data;
      
      mo_shape = new Shape();
      mo_shape.addEventListener(Event.ENTER_FRAME, onShapeEnterFrame);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // Initialize
    //==================================================================
    public static function Initialize(u_id:uint, o_callbacks:IFFramesDelayerCallbacks,
    u_frameCount:uint = 0):void {
      var o_delayer:FFramesDelayer = new FFramesDelayer(u_id, o_callbacks, u_frameCount);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onShapeEnterFrame
    //==================================================================
    private function onShapeEnterFrame(o_event:Event):void {
      if (--mu_frameCount == 0) {
        if (mo_callbacks) {
          mo_callbacks.frameDelayerOnComplete(mu_id, mo_data);
        }
        
        uninit();
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    private function uninit():void {
      mo_callbacks = null;
      mo_shape.removeEventListener(Event.ENTER_FRAME, onShapeEnterFrame);
    }
  }
}