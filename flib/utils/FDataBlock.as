/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  FDataBlock

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:         04-13-2018
  ActionScript:  3.0
  Description:  Wrapper class for manipulating key/value data format with string keys.
  History:
    11-12-2012:  Started.
    04-17-2013: Refactored for general use.
    07-20-2015: Code updates.
    09-17-2015: Added getObject method.
    10-24-2015: Added getStringsInArray method.
    11-25-2015: Added createBlock method.
    01-13-2016: Added getBoolean method.
    01-19-2017:
      - Added containsProperty method.
      - Added setProperty method.
      - Added deleteProperty method.
      - Added deleteAllProperties method.
    05-26-2017: Added addProperty.
    05-28-2017: Added getBlock.
    11-28-2017: Added getKeys method.
    04-13-2018: Added getValues method.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils {

  public class FDataBlock {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_data:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function FDataBlock(o_data:Object = null):void {
      super();
      mo_data = o_data;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addProperty
    //==================================================================
    public function addProperty(s_property:String, o_value:Object):void {
      if (mo_data && s_property) {
        mo_data[s_property] = o_value;
      }
    }
    
    //==================================================================
    // containsProperty
    //==================================================================
    public function containsProperty(s_property:String):Boolean {
      return(mo_data != null && s_property in mo_data);
    }
    
    //==================================================================
    // createBlock
    //==================================================================
    public function createBlock(s_property:String):FDataBlock {
      return(new FDataBlock(getObject(s_property)));
    }
    
    //==================================================================
    // data (get)
    //==================================================================
    public function get data():Object {
      return(mo_data);
    }
    
    //==================================================================
    // data (set)
    //==================================================================
    public function set data(o_value:Object):void {
      mo_data = o_value;
    }
    
    //==================================================================
    // deleteAllProperties
    //==================================================================
    public function deleteAllProperties():void {
      for (var s_property:String in mo_data) {
        delete mo_data[s_property];
      }
    }
    
    //==================================================================
    // deleteProperty
    //==================================================================
    public function deleteProperty(o_propertyOrArrayOf:Object):void {
      if (mo_data == null) {
        return;
      }
      
      var s_property:String;
      if (o_propertyOrArrayOf is String) {
        s_property = o_propertyOrArrayOf as String;
        if (containsProperty(s_property)) {
          delete mo_data[s_property];
        }
      } else if (o_propertyOrArrayOf is Array || o_propertyOrArrayOf is Vector.<String>) {
        for each (s_property in o_propertyOrArrayOf) {
          deleteProperty(s_property);
        }
      }
    }
    
    //==================================================================
    // getArray
    //==================================================================
    public function getArray(s_property:String):Array {
      var ao_values:Array;
      
      if (containsProperty(s_property)) {
        ao_values = mo_data[s_property] as Array;
      }
      
      return(ao_values);
    }
    
    //==================================================================
    // getBlock
    //==================================================================
    public function getBlock(s_property:String, o_default:FDataBlock = null):FDataBlock {
      if (containsProperty(s_property)) {
        return(mo_data[s_property] as FDataBlock);
      }
      return(o_default);
    }
    
    //==================================================================
    // getBoolean
    //==================================================================
    public function getBoolean(s_property:String, b_default:Boolean = false):Boolean {
      if (containsProperty(s_property)) {
        return(Boolean(mo_data[s_property]));
      }
      
      return(b_default);
    }
    
    //==================================================================
    // getClass
    //==================================================================
    public function getClass(s_property:String, o_defaultClass:Class = null):Class {
      if (!containsProperty(s_property)) {
        return(null);
      }
      
      var o_class:Class = mo_data[s_property] as Class;
      return(o_class || o_defaultClass);
    }
    
    //==================================================================
    // getClassesInArray
    //==================================================================
    public function getClassesInArray(s_property:String):Array {
      var ao_dataClasses:Array = getArray(s_property);
      if (!ao_dataClasses) {
        return(null);
      }
      
      var ao_classes:Array = new Array();
      for each (var o_elem:Object in ao_dataClasses) {
        if (o_elem is Class) {
          ao_classes.push(o_elem);
        }
      }
      
      return(ao_classes);
    }
    
    //==================================================================
    // getNumber
    //==================================================================
    public function getNumber(s_property:String, n_default:Number = NaN):Number {
      var n_value:Number;
      
      if (containsProperty(s_property)) {
        n_value = Number(mo_data[s_property]);
      }
      
      if (isNaN(n_value)) {
        n_value = n_default;
      }
      
      return(n_value);
    }
    
    //==================================================================
    // getNumbersInArray
    //==================================================================
    public function getNumbersInArray(s_property:String):Array {
      var ao_dataNumbers:Array = getArray(s_property);
      if (!ao_dataNumbers) {
        return(null);
      }
      
      var an_numbers:Array = new Array();
      for each (var o_elem:Object in ao_dataNumbers) {
        if (o_elem is Number) {
          an_numbers.push(o_elem);
        }
      }
      
      return(an_numbers);
    }
    
    //==================================================================
    // getKeys
    //==================================================================
    public function getKeys(as_keys_out:Array = null):Array {
      if (as_keys_out) {
        as_keys_out.length = 0;
      } else {
        as_keys_out = [];
      }
      
      for (var s_key:String in mo_data) {
        as_keys_out.push(s_key);
      }
      
      return(as_keys_out);
    }
    
    //==================================================================
    // getObject
    //==================================================================
    public function getObject(s_property:String, o_default:Object = null):Object {
      if (containsProperty(s_property)) {
        return(mo_data[s_property]);
      }
      
      return(o_default);
    }
    
    //==================================================================
    // getString
    //==================================================================
    public function getString(s_property:String, s_default:String = null):String {
      var s_value:String;
      
      if (containsProperty(s_property)) {
        s_value = mo_data[s_property] as String;
      }
      
      if (!s_value) {
        s_value = s_default;
      }
      
      return(s_value);
    }
    
    //==================================================================
    // getStringsInArray
    //==================================================================
    public function getStringsInArray(s_property:String):Array {
      var as_dataStrings:Array = getArray(s_property);
      if (!as_dataStrings) {
        return(null);
      }
      
      var as_strings:Array = new Array();
      for each (var o_elem:Object in as_dataStrings) {
        if (o_elem is String) {
          as_strings.push(o_elem);
        }
      }
      
      return(as_strings);
    }
    
    //==================================================================
    // getValues
    //==================================================================
    public function getValues(ao_values_out:Array = null):Array {
      if (ao_values_out) {
        ao_values_out.length = 0;
      } else {
        ao_values_out = [];
      }
      
      for (var s_key:String in mo_data) {
        ao_values_out.push(mo_data[s_key]);
      }
      
      return(ao_values_out);
    }
    
    //==================================================================
    // setProperty
    //==================================================================
    public function setProperty(s_property:String, o_value:Object):void {
      if (s_property == null) {
        return;
      }
      
      if (mo_data == null) {
        mo_data = { };
      }
      
      mo_data[s_property] = o_value;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}