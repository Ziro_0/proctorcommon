/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FArrayUtils

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			10-01-2016
	ActionScript:	3.0
	Description:	
	History:
		07-10-2015:	Started.
		09-22-2015: RandomizeEntries now accepts Vector as well as Array.
		09-26-2015: Added Remove.
		09-27-2015: Added CopyEntries and containsMembers.
		10-24-2015:
			- CopyEntries now accepts optional parameters that specify where, in both arrays,
				the operation should begin, and how many elements to copy from the source to the target.
			- Added AppendEntries.
		12-23-2015: Added Random, which returns a random element in an array or vector.
		10-01-2016: Added SwapEntriesAt, which swaps the entries at two indexes.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils {
	import flash.errors.IllegalOperationError;

	public final class FArrayUtils {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FArrayUtils():void {
			super();
			throw(new IllegalOperationError("FArrayUtils ctor. This class must not be instantiated."));
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// AppendEntries
		//==================================================================
		public static function AppendEntries(o_source_array_or_vector:Object,
		o_target_array_or_vector:Object, u_sourceStartIndex:uint = 0,
		u_numSourceElementsToCopy:uint = uint.MAX_VALUE):uint {
			if (!containsMembers(o_target_array_or_vector, "length")) {
				return(0);
			}
			
			return(CopyEntries(o_source_array_or_vector, o_target_array_or_vector, u_sourceStartIndex,
				o_target_array_or_vector.length, u_numSourceElementsToCopy));
		}
		
		//==================================================================
		// CopyEntries
		//==================================================================
		public static function CopyEntries(o_source_array_or_vector:Object,
		o_target_array_or_vector:Object, u_sourceStartIndex:uint = 0, u_targetStartIndex:uint = 0,
		u_numSourceElementsToCopy:uint = uint.MAX_VALUE):uint {
			if (!containsMembers(o_source_array_or_vector, "length")) {
				return(0);
			}
			
			var u_sourceLength:uint = o_source_array_or_vector.length;
			if (u_sourceStartIndex >= u_sourceLength) {
				return(0);
			}
			
			var u_maxCopySourceLen:uint = u_sourceLength - u_sourceStartIndex;
			if (u_numSourceElementsToCopy > u_maxCopySourceLen) {
				u_numSourceElementsToCopy = u_maxCopySourceLen;
			}
			
			if (u_numSourceElementsToCopy == 0) {
				return(0);
			}
			
			if (!containsMembers(o_target_array_or_vector, "length")) {
				return(0);
			}
			
			var u_targetEndIndex:uint = u_targetStartIndex + u_numSourceElementsToCopy;
			if (u_targetEndIndex > o_target_array_or_vector.length) {
				o_target_array_or_vector.length = u_targetEndIndex;
			}
			
			var u_sourceIndex:uint = u_sourceStartIndex;
			var u_targetIndex:uint = u_targetStartIndex;
			for (var u_count:uint = 0; u_count < u_numSourceElementsToCopy; u_count++) {
				o_target_array_or_vector[u_targetIndex++] = o_source_array_or_vector[u_sourceIndex++];
			}
			
			return(u_numSourceElementsToCopy);
		}
		
		//==================================================================
		// Random
		//==================================================================
		public static function Random(o_array_or_vector:Object):Object {
			//sanity checks
			if (!containsMembers(o_array_or_vector, "length")) {
				return(null);
			}
			
			var u_length:uint = uint(o_array_or_vector.length);
			return(u_length > 0 ? o_array_or_vector[uint(Math.random() * u_length)] : null);
		}
		
		//==================================================================
		// RandomizeEntries
		//==================================================================
		public static function RandomizeEntries(o_array_or_vector_in_out:Object,
		b_lastNotFirst:Boolean = false):void {
			//sanity checks
			if (!containsMembers(o_array_or_vector_in_out, "length", "concat", "push")) {
				return;
			}
			
			var u_numEntries:uint = o_array_or_vector_in_out.length;
			if (u_numEntries < 2) {
				//randomization n/a
				return;
			}
			
			var o_lastEntryBeforeMod:Object;
			try {
				o_lastEntryBeforeMod = o_array_or_vector_in_out[u_numEntries - 1];
			} catch (o_error:Error) {
				trace("FArrayUtils.RandomizeEntries. Error: " + o_error.message);
				return;
			}
			
			var o_temps:Object = o_array_or_vector_in_out.concat();
			var u_rndIndex:uint;
			
			o_array_or_vector_in_out.length = 0;
			
			for (var u_index:uint = 0; u_index < u_numEntries; u_index++) {
				//select an entry at random and push onto output array
				u_rndIndex = Math.random() * o_temps.length;
				o_array_or_vector_in_out.push(o_temps[u_rndIndex]);
				
				//remove selected entry
				o_temps[u_rndIndex] = o_temps[o_temps.length - 1];
				o_temps.length--;
			}
			
			if (b_lastNotFirst) {
				//if the first entry of the array after modification is the same as the last entry of the
				// array before modification, swap the positions of the first entry with another entry.
				if (o_array_or_vector_in_out.length > 1) {
					var o_firstEntryAfterMod:Object = o_array_or_vector_in_out[0];
					if (o_firstEntryAfterMod == o_lastEntryBeforeMod) {
						u_rndIndex = 1 + Math.random() * (o_array_or_vector_in_out.length - 1);
						
						var o_temp:Object = o_firstEntryAfterMod;
						o_array_or_vector_in_out[0] = o_array_or_vector_in_out[u_rndIndex];
						o_array_or_vector_in_out[u_rndIndex] = o_temp;
					}
				}
			}
		}
		
		//==================================================================
		// Remove
		//==================================================================
		//Removes an entry from a vector or array. Note that this method cannot be used to remove
		// a null value from the array. Use RemoveAt instead.
		public static function Remove(o_arrayOrVector:Object, o_element:*):Boolean {
			if (o_element == null) {
				return(false);
			}
			
			if (!containsMembers(o_arrayOrVector, "indexOf")) {
				return(false); //sanity checks
			}
			
			var i_index:int = o_arrayOrVector.indexOf(o_element);
			if (i_index == -1) {
				return(false);
			}
			
			return(RemoveAt(o_arrayOrVector, i_index) != null);
		}
		
		//==================================================================
		// RemoveAt
		//==================================================================
		//Removes the entry at the specified object, and returns that entry. Order of elements in
		// the array remains intact.
		public static function RemoveAt(o_arrayOrVector:Object, u_indexToRemove:uint):Object {
			if (!containsMembers(o_arrayOrVector, "length")) {
				return(null); //sanity checks
			}
			
			var u_length:uint = o_arrayOrVector.length;
			if (u_length == 0 || u_indexToRemove >= u_length) {
				return(null); //more sanity checks
			}
			
			var o_entry:Object = o_arrayOrVector[u_indexToRemove];
			for (var u_index:uint = u_indexToRemove + 1; u_index < u_length; u_index++) {
				o_arrayOrVector[u_index - 1] = o_arrayOrVector[u_index];
			}
			
			o_arrayOrVector.length = u_length - 1;
			return(o_entry);
		}
		
		//==================================================================
		// SwapEntriesAt
		//==================================================================
		public static function SwapEntriesAt(o_arrayOrVector:Object, u_index1:uint, u_index2:uint):void {
			if (!containsMembers(o_arrayOrVector, "length")) {
				return; //sanity checks
			}
			
			var u_length:uint = o_arrayOrVector.length;
			if (u_index1 >= u_length || u_index2 >= u_length) {
				return; //more sanity checks
			}
			
			var o_temp:Object = o_arrayOrVector[u_index1];
			o_arrayOrVector[u_index1] = o_arrayOrVector[u_index2];
			o_arrayOrVector[u_index2] = o_temp;
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// containsMembers
		//==================================================================
		private static function containsMembers(o_arrayOrVector:Object, ... o_args):Boolean {
			if (o_arrayOrVector == null) {
				return(false); //sanity check
			}
			
			var ao_args:Array = o_args as Array;
			var u_length:uint = ao_args.length;
			for (var u_index:uint = 0; u_index < u_length; u_index++) {
				var s_member:String = ao_args[u_index] as String;
				if (!(s_member in o_arrayOrVector)) {
					return(false);
				}
			}
			return(true);
		}
	}
}