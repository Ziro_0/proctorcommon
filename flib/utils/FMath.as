﻿/*
  ///////////////////////////////////////////////////////////////////
  ===================================================================
	FLib - FMath
	
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			08-18-2015
	ActionScript:	3.0
	Description:	Collection of functions and data for math-related
		tasks.
	History:
		08-09-2008:	Started. Broke away from FLib for optimization
			purposes.
		09-07-2009: Added Random, which generates a random int between
			a minimum and maximum value, inclusive.
		09-14-2009: Added CalcAngleShortInc.
		09-14-2009: Added SetAngle.
		09-24-2009: Added MoveFlagAngle and related code.
		02-07-2012: Removed all 8-way angle (old-school) functionality.
		02-22-2012: Added InRange method.
		08-18-2015: Refactored code.
  ===================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils {
	import flash.utils.Dictionary;

	public final class FMath {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public static const DEG_TO_RAD:Number = Math.PI / 180;
		public static const RAD_TO_DEG:Number = 180 / Math.PI;
		
		//lookup tables for sin and cosine radian values for angles 0 - 359
		private static var smvn_sin:Vector.<Number>;
		private static var smvn_cos:Vector.<Number>;
		
		//lookup table for radians for angles 0 - 359
		private static var smvn_rad:Vector.<Number>;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// CalcAngle
		//==================================================================
		public static function CalcAngle(n_x1:Number, n_y1:Number, n_x2:Number, n_y2:Number):Number {
			var n_opp:Number = n_y2 - n_y1;
			var n_adj:Number = n_x2 - n_x1;
			var n_ang:Number = (Math.atan2(n_opp, n_adj) * RAD_TO_DEG);
			
			while (n_ang >= 360) {
				n_ang -= 360;
			}
			
			while (n_ang < 0) {
				n_ang += 360;
			}
			
			return(n_ang);
		}
		
		//==================================================================
		// CalcAngleShortInc
		//==================================================================
		/*
			Calculates the sign of the shortest path from an angle to the
				target angle. Returns 1 or -1, or 0, if the angles are equal.
				
				This function is useful for rotating an object in increments,
					and you want the complete rotating to happen in the least
					number of increments.
		*/
		public static function CalcAngleShortInc(i_angle:int, i_targetAngle:int):int {
			if (i_angle == i_targetAngle) {
				return(0);
			}
			
			if (i_angle < i_targetAngle) {
				i_angle += 180;
			} else {
				i_targetAngle += 180;
			}
			
			return(i_angle >= i_targetAngle ? 1 : -1);
		}
		
		//==================================================================
		// Choose
		//==================================================================
		public static function Choose(ai_prop:Array):int {
			/*
				Selects a random number from a range of numbers. The range is
					the length of the specified array, from 0 to length - 1.
				
				ai_prop - An array specifying the probability of a number in the
					range to be selected. A higher probability number has a higher
					chance of being selected. No number in this array must be < 0.
				
				The function returns the array index number that corresponds to
					that probability. For example if ai_prop = [ 10, 20, 40 ],
					then the value 2 (for index 2) has the highest chance of being
					returned. (The value 2 would be returned, not 40).
				
				How the function works (actual implementation varies):
					In this example, assume ai_prop = [4, 2, 30, 19]
					1) Sum all the probabilities.
						4 + 2 + 30 + 19 = 55
					
					2) Get the % of sum for each frequency
						4 / 55
						2 / 55
						30 / 55
						19 / 55
						NOTE: One difference is, in the actual code, the division
							operator is not used.
					
					3) Get a random number from 0 to (sum - 1), and determine which
						"probability area" that the number falls under.
					
					This chart shows the index that would be returned, based of
						the random number generated:
					
					-------------+-------+-------+--------+---------
					index        |   0   |   1   |   2    |   3
					probability  |   4   |   2   |   30   |   19
					range        | [0-3] | [4-5] | [6-35] | [36-54]
					-------------+-------+-------+--------+---------
					
					The index returned is determined by the range in which
						the random number falls.
				
				Returns -1 if the parameter is invalid.
			*/
			var i_l:int = ai_prop ? ai_prop.length : 0;
			if (!i_l) {
				return(-1);
			}
			
			var i_sum:int;
			var i_i:int;
			var i_add:int;
			
			do {
				i_add = int(ai_prop[i_i]);
				if (i_add < 0) {
					return( -1);
				}
				i_sum += i_add;
			} while (++i_i < i_l);
			
			var i_rnd:int = Math.random() * i_sum;
			var i_n:int = ai_prop[0] - 1;
			
			i_i = 0;
			do {
				if (i_rnd <= i_n) {
					//random number was in this range
					return(i_i);
				}
				
				if (++i_i >= i_l) {
					break;
				}
				i_n += ai_prop[i_i];
			} while (true);
			
			return(i_l - 1); //random number was in range of the last index
		}
		
		//==================================================================
		// CopyCos
		//==================================================================
		//Returns the cosine table. The values are in
		// radians for 360 degrees.
		public static function CopyCos():Vector.<Number> {
			initRadTables(); //init if not already done
			return(smvn_cos);
		}
		
		//==================================================================
		// CopyRad
		//==================================================================
		//Returns the radian table.
		public static function CopyRad():Vector.<Number> {
			initRadTables(); //init if not already done
			return(smvn_rad);
		}
		
		//==================================================================
		// CopySin
		//==================================================================
		public static function CopySin():Vector.<Number> {
		//Returns the sine table. The values are in
		// radians for 360 degrees.
			initRadTables(); //init if not already done
			return(smvn_sin);
		}
		
		//==================================================================
		// InRange
		//==================================================================
		public static function InRange(n_value:Number, n_min:Number, n_max:Number):Number {
			if (n_value < n_min) {
				n_value = n_min
			} else if (n_value > n_max) {
				n_value = n_max;
			}
			
			return(n_value);
		}
		
		//==================================================================
		// Random
		//==================================================================
		public static function Random(n_min:Number, n_max:Number):Number {
			if (n_max < n_min) {
				var n_temp:Number = n_max;
				n_max = n_min;
				n_min = n_temp;
			}
			
			return(n_min + Math.random() * (n_max - n_min));
		}
		
		//==================================================================
		// SetAngle
		//==================================================================
		public static function SetAngle(n_angle:Number):Number {
			while (n_angle >= 360) {
				n_angle -= 360;
			}
			
			while (n_angle < 0) {
				n_angle += 360;
			}
			
			return(n_angle);
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// initRadTables
		//==================================================================
		public static function initRadTables():void {
			/*
				Calculates radian tables for sine and cosine.
				
				If an object is to move in a direction (360 degrees, where
					the degree denotes the direction in which the object is
					moving), then increment its coordinates as:
					x += cos[angle]
					y += sin[angle]
			*/
			
			if (smvn_rad) {
				return;
			}
			
			var vn_r:Vector.<Number> = new Vector.<Number>(360, false);
			var vn_s:Vector.<Number> = new Vector.<Number>(360, false);
			var vn_c:Vector.<Number> = new Vector.<Number>(360, false);
			var u_i:int;
			
			do {
				//flip the sign of the a values, since points on the y
				// axis increase as the object moves downward on the screen.
				var n_r:Number = vn_r[u_i] = u_i * DEG_TO_RAD;
				vn_s[u_i] = -Math.sin(n_r);
				vn_c[u_i] = Math.cos(n_r);
			} while (++u_i < 360)
			
			smvn_rad = vn_r;
			
			//Fix these - should be 0, NOT a very small (+/-) number
			vn_s[0] = vn_s[180] = vn_c[90] = vn_c[270] = 0;
			
			smvn_sin = vn_s;
			smvn_cos = vn_c;
		}
	}
}