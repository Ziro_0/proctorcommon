package flib.utils
{
  import flash.geom.Rectangle;
  import flash.system.Capabilities;
  import flash.utils.ByteArray;
  import flash.utils.describeType;
  import flash.utils.getQualifiedClassName;
  
  /**
   * A collection of static utility functions.
   */
  public class ExtUtils
  {
    
    /**************************************************************************
     * CONSTANTS
     **************************************************************************/
    
    /**
     * iOS device group IDs.
     */
    public static const IOS_UNKNOWN :int  = 0;

    public static const IPHONE_1    :int  = 100;
    public static const IPHONE_3G   :int  = 101;
    public static const IPHONE_4    :int  = 102;
    public static const IPHONE_4S   :int  = 103;
    public static const IPHONE_5    :int  = 104;
    public static const IPHONE_5C   :int  = 105;
    public static const IPHONE_5S   :int  = 106;
    public static const IPHONE_6    :int  = 107;

    public static const IPAD_1      :int  = 200;
    public static const IPAD_2      :int  = 201;
    public static const IPAD_MINI_1 :int  = 202;
    public static const IPAD_3      :int  = 203;
    public static const IPAD_4      :int  = 204;
    public static const IPAD_AIR_1  :int  = 205;
    public static const IPAD_MINI_2 :int  = 206;
    public static const IPAD_AIR_2  :int  = 207;
    public static const IPAD_MINI_3 :int  = 208;

    /**
     * All known iPhone/iPad devices with group and description, keyed by Model Identifier.
     * Data from http://www.everyi.com/by-identifier/ipod-iphone-ipad-specs-by-model-identifier.html
     * Note: items with duplicate keys (i.e. "iPhone5,4") are marked with a trailing '*' in the description
     */
    private static const KNOWN_IOS_DEVICES:Object =
    {
      "iPhone1,1" : { grp:IPHONE_1    , dsc:"iPhone (Original/1st Gen/EDGE)" },
      "iPhone1,2" : { grp:IPHONE_3G   , dsc:"iPhone 3G" },
      "iPhone2,1" : { grp:IPHONE_3G   , dsc:"iPhone 3GS" },
      "iPhone1,2*": { grp:IPHONE_3G   , dsc:"iPhone 3G (China/No Wi-Fi)" },
      "iPhone2,1*": { grp:IPHONE_3G   , dsc:"iPhone 3GS (China/No Wi-Fi)" },
      "iPhone3,1" : { grp:IPHONE_4    , dsc:"iPhone 4 (GSM)" },
      "iPhone3,3" : { grp:IPHONE_4    , dsc:"iPhone 4 (CDMA/Verizon/Sprint)" },
      "iPhone4,1" : { grp:IPHONE_4S   , dsc:"iPhone 4S (4s*)" },
      "iPhone4,1*": { grp:IPHONE_4S   , dsc:"iPhone 4S (GSM China/WAPI)" },
      "iPhone5,1" : { grp:IPHONE_5    , dsc:"iPhone 5 (GSM/LTE)*" },
      "iPhone5,2" : { grp:IPHONE_5    , dsc:"iPhone 5 (CDMA)*" },
      "iPhone5,3" : { grp:IPHONE_5C   , dsc:"iPhone 5c (A1532 or A1456)*" },
      "iPhone5,4" : { grp:IPHONE_5C   , dsc:"iPhone 5c (A1507,A1526,A1529, or A1516)*" },
      "iPhone6,1" : { grp:IPHONE_5S   , dsc:"iPhone 5s (A1533 or A1453)*" },
      "iPhone6,2" : { grp:IPHONE_5S   , dsc:"iPhone 5s (A1457,A1528,A1530, or A1518)*" },
      "iPhone7,2" : { grp:IPHONE_6    , dsc:"iPhone 6 (A1549 or A1586)*" },
      "iPhone7,2*": { grp:IPHONE_6    , dsc:"iPhone 6 (China Mobile/A1589)" },
      "iPhone7,1" : { grp:IPHONE_6    , dsc:"iPhone 6 Plus (A1522 or A1524)" },
      "iPhone7,1*": { grp:IPHONE_6    , dsc:"iPhone 6 Plus (China Mobile/A1593)" },
      "iPad1,1"   : { grp:IPAD_1      , dsc:"iPad Wi-Fi (Original/1st Gen) OR iPad Wi-Fi/3G/GPS (Original/1st Gen)" },
      "iPad2,1"   : { grp:IPAD_2      , dsc:"iPad 2 (Wi-Fi Only)" },
      "iPad2,2"   : { grp:IPAD_2      , dsc:"iPad 2 (Wi-Fi/GSM/GPS)" },
      "iPad2,3"   : { grp:IPAD_2      , dsc:"iPad 2 (Wi-Fi/CDMA/GPS)" },
      "iPad2,4"   : { grp:IPAD_2      , dsc:"iPad 2 (Wi-Fi Only, iPad2,4)" },
      "iPad2,5"   : { grp:IPAD_MINI_1 , dsc:"iPad mini (Wi-Fi Only/1st Gen)" },
      "iPad2,6"   : { grp:IPAD_MINI_1 , dsc:"iPad mini (Wi-Fi/AT&T/GPS - 1st Gen)" },
      "iPad2,7"   : { grp:IPAD_MINI_1 , dsc:"iPad mini (Wi-Fi/VZ & Sprint/GPS - 1st Gen)" },
      "iPad3,1"   : { grp:IPAD_3      , dsc:"iPad 3rd Gen (Wi-Fi Only)" },
      "iPad3,3"   : { grp:IPAD_3      , dsc:"iPad 3rd Gen (Wi-Fi/Cellular AT&T/GPS)" },
      "iPad3,2"   : { grp:IPAD_3      , dsc:"iPad 3rd Gen (Wi-Fi/Cellular Verizon/GPS)" },
      "iPad3,4"   : { grp:IPAD_4      , dsc:"iPad 4th Gen (Wi-Fi Only)" },
      "iPad3,5"   : { grp:IPAD_4      , dsc:"iPad 4th Gen (Wi-Fi/AT&T/GPS)" },
      "iPad3,6"   : { grp:IPAD_4      , dsc:"iPad 4th Gen (Wi-Fi/Verizon & Sprint/GPS)" },
      "iPad4,1"   : { grp:IPAD_AIR_1  , dsc:"iPad Air (Wi-Fi Only)" },
      "iPad4,2"   : { grp:IPAD_AIR_1  , dsc:"iPad Air (Wi-Fi/Cellular)" },
      "iPad4,3"   : { grp:IPAD_AIR_1  , dsc:"iPad Air (Wi-Fi/TD-LTE - China)" },
      "iPad4,4"   : { grp:IPAD_MINI_2 , dsc:"iPad mini (Retina/2nd Gen - Wi-Fi Only)" },
      "iPad4,5"   : { grp:IPAD_MINI_2 , dsc:"iPad mini (Retina/2nd Gen - Wi-Fi/Cellular)" },
      "iPad4,6"   : { grp:IPAD_MINI_2 , dsc:"iPad mini (Retina/2nd Gen - China)" },
      "iPad5,3"   : { grp:IPAD_AIR_2  , dsc:"iPad Air 2 (Wi-Fi Only)" },
      "iPad5,4"   : { grp:IPAD_AIR_2  , dsc:"iPad Air 2 (Wi-Fi/Cellular)" },
      "iPad4,7"   : { grp:IPAD_MINI_3 , dsc:"iPad mini 3 (Wi-Fi Only)" },
      "iPad4,8"   : { grp:IPAD_MINI_3 , dsc:"iPad mini 3 (Wi-Fi/Cellular)" }
    };
    
    private static const UNKNOWN_IOS_DEVICE:Object = { grp:IOS_UNKNOWN, dsc:"iOS unknown??" };

    /**************************************************************************
     * STRINGS
     **************************************************************************/

    /**
     * Converts an integer to a string and left pads it with zeros.
     * 
     * @param integer  The integer to convert
     * @param width    The desired string width  
     */
    public static function zeroPad(integer:int, width:int):String {
      return leftPadString(String(integer),width,'0');
    }
    
    /**
     * Left pads a string to the given width using the given pad character.
     */
    public static function leftPadString(string:String, width:int, padCharacter:String=' '):String {
      var ret:String = string + "";
      while(ret.length < width) { ret=padCharacter+ret; }
      return ret;
    }
    
    /**
     * Right pads a string to the given width using the given pad character.
     */
    public static function rightPadString(string:String, width:int, padCharacter:String=' '):String {
      var ret:String = string + "";
      while(ret.length < width) { ret=ret+padCharacter; }
      return ret;
    }
    
    /**
     * Determines whether the specified string begins with the specified prefix.
     */
    public static function beginsWith(input:String, prefix:String):Boolean {
      return (prefix == input.substring(0, prefix.length));
    }
    
    /**
     * Determines whether the specified string ends with the specified suffix.
     */
    public static function endsWith(input:String, suffix:String):Boolean {
      return (suffix == input.substring(input.length - suffix.length));
    }
    
    /**
     * @return The input string truncated to the given length, with trailing "..." suffix.
     */
    public static function truncate(string:String, length:int):String {
      if(string==null) { return string; }
      if(string.length > length) {
        var sfx:String = "...";
        if(length < sfx.length) { return sfx; }
        string = string.slice(0,length-sfx.length);
        string += sfx;
      }
      return string;
    }

    /**
     * Tests if a string contains a valid numeric value.
     * @param string
     * @return true if the string is a number, false otherwise
     */
    public static function isNumeric(string:String):Boolean
    {
      return !isNaN(Number(string));
    }

    /**
     * @return a random string of alpha numeric characters
     *
     * @param length      The length of the string to generate
     * @param mixedCase   If true, then upper and lowercase characters will used
     *                    If false, then only uppercase characters will be used
     */
    public static function randomAlphaNumericString(length:int=10, mixedCase:Boolean=true):String
    {
      var dgtarr:String = "0123456789";                       // digits array
      var uprarr:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";       // uppercase characters array
      var lwrarr:String = "abcdefghijklmnopqrstuvwxyz";       // lowercase characters array
      var smparr:String;                                      // sample characters array (used to build random string)
      var rndstr:String = "";                                 // random string

      // build sample characters array
      smparr = dgtarr + uprarr;
      if(mixedCase) { smparr += lwrarr; }

      // build a random string using the characters in the sample array
      var smparrmaxidx:int = smparr.length - 1;
      for(var xa:int=0; xa<length; xa++) {
        rndstr += smparr.charAt(randomInt(0,smparrmaxidx));
      }

      // return the new random string
      return rndstr;
    }
    
    /**************************************************************************
     * NUMBERS
     **************************************************************************/
    
    /**
     * Clamps an int value between min and max.
     */
    public static function clampInt(val:int, min:int, max:int):int {
      if(val<min) { return min; }
      if(val>max) { return max; }
      return val;
    }

    /**
     * Clamps an Number value between min and max.
     */
    public static function clampNumber(val:Number, min:Number, max:Number):Number {
      if(isNaN(val)) { return min; }
      if(val<min) { return min; }
      if(val>max) { return max; }
      return val;
    }

    /**
     * Generates a random integer between min (inclusive) and max (inclusive).
     */
    public static function randomInt(min:int, max:int):int {
      if(max<min) { return min; }
      if(min>max) { return max; }
      return Math.floor(min + (Math.random() * (max - min + 1)));
    }
    
    /**************************************************************************
     * TIME
     **************************************************************************/
    
    /**
     * Converts seconds to a HH:MM:SS string.
     * Modified from: http://www.codebelt.com/actionscript-3/as3-convert-seconds-to-hours-minutes-seconds/
     */
    public static function convertToHHMMSS(seconds:Number):String
    {
      var s:Number = seconds % 60;
      var m:Number = Math.floor((seconds % 3600 ) / 60);
      var h:Number = Math.floor(seconds / 3600);
      
      var hourStr:String = (h == 0) ? "" : doubleDigitFormat(h) + ":";
      var minuteStr:String = doubleDigitFormat(m) + ":";
      var secondsStr:String = doubleDigitFormat(s);
      
      return hourStr + minuteStr + secondsStr;

      // helper function
      function doubleDigitFormat(nbr:uint):String {
        if(nbr<10) { return ("0" + nbr); }
        return String(nbr);
      }
    }
    
    /**************************************************************************
     * ARRAYS
     **************************************************************************/

    /**
     * Returns a random element from this array
     */
    public static function randomElement(array:Array):Object {
      if(!array || array.length==0) { return null; }
      return array[Math.floor(Math.random()*array.length)];
    }

    /**************************************************************************
     * OBJECTS
     **************************************************************************/
    
    /**
     * Returns the Class object of the given object.
     */
    public static function getClass(object:Object):Class {
      var clsnam:String = flash.utils.getQualifiedClassName(object);
      return flash.utils.getDefinitionByName(clsnam) as Class;
    }

    /**
     *   Count the properties in an object
     *   @param object Object to count the properties of
     *   @return The number of properties in the specified object.
     */
    public static function numProperties(object:Object):int
    {
      var cnt:int = 0;
      for each(var prp:Object in object) { cnt++; }
      return cnt;
    }

    /**
     * @return a random property (value) of this Object, or null if there are no properties
     */
    public static function randomProperty(object:Object):Object
    {
      if(!object) { return null; }
      var prpcnt:int = numProperties(object);           // get total property count
      if(prpcnt==0) { return null; }
      var rndidx:int = ExtUtils.randomInt(0,prpcnt-1);      // choose a random index
      var idx:int = 0;
      for each(var prp:Object in object) {              // iterate to the selected property and return it
        if(idx==rndidx) { return prp; }
        idx++;
      }
      return null;
    }

    /**
     * Deep clones an object by serializing to a ByteArray and then creating a new object off of the ByteArray.
     * From http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee7.html
     */
    public static function clone(source:Object):*
    { 
      if(source==null) { return null; }
      var myBA:ByteArray = new ByteArray();
      myBA.writeObject(source);
      myBA.position = 0;
      return(myBA.readObject());
    }
    
    /**
     * Checks recursively if two objects have identical properties.
     * Supported data types: Object, Array, Number, Boolean, String, null, undefined
     * Adapted from http://stackoverflow.com/questions/15816008/in-as3-how-to-check-if-two-json-objects-are-equal
     */
    public static function areObjectsEqual(a:*, b:*):Boolean
    {
      if(a == null /* null or undefined */ || a is Number || a is Boolean || a is String) {
        // compare primitive values
        return a === b;
      }
      else {
        // non-primitive types: compare Arrays and Objects only
        var clsnamA:String = getQualifiedClassName(a);
        if(clsnamA!="Array" && clsnamA!="Object") { trace("warning: unsupported type for object comparison: " + clsnamA); return false; }
        
        // class names must match
        var clsnamB:String = getQualifiedClassName(b);
        if(clsnamA!=clsnamB) { return false; }
        
        // compare all properties
        var prp:*;
        for(prp in a) {
          // check if a and b have different values for prp
          if(a.hasOwnProperty(prp) && !b.hasOwnProperty(prp)) { return false; }
          if(!areObjectsEqual(a[prp], b[prp])) { return false; }
        }
        for(prp in b) {
          // check if b has a value which a does not
          if(!a.hasOwnProperty(prp)) { return false; }
        }
        return true;
      }
    }

    /**************************************************************************
     * XML
     **************************************************************************/

    /**
     * @return an XML attribute value or the default value if it is not defined
     */
    public static function xmlAttribute(xml:XML, attributeName:String, defaultValue:*):* {
      return ((('@'+attributeName) in xml) ? xml.attribute(attributeName) : defaultValue);
    }

    /**************************************************************************
     * GEOMETRY
     **************************************************************************/
    
    /**
     * Tests if two given rectangles intersect.
     * No objects are created (unlike flash.geom.Rectangle.intersects() method)
     */
    public static function intersects(r1:Rectangle, r2:Rectangle):Boolean
    {
      return (r1.x < r2.x + r2.width  &&
              r2.x < r1.x + r1.width  &&
              r1.y < r2.y + r2.height &&
              r2.y < r1.y + r1.height );
    }
    
    /**************************************************************************
     * DEVICE DETECTION
     **************************************************************************/
    
    /**
     * Returns true if running in a Flash Player plugin inside a browser.
     */
    public static function isBrowserPlugin():Boolean {
      return (Capabilities.playerType=='PlugIn' || Capabilities.playerType=='ActiveX');
    }
    
    /**
     * Returns true if running on a mobile (ios or android) device.
     */
    public static function isMobile():Boolean {
      return(isAndroid() || isIos());
    }
    
    /**
     * Returns true if running on Android.
     * 
     * Capabilities.manufacturer note:
     *   Nabi2 and Kindle Fire HDX both report 'Android Linux'
     */
    public static function isAndroid():Boolean {
      return (Capabilities.manufacturer.indexOf('Android') != -1);
    }

    /**
     * Returns true if running on tvOS.
     *
     * Capabilities.os note:
     *   Example tvOS os string format: 'tvOS 9.2.2 AppleTV5,3'
     */
    public static function isTvOS():Boolean {
      return (Capabilities.os.indexOf('tvOS') != -1);
    }

    /**
     * Returns true if running on iOS.
     * 
     * Capabilities.os note:
     *   Expected iOS os string format: 'iPhone OS 7.0.4 iPad2,3'
     */
    public static function isIos():Boolean {
      return (Capabilities.os.substr(0,6) == 'iPhone');
    }
    
    /**
     * Returns true if running on an iPad (any resolution).
     */
    public static function isIPad():Boolean {
      return isIos() && (Capabilities.os.indexOf('iPad') != -1);
    }

    /**
     * Returns true if on iPad Retina.
     */
    public static function isIpadRetina():Boolean {
      return isIPad() && (Capabilities.screenResolutionX > 2000 || Capabilities.screenResolutionY > 2000);
    }

    /**
     * Returns the info object (with device group and description) for this iOS device.
     * Returns null if not running on an iOS device.
     * Returns the UNKNOWN_IOS_DEVICE object if this iOS device is not recognized.
     */    
    public static function getIosDeviceInfo():Object
    {
      if(!isIos()) { return null; } // make sure we're running on iOS!
      
      // search for model identifier in os description string
      for(var mdlidn:String in KNOWN_IOS_DEVICES) {
        if(Capabilities.os.indexOf(mdlidn) > -1) { return KNOWN_IOS_DEVICES[mdlidn]; }
      }
      
      // no known model identifier found 
      return UNKNOWN_IOS_DEVICE;
    }

    /**************************************************************************
     * DEBUG
     **************************************************************************/
    
    /**
     * Displays all properties / methods for an object.
     */
    public static function dumpObject(object:Object):void
    {
      var dsc:XML = describeType(object);
      
      trace("Properties:\n------------------");
      for each (var a:XML in dsc.accessor) { trace(a.@name+" : "+a.@type); }
      
      trace("\n\nMethods:\n------------------");
      for each (var m:XML in dsc.method) {
        trace(m.@name+" : "+m.@returnType);
        if (m.parameter != undefined) {
          trace("     arguments");
          for each (var p:XML in m.parameter) { trace("               - "+p.@type); }
        }
      }    
    }
    
    /**
     * Displays full object description XML from describeType().
     */
    public static function dumpObjectDescription(object:Object):void
    {
      var dsc:XML = describeType(object);
      trace(dsc);
    }
    
    /**
     * Displays many device capabilities.
     */
    public static function dumpCapabilities():void
    {
      trace("OS ................ [" + Capabilities.os                 + "]");
      trace("manufacturer ...... [" + Capabilities.manufacturer       + "]");
      trace("playerType ........ [" + Capabilities.playerType         + "]");
      trace("cpuArchitecture ... [" + Capabilities.cpuArchitecture    + "]");
      trace("version ........... [" + Capabilities.version            + "]");
      trace("screenDPI ......... [" + Capabilities.screenDPI          + "]");
      trace("screenResolutionX . [" + Capabilities.screenResolutionX  + "]");
      trace("screenResolutionY . [" + Capabilities.screenResolutionY  + "]");
      trace("serverString ...... [" + Capabilities.serverString       + "]");
    }

  }
}