/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	fGetItemCompClass

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			11-27-2014
	ActionScript:	3.0
	Description:	
	History:
		11-27-2014: Started.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.pckgLvl {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	public function fGetItemCompClass(o_containerOrDisplay:Object, s_displayName:String = null):Class {
		var o_display:DisplayObject;
		if (o_containerOrDisplay is DisplayObjectContainer) {
			if (s_displayName) {
				o_display = (o_containerOrDisplay as DisplayObjectContainer).getChildByName(s_displayName);
				if (!o_display) {
					return(null);
				}
			}
		}
		
		o_display ||= o_containerOrDisplay as DisplayObject;
		if (!o_display) {
			return(null);
		}
		
		var s_className:String = getQualifiedClassName(o_display);
		return(getDefinitionByName(s_className) as Class);
	}
}