/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FLib - fIsClass
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			07-12-2010
	ActionScript:	3.0
	Description:	Package-level function for determining if one class extends
		another class, or implements an interface.
	History:
		07-12-2010:	Started.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.pckgLvl {
	import flash.utils.describeType;
	import flash.utils.getQualifiedClassName;

	////////////////////////////////////////////////////////////////////
	//==================================================================
	// static / const
	//==================================================================
	////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////
	//==================================================================
	// public functions
	//==================================================================
	////////////////////////////////////////////////////////////////////
	
	//==================================================================
	// fIsClass
	//==================================================================
	public function fIsClass(o_classToCheck:Class, o_isObject:Object):Boolean {
		if (!o_classToCheck || !o_isObject) {
			return(false);
		} else if (o_classToCheck == o_isObject) {
			return(true);
		}
		
		var s_clsName:String = getQualifiedClassName(o_isObject);
		var o_xmlDesc:XML = describeType(o_classToCheck),
				o_xmlFactory:XMLList = o_xmlDesc.factory;
		return((o_xmlFactory.extendsClass.(@type == s_clsName).length() != 0) ||
			(o_xmlFactory.implementsInterface.(@type == s_clsName).length() != 0));
	}
}