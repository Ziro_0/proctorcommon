﻿/*
  ///////////////////////////////////////////////////////////////////
  ===================================================================
	Function Library - fWordFromCode
	
	Author:				Zir0
	Date:   			08-09-2008
	ActionScript:	3.0
	Description:	Similar to String.fromCharCode, except a word string
		is returned for non alpha-numeric codes.
	History:
		08-09-2008:	Started. Broke away from FLib for optimization
			purposes.
  ===================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.pckgLvl {
	import flib.input.FKey;
	
	public function fWordFromCode(i_kc:int):String {
		/*
			i_kc - The key code to use.
		*/
			
		var s_w:String;
		if ((FKey.NUMBER_0 <= i_kc) && (i_kc <= FKey.NUMBER_9)) {
				//number keys
			s_w = String.fromCharCode(i_kc);
		} else if ((FKey.NUMPAD_0 <= i_kc) && (i_kc <= FKey.NUMPAD_9)) {
				//numpad keys (number keys only)
			s_w = "#" + String.fromCharCode(i_kc - FKey.NUMBER_0);
		} else {
			switch (i_kc) {
				case FKey.BACKSPACE: s_w = "BCKSP"; break;
				case FKey.TAB: s_w = "TAB"; break;
				case FKey.ENTER: s_w = "ENTER"; break;
				case FKey.SPACE: s_w = "SPACE"; break;
				case FKey.LEFT: s_w = "LEFT"; break;
				case FKey.UP: s_w = "UP"; break;
				case FKey.RIGHT: s_w = "RIGHT"; break;
				case FKey.DOWN: s_w = "DOWN"; break;
				case FKey.NUMPAD_MULTIPLY: s_w = "#*"; break;
				case FKey.NUMPAD_ADD: s_w = "#+"; break;
				case FKey.NUMPAD_ENTER: s_w = "#ENTR"; break;
				case FKey.NUMPAD_SUBTRACT: s_w = "#-"; break;
				case FKey.NUMPAD_DECIMAL: s_w = "#."; break;
				case FKey.NUMPAD_DIVIDE: s_w = "#/"; break;
				default:
					s_w = String.fromCharCode(i_kc);
			}
		}
		
		return(s_w);
	}
}