﻿/*
  ///////////////////////////////////////////////////////////////////
  ===================================================================
	Function Library - fPadDigits
	
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			08-09-2008
	ActionScript:	3.0
	Description:	Routine for padding a number with a specfic digit,
		such as zeros, similar to a scoreboard.
	History:
		08-09-2008:	Started. Broke away from FLib.
		11-27-2008: Added param i_base to fPadDigits function.
  ===================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.utils.pckgLvl {
	
	public function fPadDigits(p_value:Number, i_numChars:uint,
	s_padChar:String = "0", i_base:int = 10):String {
		var s_result:String = p_value.toString(i_base),
				s_pads:String = "";
		
		var i_i:int = s_result.length,
				i_l:int = i_numChars;
		while (i_i++ < i_l) {
			s_pads += s_padChar;
		}
		
		return(s_pads + s_result);
	}
}