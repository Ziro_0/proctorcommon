﻿/*
  ///////////////////////////////////////////////////////////////////
  ===================================================================
	FLib - FSound
	
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			03-16-2012
	ActionScript:	3.0
	Description:	Collection of functions and data for playing sounds, including
		support for 2D sounds by calculating a sound's volume and pan based on it's position to
		a listener.
	History:
		09-15-2009:	Started.
		02-22-2011:
			- Added optional o_channels parameter to Run2D, which allows only one
				SoundChannel (if o_channels is a SoundChannel) or a group of SoundChannels (if
				o_channel is an Array) to be updated.
			- Added Stop method.
		05-13-2011:
			- Changed parameters of Play method, so the sound saource could now either be a Class
				which constructs a Sound object, or an array of such Class objects.
		11-01-2011:
			- Changed parameters of Play method, so that a Sound object can now be included
				(in addition to Class or an Array of Class and/or Sound objects).
		03-16-2012:
			- Added optional startTime param to Play and Play2D methods.
			- Removed removeWhenDone param from various methods.
  ===================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.audio {
	import flash.events.Event;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	import flib.utils.FMath;

	public final class FSound {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//Key: Class
		//Data: Sound
		private static var smo_soundClasses:Dictionary;
		
		//Key: SoundChannel
		//Data: soundSource2D
		private static var smo_2DSndSources:Dictionary;
		
		private static var smo_sndTrnsfrm:SoundTransform;
		private static var smap_panAngs:Array;
		
		private static var smo_objListener:Object;
		
		private static var smp_maxDist:Number = 10;
		private static var smp_maxDistDiv:Number = 1 / smp_maxDist;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// Add2DSoundChannel
		//==================================================================
		public static function Add2DSoundChannel(o_objSource:Object, o_chnl:SoundChannel,
		p_baseVol:Number = 1.0, p_chDistance:Number = 0):void {
			var o_sndSource:soundSource2D = new soundSource2D(o_objSource, o_chnl, p_baseVol,
				p_chDistance);
			
			smo_2DSndSources[o_chnl] = o_sndSource;
			o_chnl.addEventListener(Event.SOUND_COMPLETE, onSndDone2DChnl, false, 0, true);
		}
		
		//==================================================================
		// BaseVol2DSoundChannel
		//==================================================================
		public static function BaseVol2DSoundChannel(o_chnl:SoundChannel):Number {
			var o_sndSource:soundSource2D = smo_2DSndSources[o_chnl];
			return(o_sndSource ? o_sndSource.mp_baseVol : 0);
		}
		
		//==================================================================
		// Get2DChannelVolume
		//==================================================================
		public static function Get2DChannelVolume(o_channel:SoundChannel):Number {
			var o_source:soundSource2D = smo_2DSndSources[o_channel];
			return(o_source ? o_source.mp_baseVol : 0);
		}
		
		//==================================================================
		// Init2D
		//==================================================================
		public static function Init2D(o_objListener:Object, p_maxDistance:Number):void {
			init();
			ListenerObject = o_objListener;
			MaxDistance = p_maxDistance;
		}
		
		//==================================================================
		// ListenerObject (get)
		//==================================================================
		public static function get ListenerObject():Object {
			return(smo_objListener);
		}
		
		//==================================================================
		// ListenerObject (set)
		//==================================================================
		public static function set ListenerObject(o_value:Object):void {
			smo_objListener = o_value;
		}
		
		//==================================================================
		// MaxDistance (get)
		//==================================================================
		public static function get MaxDistance():Number {
			return(smp_maxDist);
		}
		
		//==================================================================
		// MaxDistance (set)
		//==================================================================
		public static function set MaxDistance(p_value:Number):void {
			if (!isNaN(p_value) && (p_value >= 0)) {
				smp_maxDist = p_value;
				smp_maxDistDiv = (p_value > 0 ? 1 / smp_maxDist : 0);
				Run2D();
			}
		}
		
		//==================================================================
		// Num2DSoundChannels (get)
		//==================================================================
		public static function get Num2DSoundChannels():int {
			var i_ct:int;
			for each (var o_sndSource:soundSource2D in smo_2DSndSources) {
				i_ct++;
			}
			
			return(i_ct);
		}
		
		//==================================================================
		// Play
		//==================================================================
		public static function Play(o_soundSourceClsOrArray:Object, p_volume:Number = 1.0,
		p_pan:Number = 0, i_loops:int = 0, f_sndDone:Function = null, n_startTime:Number = 0,
		o_sound_out:Object = null):SoundChannel {
			init();
			
			var o_sndClass:Class;
			var o_snd:Sound;
			
			if (o_soundSourceClsOrArray is Class) {
				o_sndClass = o_soundSourceClsOrArray as Class;
			} else if (o_soundSourceClsOrArray is Sound) {
				o_snd = o_soundSourceClsOrArray as Sound;
			} else if (o_soundSourceClsOrArray is Array) {
				var ao_soundClasses:Array = o_soundSourceClsOrArray as Array;
				var i_len:uint = ao_soundClasses.length;
				if (i_len > 0) {
					var i_idx:uint = Math.random() * i_len,
							o_source:Object = ao_soundClasses[i_idx];
							
					o_sndClass = o_source as Class;
					if (!o_sndClass) {
						o_snd = o_source as Sound;
					}
				}
			}
			
			if (!o_snd) {
				o_snd = getSound(o_sndClass);
				if (!o_snd) {
					return(null);
				}
			}
			
			smo_sndTrnsfrm.pan = p_pan;
			smo_sndTrnsfrm.volume = p_volume;
			var o_ch:SoundChannel = o_snd.play(n_startTime, i_loops, smo_sndTrnsfrm);
			
			if (o_ch) {
				if (f_sndDone != null) {
					o_ch.addEventListener(Event.SOUND_COMPLETE, f_sndDone, false, 0, true);
				}
				
				if (o_sound_out != null) {
					o_sound_out.sound = o_snd;
				}
			}
			
			return(o_ch);
		}
		
		//==================================================================
		// Play2D
		//==================================================================
		public static function Play2D(o_soundSourceClsOrArray:Object, o_objSource:Object,
		p_volume:Number = 1.0, i_loops:int = 0, f_sndDone:Function = null,
		p_chDistance:Number = 0, n_startTime:Number = 0, o_sound_out:Object = null):SoundChannel {
			if (!o_objSource || !smo_objListener) {
				return(null);
			}
			
			calc2DTrnsfrm(o_objSource, p_volume, p_chDistance);
			
			var o_chnl:SoundChannel = Play(o_soundSourceClsOrArray, smo_sndTrnsfrm.volume,
				smo_sndTrnsfrm.pan, i_loops, f_sndDone, n_startTime, o_sound_out);
				
			if (o_chnl) {
				Add2DSoundChannel(o_objSource, o_chnl, p_volume, p_chDistance);
			}
			
			return(o_chnl);
		}
		
		//==================================================================
		// Remove2DSoundChannel
		//==================================================================
		public static function Remove2DSoundChannel(o_chnl:SoundChannel):void {
			o_chnl.removeEventListener(Event.SOUND_COMPLETE, onSndDone2DChnl);
			delete smo_2DSndSources[o_chnl];
		}
		
		//==================================================================
		// Run2D
		//==================================================================
		public static function Run2D(o_channels:Object = null):void {
			if (!smo_objListener) {
				return;
			}
			
			var o_sndSrc:soundSource2D;
			
			if (o_channels is Array) {
				for each (var o_channel:SoundChannel in o_channels) {
					o_sndSrc = smo_2DSndSources[o_channels];
					calc2DTrnsfrm(o_sndSrc.mo_objSource, o_sndSrc.mp_baseVol, o_sndSrc.mp_chDistance);
					o_sndSrc.mo_chnl.soundTransform = smo_sndTrnsfrm;
				}
			} else if (o_channels is SoundChannel) {
				o_sndSrc = smo_2DSndSources[o_channels];
				calc2DTrnsfrm(o_sndSrc.mo_objSource, o_sndSrc.mp_baseVol, o_sndSrc.mp_chDistance);
				o_sndSrc.mo_chnl.soundTransform = smo_sndTrnsfrm;
			} else {
				for each (o_sndSrc in smo_2DSndSources) {
					calc2DTrnsfrm(o_sndSrc.mo_objSource, o_sndSrc.mp_baseVol, o_sndSrc.mp_chDistance);
					o_sndSrc.mo_chnl.soundTransform = smo_sndTrnsfrm;
				}
			}
		}
		
		//==================================================================
		// Set2DChannelVolume
		//==================================================================
		public static function Set2DChannelVolume(o_channel:SoundChannel, p_volume:Number):void {
			var o_source:soundSource2D = smo_2DSndSources[o_channel];
			
			if (!o_source) {
				return;
			}
			
			o_source.mp_baseVol = p_volume;
			calc2DTrnsfrm(o_source.mo_objSource, o_source.mp_baseVol, o_source.mp_chDistance);
			o_channel.soundTransform = smo_sndTrnsfrm;
		}
		
		//==================================================================
		// Stop
		//==================================================================
		public static function Stop(o_channel:SoundChannel):void {
			if (o_channel) {
				Remove2DSoundChannel(o_channel);
				o_channel.stop();
			}
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// calc2DTrnsfrm
		//==================================================================
		private static function calc2DTrnsfrm(o_objSource:Object, p_volume:Number,
		p_chDistance:Number = 0):void {
			var p_2dVol:Number,
					p_2dPan:Number;
					
			if (o_objSource == smo_objListener) {
				p_2dVol = p_volume;
				p_2dPan = 0;
			} else {
				var p_xLst:Number = smo_objListener.x,
						p_yLst:Number = smo_objListener.y,
						p_xSrc:Number = o_objSource.x,
						p_ySrc:Number = o_objSource.y;
				
				//calc distance (for volume)
				var i_dx:int = p_xSrc - p_xLst,
						i_dy:int = p_ySrc - p_yLst,
						p_dist:Number = Math.sqrt((i_dx * i_dx) + (i_dy * i_dy));
				
				p_2dVol = ((smp_maxDist + p_chDistance - p_dist) * smp_maxDistDiv) * p_volume;
				
				if (p_2dVol > 1) {
					p_2dVol = 1;
				} else if (p_2dVol < 0) {
					p_2dVol = 0;
				}
				
				if (p_dist == 0) {
					//no distance between source and listener - center panning
					p_2dPan = 0;
				} else {
					//calc angle (for panning)
					var	i_angle:int = FMath.CalcAngle(p_xSrc, p_ySrc, p_xLst, p_yLst);
					p_2dPan = smap_panAngs[i_angle];
				}
			}
			
			smo_sndTrnsfrm.volume = p_2dVol;
			smo_sndTrnsfrm.pan = p_2dPan;
		}
		
		//==================================================================
		// getSound
		//==================================================================
		private static function getSound(o_sndClass:Class):Sound {
			var o_snd:Sound = smo_soundClasses[o_sndClass];
			if (!o_snd) {
				if (o_sndClass) {
					o_snd = new o_sndClass();
					smo_soundClasses[o_sndClass] = o_snd;
				}
			}
			
			return(o_snd);
		}
		
		//==================================================================
		// init
		//==================================================================
		private static function init():void {
			if (!smo_soundClasses) {
				smo_soundClasses = new Dictionary();
				smo_sndTrnsfrm = new SoundTransform();
				smo_2DSndSources = new Dictionary();
				
				smap_panAngs = new Array(360);
				var i:int = 0;
				do {
					smap_panAngs[i] = i / 90 - 1;
				} while (++i < 180);
				
				do {
					smap_panAngs[i] = -i / 90 + 3;
				} while (++i < 360);
			}
		}
		
		//==================================================================
		// onSndDone2DChnl
		//==================================================================
		private static function onSndDone2DChnl(o_evt:Event):void {
			var o_chnl:SoundChannel = o_evt.target as SoundChannel;
			Remove2DSoundChannel(o_chnl);
		}
	}
}
import flash.geom.Point;
import flash.media.SoundChannel;


////////////////////////////////////////////////////////////////////
//==================================================================
// internal class - soundSource2D
//==================================================================
////////////////////////////////////////////////////////////////////
internal class soundSource2D {
	public var mo_objSource:Object;
	public var mo_chnl:SoundChannel;
	public var mp_baseVol:Number;
	public var mp_chDistance:Number;
	
	public function soundSource2D(o_objSource:Object, o_chnl:SoundChannel,
	p_baseVol:Number = 1.0, p_chDistance:Number = 0):void {
		mo_objSource = o_objSource;
		mo_chnl = o_chnl;
		mp_baseVol = p_baseVol;
		mp_chDistance = p_chDistance;
	}
}