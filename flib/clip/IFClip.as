/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	IFClip

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			03-06-2013
	ActionScript:	3.0
	Description:	Interface for FClip objects
	History:
		03-06-2013:	Started.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.clip {
	
	////////////////////////////////////////////////////////////////////
	//==================================================================
	// interface
	//==================================================================
	////////////////////////////////////////////////////////////////////
	public interface IFClip {
		function fclipFrameCallback(o_clip:FClip):void;
	}
}