/*
  ///////////////////////////////////////////////////////////////////
  ===================================================================
	Function Library - fClipPlayAll
	
	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			03-18-2013
	ActionScript:	3.0
	Description:	Package level method for playing a MovieClip, and recursively playing
		any children that may be MovieClip objects.
	History:
		03-18-2013:	Started. Broke away from FClip to remove static method.
  ===================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.clip {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	public function fClipPlayAll(o_container:DisplayObjectContainer):void {
		var o_child:DisplayObjectContainer;
		var i_index:int;
		
		if (!o_container) {
			return;
		}
		
		if (o_container is MovieClip) {
			MovieClip(o_container).play();
		}
		
		while (i_index < o_container.numChildren) {
			o_child = o_container.getChildAt(i_index++) as DisplayObjectContainer;
			if (o_child) {
				fClipPlayAll(o_child);
			}
		}
	}
}