﻿/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FLib - FClip

	Author:				Cartrell (Ziro)
		zir0@sbcglobal.net
		https://www.upwork.com/users/~0180f72de0fb06b175
	Date:   			08-20-2015
	ActionScript:	3.0
	Description:	Adds some extra functionality to the MovieClip class.
		- MovieClips are stopped by default.
		- A MovieClip can remove itself from its parent with the remove function.
		- Ability to play clips in reverse, as well as obtain play status
		- Ability to loop playing when the playhead has reached either end.
		- Dispatches Event.COMPLETE when the clip is done playing.
		- The function supplied to addFrameScript accepts one parameter, the FClip object itself.
		- The MovieClip method, addFrameScript, is "deprecated".
		
	History:
		09-18-2009:	Started.
		10-02-2009: Added SortChildrenByY (source:
			http://www.senocular.com/flash/tutorials/as3withflashcs3/?page=2)
		11-13-2012: Moved method SortChildrenByY to FDisplay.
		03-06-2013:
			- Added new IFClip interface, which is used for frame scripts.
			- Original addFrameScript is "deprecated", and throws an exception if the method is
				called.
			- Changed FClip from inheritence to composition, so it can be used with existing
				MovieClip objects. Adde get/set clip properties.
		03-18-2013:
			- Refactored static methods PlayAll and StopAll into package-level functions
				fClipPlayAll and fClipStopAll, respectively.
		04-26-2013:
			- Fixed bug in onFrame, where an out of bounds array index was being accessed
				on the running clips if an FClip was removed during its onFrame.
		07-23-2013:
			- Added status fix to play method.
		09-16-2013:
			- Added visible property.
		07-29-2015: Can now specify a MovieClip or a class that constructs a MovieClip.
		08-20-2015: Refactored code.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.clip {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	public class FClip {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public static const LOOP_NONE:int = 0;
		public static const LOOP_NORMAL:int = 1;
		public static const LOOP_BI:int = 2;
		
		private static var smo_frameRunner:Shape;
		
		//data: FClip
		private static var smao_runningClips:Array;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		private var mo_clip:MovieClip;
		
		//key: frame (int or String)
		//data: (true)
		private var mo_callbackFrames:Dictionary;
		
		private var mo_callbacks:IFClip;
		
		private var mi_looping:int;
		
		private var mb_reverse:Boolean;
		private var mb_playing:Boolean;
		private var mb_inFrame:Boolean;
		private var mb_toDelete:Boolean;
		private var mb_toUninit:Boolean;
		private var mb_visible:Boolean;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		public function FClip(o_clipOrClass:Object, o_callbacks:IFClip = null):void {
			super();
			mb_visible = true;
			setClip(o_clipOrClass);
			mo_callbacks = o_callbacks;
			mo_callbackFrames = new Dictionary();
			mi_looping = LOOP_NONE;
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// addFrameScript
		//==================================================================
		public function addFrameScript(... ao_args):void {
			throw(new IllegalOperationError("FClip.addFrameCallback. MovieClip.addFrameScript is " +
				"effed up. One of the reasons I coded FClip in the first place is to get AWAY from " +
				"broke-@$$ methods like it. Please use the setFrameCallback " +
				"method. I hate the MovieClip object as it is. Adobe needs to recode the MovieClip " +
				"class it from SCRATCH, " +
				"instead of messing around with all this AIR, shcmobile, and sandbox bull$hit every " +
				"Flash Player release. But, that's typical of big corporations - always coming out " +
				"with new $hit, instead of fixing the current $hit."));
		}
		
		//==================================================================
		// callbacks (get)
		//==================================================================
		public function get callbacks():IFClip {
			return(mo_callbacks);
		}
		
		//==================================================================
		// callbacks (set)
		//==================================================================
		public function set callbacks(o_callbacks:IFClip):void {
			mo_callbacks = o_callbacks;
		}
		
		//==================================================================
		// clip (get)
		//==================================================================
		public function get clip():MovieClip {
			return(mo_clip);
		}
		
		//==================================================================
		// currentFrame (get)
		//==================================================================
		public function get currentFrame():int {
			return(mo_clip ? mo_clip.currentFrame : 0);
		}
		
		//==================================================================
		// currentFrameLabel (get)
		//==================================================================
		public function get currentFrameLabel():String {
			return(mo_clip ? mo_clip.currentFrameLabel : null);
		}
		
		//==================================================================
		// gotoAndPlay
		//==================================================================
		public function gotoAndPlay(o_frame:Object, s_scene:String = null):void {
			var b_callback:Boolean;
			var i_frame:int;
			
			if (!mo_clip) {
				return;
			}
			
			i_frame = mo_clip.currentFrame;
			mo_clip.gotoAndStop(o_frame, s_scene);
			play();
			
			if (mo_clip.currentFrame != i_frame) {
				b_callback = mo_callbackFrames[currentFrame] as Boolean ||
					mo_callbackFrames[currentFrameLabel] as Boolean;
				if (b_callback && mo_callbacks) {
					mo_callbacks.fclipFrameCallback(this);
				}
			}
		}
		
		//==================================================================
		// gotoAndStop
		//==================================================================
		public function gotoAndStop(o_frame:Object, s_scene:String = null):void {
			var b_callback:Boolean
			var i_frame:int;
			
			if (!mo_clip) {
				return;
			}
			
			i_frame = mo_clip.currentFrame;
			mo_clip.gotoAndStop(o_frame, s_scene);
			stop();
			
			if (mo_clip.currentFrame != i_frame) {
				b_callback = mo_callbackFrames[currentFrame] as Boolean ||
					mo_callbackFrames[currentFrameLabel] as Boolean;
				if (b_callback && mo_callbacks) {
					mo_callbacks.fclipFrameCallback(this);
				}
			}
		}
		
		//==================================================================
		// looping (get)
		//==================================================================
		public function get looping():int {
			return(mi_looping);
		}
		
		//==================================================================
		// looping (set)
		//==================================================================
		public function set looping(i_value:int):void {
			mi_looping = i_value;
		}
		
		//==================================================================
		// play
		//==================================================================
		public function play():void {
			if (!mo_clip) {
				return;
			}
			
			mb_toDelete = mb_toUninit = false;
			
			if (!mb_playing) {
				mb_playing = true;
				addToRunningClips();
			}
		}
		
		//==================================================================
		// playing (get)
		//==================================================================
		public function get playing():Boolean {
			return(mb_playing);
		}
		
		//==================================================================
		// reverse (get)
		//==================================================================
		public function get reverse():Boolean {
			return(mb_reverse);
		}
		
		//==================================================================
		// reverse (set)
		//==================================================================
		public function set reverse(b_value:Boolean):void {
			mb_reverse = b_value;
		}
		
		//==================================================================
		// setClip
		//==================================================================
		public function setClip(o_clipOrClass:Object):void {
			var o_clip:MovieClip;
			if (o_clipOrClass is MovieClip) {
				o_clip = o_clipOrClass as MovieClip;
			} else if (o_clipOrClass is Class) {
				try {
					o_clip = new o_clipOrClass() as MovieClip;
				} catch (o_error:Error) {
					trace("FClip.clip. Error: " + o_error.message);
					return;
				}
			}
			
			if (mo_clip == o_clip) {
				return;
			}
			
			if (mo_clip) {
				stop();
			}
			
			mo_clip = o_clip;
			
			if (mo_clip) {
				mo_clip.visible = mb_visible;
				mo_clip.stop();
			}
		}
		
		//==================================================================
		// setCurrentFrame
		//==================================================================
		public function setCurrentFrame(o_frame:Object):void {
			var i_frame:int;
			var b_callback:Boolean;
			
			if (!mo_clip) {
				return;
			}
			
			i_frame = mo_clip.currentFrame;
			mo_clip.gotoAndStop(o_frame);
			
			if (mo_clip.currentFrame != i_frame) {
				b_callback = mo_callbackFrames[currentFrame] as Boolean ||
					mo_callbackFrames[currentFrameLabel] as Boolean;
				if (b_callback && mo_callbacks) {
					mo_callbacks.fclipFrameCallback(this);
				}
			}
		}
		
		//==================================================================
		// setFrameCallback
		//==================================================================
		public function setFrameCallback(o_frame:Object, b_add:Boolean):Boolean {
			if (!((o_frame is uint) || (o_frame is String))) {
				return(false);
			}
			
			if (b_add) {
				mo_callbackFrames[o_frame] = true;
			} else {
				delete mo_callbackFrames[o_frame];
			}
			
			return(true);
		}
		
		//==================================================================
		// stop
		//==================================================================
		public function stop():void {
			if (!mo_clip) {
				return;
			}
			
			if (mb_playing) {
				mb_playing = false;
				removeFromRunningClips();
			}
		}
		
		//==================================================================
		// totalFrames (get)
		//==================================================================
		public function get totalFrames():int {
			return(mo_clip ? mo_clip.totalFrames : 0);
		}
		
		//==================================================================
		// uninit
		//==================================================================
		public function uninit():void {
			if (mb_inFrame) {
				mb_toUninit = true;
			} else {
				uninitInner();
			}
		}
		
		//==================================================================
		// visible (get)
		//==================================================================
		public function get visible():Boolean {
			return(mb_visible);
		}
		
		//==================================================================
		// visible (set)
		//==================================================================
		public function set visible(b_value:Boolean):void {
			mb_visible = b_value;
			
			if (mo_clip) {
				mo_clip.visible = mb_visible;
			}
		}
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		//==================================================================
		// addToRunningClips
		//==================================================================
		private function addToRunningClips():void {
			var i_length:int;
			
			if (!smo_frameRunner) {
				smo_frameRunner = new Shape();
			}
			
			if (smao_runningClips) {
				i_length = smao_runningClips.length;
				if (smao_runningClips.indexOf(this) == -1) {
					smao_runningClips.push(this);
				}
			} else {
				smao_runningClips = new Array();
				smao_runningClips.push(this);
			}
			
			if (!i_length) {
				smo_frameRunner.addEventListener(Event.ENTER_FRAME, onFrameRunnerEnterFrame,
					false, 0, true);
			}
		}
		
		//==================================================================
		// frame
		//==================================================================
		private function frame():void {
			var i_frame:int = mo_clip.currentFrame;
			var b_endReached:Boolean;
			var b_callback:Boolean;
			
			mb_inFrame = true;
			
			if (mb_playing) {
				if (mb_reverse) {
					if (mo_clip.currentFrame > 1) {
						mo_clip.gotoAndStop(currentFrame - 1);
					} else {
						b_endReached = true;
					}
				} else {
					if (mo_clip.currentFrame < mo_clip.totalFrames) {
						mo_clip.gotoAndStop(mo_clip.currentFrame + 1);
					} else {
						b_endReached = true;
					}
				}
				
				if (b_endReached) {
					switch (mi_looping) {
						case LOOP_NORMAL:
							if (mb_reverse) {
								setCurrentFrame(mo_clip.totalFrames);
							} else {
								setCurrentFrame(1);
							}
							break;
							
						case LOOP_BI:
							mb_reverse = !mb_reverse;
							break;
							
						case LOOP_NONE:
						default:
							stop();
					}
				}
			}
			
			if (mo_clip.currentFrame != i_frame) {
				b_callback = mo_callbackFrames[currentFrame] as Boolean ||
					mo_callbackFrames[currentFrameLabel] as Boolean;
				if (b_callback && mo_callbacks) {
					mo_callbacks.fclipFrameCallback(this);
				}
			}
			
			mb_inFrame = false;
		}
		
		//==================================================================
		// onFrameRunnerEnterFrame
		//==================================================================
		private static function onFrameRunnerEnterFrame(o_event:Event):void {
			var ao_clips:Array = smao_runningClips;
			var u_length:uint = ao_clips.length;
			var o_clip:FClip;
			var u_index:uint;
			
			for (u_index = 0; u_index < u_length; ) {
				o_clip = ao_clips[u_index] as FClip;
				o_clip.frame();
				
				if (o_clip.mb_toUninit) {
					if (o_clip.uninitInner()) {
						u_length--;
					} else {
						u_index++;
					}
				} else if (o_clip.mb_toDelete) {
					if (o_clip.removeFromRunningClips()) {
						u_length--;
					} else {
						u_index++;
					}
				} else {
					u_index++;
				}
			}
		}
		
		//==================================================================
		// removeFromRunningClips
		//==================================================================
		private function removeFromRunningClips():Boolean {
			var i_index:int;
			var i_length:int;
			var b_removed:Boolean;
			
			if (mb_inFrame) {
				mb_toDelete = true;
				return(b_removed);
			}
			
			mb_toDelete = false;
			
			if (smao_runningClips) {
				i_index = smao_runningClips.indexOf(this);
				if (i_index > -1) {
					i_length = smao_runningClips.length;
					smao_runningClips[i_index] = smao_runningClips[--i_length];
					smao_runningClips.length = i_length;
					b_removed = true;
				}
			}
			
			if (smo_frameRunner) {
				if (i_length == 0) {
					smo_frameRunner.removeEventListener(Event.ENTER_FRAME, onFrameRunnerEnterFrame);
				}
			}
			
			return(b_removed);
		}
		
		//==================================================================
		// uninitInner
		//==================================================================
		private function uninitInner():Boolean {
			var o_key:Object;
			var b_removed:Boolean;
			
			/*
			if (mb_playing) {
				mb_playing = false;
				b_removed = removeFromRunningClips();
			}
			*/
			
			mb_playing = false;
			b_removed = removeFromRunningClips();
			
			mo_clip = null;
			mo_callbackFrames = null;
			mb_inFrame = mb_toDelete = mb_toUninit = false;
			
			for (o_key in mo_callbackFrames) {
				delete mo_callbackFrames[o_key];
			}
			
			return(b_removed);
		}
	}
}