﻿/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FKey
	Author:				Cartrell (Ziro)
		https://www.gameplaycoder.com
	Date:   			04-07-2008
	ActionScript:	3.0
	Description:	Constants for keyboard codes.
	History:
		04-07-2008: Started.
  ====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.input {
	public final class FKey {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		/**
		 * Constant associated with the key code value for the A key (65).
		 */
		public static const A:int = 65;
		/**
		 * Constant associated with the key code value for the Alternate (Option) key (18).
		 */
		public static const ALTERNATE:int = 18;
		/**
		 * Constant associated with the key code value for the B key (66).
		 */
		public static const B:int = 66;
		/**
		 * Constant associated with the key code value for the ` key (192).
		 */
		public static const BACKQUOTE:int = 192;
		/**
		 * Constant associated with the key code value for the \ key (220).
		 */
		public static const BACKSLASH:int = 220;
		/**
		 * Constant associated with the key code value for the Backspace key (8).
		 */
		public static const BACKSPACE:int = 8;
		/**
		 * Constant associated with the key code value for the C key (67).
		 */
		public static const C:int = 67;
		/**
		 * Constant associated with the key code value for the Caps Lock key (20).
		 */
		public static const CAPS_LOCK:int = 20;
		/**
		 * Constant associated with the key code value for the , key (188).
		 */
		public static const COMMA:int = 188;
		/**
		 * Constant associated with the Mac command key (15).  This constant is currently only used for setting menu key equivalents.
		 */
		public static const COMMAND:int = 15;
		/**
		 * Constant associated with the key code value for the Control key (17).
		 */
		public static const CONTROL:int = 17;
		/**
		 * Constant associated with the key code value for the D key (68).
		 */
		public static const D:int = 68;
		/**
		 * Constant associated with the key code value for the Delete key (46).
		 */
		public static const DELETE:int = 46;
		/**
		 * Constant associated with the key code value for the Down Arrow key (40).
		 */
		public static const DOWN:int = 40;
		/**
		 * Constant associated with the key code value for the E key (69).
		 */
		public static const E:int = 69;
		/**
		 * Constant associated with the key code value for the End key (35).
		 */
		public static const END:int = 35;
		/**
		 * Constant associated with the key code value for the Enter key (13).
		 */
		public static const ENTER:int = 13;
		/**
		 * Constant associated with the key code value for the = key (187).
		 */
		public static const EQUAL:int = 187;
		/**
		 * Constant associated with the key code value for the Escape key (27).
		 */
		public static const ESCAPE:int = 27;
		/**
		 * Constant associated with the key code value for the F key (70).
		 */
		public static const F:int = 70;
		/**
		 * Constant associated with the key code value for the F1 key (112).
		 */
		public static const F1:int = 112;
		/**
		 * Constant associated with the key code value for the F10 key (121).
		 */
		public static const F10:int = 121;
		/**
		 * Constant associated with the key code value for the F11 key (122).
		 */
		public static const F11:int = 122;
		/**
		 * Constant associated with the key code value for the F12 key (123).
		 */
		public static const F12:int = 123;
		/**
		 * Constant associated with the key code value for the F13 key (124).
		 */
		public static const F13:int = 124;
		/**
		 * Constant associated with the key code value for the F14 key (125).
		 */
		public static const F14:int = 125;
		/**
		 * Constant associated with the key code value for the F15 key (126).
		 */
		public static const F15:int = 126;
		/**
		 * Constant associated with the key code value for the F2 key (113).
		 */
		public static const F2:int = 113;
		/**
		 * Constant associated with the key code value for the F3 key (114).
		 */
		public static const F3:int = 114;
		/**
		 * Constant associated with the key code value for the F4 key (115).
		 */
		public static const F4:int = 115;
		/**
		 * Constant associated with the key code value for the F5 key (116).
		 */
		public static const F5:int = 116;
		/**
		 * Constant associated with the key code value for the F6 key (117).
		 */
		public static const F6:int = 117;
		/**
		 * Constant associated with the key code value for the F7 key (118).
		 */
		public static const F7:int = 118;
		/**
		 * Constant associated with the key code value for the F8 key (119).
		 */
		public static const F8:int = 119;
		/**
		 * Constant associated with the key code value for the F9 key (120).
		 */
		public static const F9:int = 120;
		/**
		 * Constant associated with the key code value for the G key (71).
		 */
		public static const G:int = 71;
		/**
		 * Constant associated with the key code value for the H key (72).
		 */
		public static const H:int = 72;
		/**
		 * Constant associated with the key code value for the Home key (36).
		 */
		public static const HOME:int = 36;
		/**
		 * Constant associated with the key code value for the I key (73).
		 */
		public static const I:int = 73;
		/**
		 * Constant associated with the key code value for the Insert key (45).
		 */
		public static const INSERT:int = 45;
		/**
		 * Constant associated with the key code value for the J key (74).
		 */
		public static const J:int = 74;
		/**
		 * Constant associated with the key code value for the K key (75).
		 */
		public static const K:int = 75;
		/**
		 * Constant associated with the key code value for the L key (76).
		 */
		public static const L:int = 76;
		/**
		 * Constant associated with the key code value for the Left Arrow key (37).
		 */
		public static const LEFT:int = 37;
		/**
		 * Constant associated with the key code value for the [ key (219).
		 */
		public static const LEFTBRACKET:int = 219;
		/**
		 * Constant associated with the key code value for the M key (77).
		 */
		public static const M:int = 77;
		/**
		 * Constant associated with the key code value for the - key (189).
		 */
		public static const MINUS:int = 189;
		/**
		 * Constant associated with the key code value for the N key (78).
		 */
		public static const N:int = 78;
		/**
		 * Constant associated with the key code value for the 0 key (48).
		 */
		public static const NUMBER_0:int = 48;
		/**
		 * Constant associated with the key code value for the 1 key (49).
		 */
		public static const NUMBER_1:int = 49;
		/**
		 * Constant associated with the key code value for the 2 key (50).
		 */
		public static const NUMBER_2:int = 50;
		/**
		 * Constant associated with the key code value for the 3 key (51).
		 */
		public static const NUMBER_3:int = 51;
		/**
		 * Constant associated with the key code value for the 4 key (52).
		 */
		public static const NUMBER_4:int = 52;
		/**
		 * Constant associated with the key code value for the 5 key (53).
		 */
		public static const NUMBER_5:int = 53;
		/**
		 * Constant associated with the key code value for the 6 key (54).
		 */
		public static const NUMBER_6:int = 54;
		/**
		 * Constant associated with the key code value for the 7 key (55).
		 */
		public static const NUMBER_7:int = 55;
		/**
		 * Constant associated with the key code value for the 8 key (56).
		 */
		public static const NUMBER_8:int = 56;
		/**
		 * Constant associated with the key code value for the 9 key (57).
		 */
		public static const NUMBER_9:int = 57;
		/**
		 * Constant associated with the pseudo-key code for the the number pad (21).  Use to
		 *  set numpad modifier on key equivalents
		 */
		public static const NUMPAD:int = 21;
		/**
		 * Constant associated with the key code value for the number 0 key on the number pad (96).
		 */
		public static const NUMPAD_0:int = 96;
		/**
		 * Constant associated with the key code value for the number 1 key on the number pad (97).
		 */
		public static const NUMPAD_1:int = 97;
		/**
		 * Constant associated with the key code value for the number 2 key on the number pad (98).
		 */
		public static const NUMPAD_2:int = 98;
		/**
		 * Constant associated with the key code value for the number 3 key on the number pad (99).
		 */
		public static const NUMPAD_3:int = 99;
		/**
		 * Constant associated with the key code value for the number 4 key on the number pad (100).
		 */
		public static const NUMPAD_4:int = 100;
		/**
		 * Constant associated with the key code value for the number 5 key on the number pad (101).
		 */
		public static const NUMPAD_5:int = 101;
		/**
		 * Constant associated with the key code value for the number 6 key on the number pad (102).
		 */
		public static const NUMPAD_6:int = 102;
		/**
		 * Constant associated with the key code value for the number 7 key on the number pad (103).
		 */
		public static const NUMPAD_7:int = 103;
		/**
		 * Constant associated with the key code value for the number 8 key on the number pad (104).
		 */
		public static const NUMPAD_8:int = 104;
		/**
		 * Constant associated with the key code value for the number 9 key on the number pad (105).
		 */
		public static const NUMPAD_9:int = 105;
		/**
		 * Constant associated with the key code value for the addition key on the number pad (107).
		 */
		public static const NUMPAD_ADD:int = 107;
		/**
		 * Constant associated with the key code value for the decimal key on the number pad (110).
		 */
		public static const NUMPAD_DECIMAL:int = 110;
		/**
		 * Constant associated with the key code value for the division key on the number pad (111).
		 */
		public static const NUMPAD_DIVIDE:int = 111;
		/**
		 * Constant associated with the key code value for the Enter key on the number pad (108).
		 */
		public static const NUMPAD_ENTER:int = 108;
		/**
		 * Constant associated with the key code value for the multiplication key on the number pad (106).
		 */
		public static const NUMPAD_MULTIPLY:int = 106;
		/**
		 * Constant associated with the key code value for the subtraction key on the number pad (109).
		 */
		public static const NUMPAD_SUBTRACT:int = 109;
		/**
		 * Constant associated with the key code value for the O key (79).
		 */
		public static const O:int = 79;
		/**
		 * Constant associated with the key code value for the P key (80).
		 */
		public static const P:int = 80;
		/**
		 * Constant associated with the key code value for the Page Down key (34).
		 */
		public static const PAGE_DOWN:int = 34;
		/**
		 * Constant associated with the key code value for the Page Up key (33).
		 */
		public static const PAGE_UP:int = 33;
		/**
		 * Constant associated with the key code value for the . key (190).
		 */
		public static const PERIOD:int = 190;
		/**
		 * Constant associated with the key code value for the Q key (81).
		 */
		public static const Q:int = 81;
		/**
		 * Constant associated with the key code value for the ' key (222).
		 */
		public static const QUOTE:int = 222;
		/**
		 * Constant associated with the key code value for the R key (82).
		 */
		public static const R:int = 82;
		/**
		 * Constant associated with the key code value for the Right Arrow key (39).
		 */
		public static const RIGHT:int = 39;
		/**
		 * Constant associated with the key code value for the ] key (221).
		 */
		public static const RIGHTBRACKET:int = 221;
		/**
		 * Constant associated with the key code value for the S key (83).
		 */
		public static const S:int = 83;
		/**
		 * Constant associated with the key code value for the ; key (186).
		 */
		public static const SEMICOLON:int = 186;
		/**
		 * Constant associated with the key code value for the Shift key (16).
		 */
		public static const SHIFT:int = 16;
		/**
		 * Constant associated with the key code value for the / key (191).
		 */
		public static const SLASH:int = 191;
		/**
		 * Constant associated with the key code value for the Spacebar (32).
		 */
		public static const SPACE:int = 32;
		/**
		 * Constant associated with the key code value for the T key (84).
		 */
		public static const T:int = 84;
		/**
		 * Constant associated with the key code value for the Tab key (9).
		 */
		public static const TAB:int = 9;
		/**
		 * Constant associated with the key code value for the U key (85).
		 */
		public static const U:int = 85;
		/**
		 * Constant associated with the key code value for the Up Arrow key (38).
		 */
		public static const UP:int = 38;
		/**
		 * Constant associated with the key code value for the V key (86).
		 */
		public static const V:int = 86;
		/**
		 * Constant associated with the key code value for the W key (87).
		 */
		public static const W:int = 87;
		/**
		 * Constant associated with the key code value for the X key (88).
		 */
		public static const X:int = 88;
		/**
		 * Constant associated with the key code value for the Y key (89).
		 */
		public static const Y:int = 89;
		/**
		 * Constant associated with the key code value for the Z key (90).
		 */
		public static const Z:int = 90;
	}
}