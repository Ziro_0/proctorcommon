﻿/*
	////////////////////////////////////////////////////////////////////
	====================================================================
	FAscii
	Author:				Cartrell (Ziro)
		https://www.gameplaycoder.com
	Date:   			05-30-2010
	ActionScript:	3.0
	Description:	Contains constant values for ASCII codes 8-10, 13, and 32-127.
	History:
		05-30-2010:	Started.
	====================================================================
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package flib.input {

	public final class FAscii {
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// static / const
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		public static const BACKSPACE:uint = 8;
		public static const TAB:uint = 9;
		public static const LINEFEED:uint = 10;
		public static const CARRIAGE_RETURN:uint = 13;
		public static const SPACE:uint = 32;
		public static const EXCLAMATION:uint = 33;
		public static const QUOTATION:uint = 34;
		public static const NUMBER:uint = 35;
		public static const DOLLAR:uint = 36;
		public static const PERCENT:uint = 37;
		public static const AMPERSAND:uint = 38;
		public static const APOSTROPHE:uint = 39;
		public static const PAREN_LEFT:uint = 40;
		public static const PAREN_RIGHT:uint = 41;
		public static const ASTERISK:uint = 42;
		public static const PLUS:uint = 43;
		public static const COMMA:uint = 44;
		public static const MINUS_HYPHEN:uint = 45;
		public static const PERIOD:uint = 46;
		public static const BACKSLASH:uint = 47;
		public static const NUM_0:uint = 48;
		public static const NUM_1:uint = 49;
		public static const NUM_2:uint = 50;
		public static const NUM_3:uint = 51;
		public static const NUM_4:uint = 52;
		public static const NUM_5:uint = 53;
		public static const NUM_6:uint = 54;
		public static const NUM_7:uint = 55;
		public static const NUM_8:uint = 56;
		public static const NUM_9:uint = 57;
		public static const COLON:uint = 58;
		public static const SEMICOLON:uint = 59;
		public static const LESS_THAN:uint = 60;
		public static const EQUAL:uint = 61;
		public static const GREATER_THAN:uint = 62;
		public static const QUESTION:uint = 63;
		public static const AT:uint = 64;
		public static const UPPER_A:uint = 65;
		public static const UPPER_B:uint = 66;
		public static const UPPER_C:uint = 67;
		public static const UPPER_D:uint = 68;
		public static const UPPER_E:uint = 69;
		public static const UPPER_F:uint = 70;
		public static const UPPER_G:uint = 71;
		public static const UPPER_H:uint = 72;
		public static const UPPER_I:uint = 73;
		public static const UPPER_J:uint = 74;
		public static const UPPER_K:uint = 75;
		public static const UPPER_L:uint = 76;
		public static const UPPER_M:uint = 77;
		public static const UPPER_N:uint = 78;
		public static const UPPER_O:uint = 79;
		public static const UPPER_P:uint = 80;
		public static const UPPER_Q:uint = 81;
		public static const UPPER_R:uint = 82;
		public static const UPPER_S:uint = 83;
		public static const UPPER_T:uint = 84;
		public static const UPPER_U:uint = 85;
		public static const UPPER_V:uint = 86;
		public static const UPPER_W:uint = 87;
		public static const UPPER_X:uint = 88;
		public static const UPPER_Y:uint = 89;
		public static const UPPER_Z:uint = 90;
		public static const BRACKET_LEFT:uint = 91;
		public static const FORWARD_SLASH:uint = 92;
		public static const BRACKET_RIGHT:uint = 93;
		public static const CARET:uint = 94;
		public static const UNDERSCORE:uint = 95;
		public static const GRAVE:uint = 96;
		public static const LOWER_A:uint = 97;
		public static const LOWER_B:uint = 98;
		public static const LOWER_C:uint = 99;
		public static const LOWER_D:uint = 100;
		public static const LOWER_E:uint = 101;
		public static const LOWER_F:uint = 102;
		public static const LOWER_G:uint = 103;
		public static const LOWER_H:uint = 104;
		public static const LOWER_I:uint = 105;
		public static const LOWER_J:uint = 106;
		public static const LOWER_K:uint = 107;
		public static const LOWER_L:uint = 108;
		public static const LOWER_M:uint = 109;
		public static const LOWER_N:uint = 110;
		public static const LOWER_O:uint = 111;
		public static const LOWER_P:uint = 112;
		public static const LOWER_Q:uint = 113;
		public static const LOWER_R:uint = 114;
		public static const LOWER_S:uint = 115;
		public static const LOWER_T:uint = 116;
		public static const LOWER_U:uint = 117;
		public static const LOWER_V:uint = 118;
		public static const LOWER_W:uint = 119;
		public static const LOWER_X:uint = 120;
		public static const LOWER_Y:uint = 121;
		public static const LOWER_Z:uint = 122;
		public static const BRACE_LEFT:uint = 123;
		public static const PIPE:uint = 124;
		public static const BRACE_RIGHT:uint = 125;
		public static const TILDE:uint = 126;
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// members
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// constructor
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// public functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// protected functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////
		//==================================================================
		// private functions
		//==================================================================
		////////////////////////////////////////////////////////////////////
	}
}