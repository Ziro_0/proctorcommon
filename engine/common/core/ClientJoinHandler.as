/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientJoinHandler

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-03-2018
  ActionScript:  3.0
  Description:  
  History:
    05-03-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.ClientJoinedKeys;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import flib.utils.FDataBlock;
  import net.data.NetClientData;
  
  internal class ClientJoinHandler {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ClientJoinHandler(o_engine:I_Engine) {
      super();
      mo_engine = o_engine;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // run
    //==================================================================
    internal function run(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      var s_clientId:String = o_messageBlock.getString(ClientJoinedKeys.CLIENT_ID);
      if (s_clientId == mo_engine.playerId) {
        handleClientJoinedThis(o_clientData, o_messageBlock);
      } else {
        handleClientJoinedOther(o_clientData, o_messageBlock);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getOrCreatePlayerState
    //==================================================================
    private function getOrCreatePlayerState(s_clientId:String):FDataBlock {
      var o_playerState:FDataBlock = mo_engine.getPlayerData(s_clientId);
      if (!o_playerState) {
        o_playerState = new FDataBlock({ });
        mo_engine.players.setProperty(s_clientId, o_playerState);
      }
      return(o_playerState);
    }
    
    //==================================================================
    // handleClientJoinedOther
    //==================================================================
    private function handleClientJoinedOther(o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      var s_clientId:String = o_messageBlock.getString(ClientJoinedKeys.CLIENT_ID);
      var o_targetPlayerState:FDataBlock = new FDataBlock({ });
      mo_engine.players.setProperty(s_clientId, o_targetPlayerState);
      updatePlayerState(o_targetPlayerState, ClientJoinedKeys.OTHER_PLAYER_STATE, o_clientData,
        o_messageBlock);
    }
    
    //==================================================================
    // handleClientJoinedThis
    //==================================================================
    private function handleClientJoinedThis(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      var s_clientId:String = o_messageBlock.getString(ClientJoinedKeys.CLIENT_ID);
      var o_targetPlayerState:FDataBlock = getOrCreatePlayerState(s_clientId);
      updatePlayerState(o_targetPlayerState, ClientJoinedKeys.PLAYER_STATE, o_clientData, o_messageBlock);
    }
    
    //==================================================================
    // updatePlayerState
    //==================================================================
    private function updatePlayerState(o_targetPlayerState:FDataBlock, s_playerStateKey:String,
    o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      updatePlayerStateProperties(o_targetPlayerState, s_playerStateKey, o_clientData, o_messageBlock);
      updatePlayerStateUserData(o_targetPlayerState, o_clientData, o_messageBlock);
      updatePlayerStateGameProgress(o_targetPlayerState, o_clientData, o_messageBlock);
    }
    
    //==================================================================
    // updatePlayerStateGameProgress
    //==================================================================
    private function updatePlayerStateGameProgress(o_targetPlayerState:FDataBlock, o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      var o_playerProgress:PlayerGameProgress =
        o_targetPlayerState.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress;
      if (!o_playerProgress) {
        o_playerProgress = new PlayerGameProgress();
        o_targetPlayerState.setProperty(PlayerStateKeys.GAME_PROGRESS, o_playerProgress);
      }
    }
    
    //==================================================================
    // updatePlayerStateProperties
    //==================================================================
    private function updatePlayerStateProperties(o_targetPlayerState:FDataBlock, s_playerStateKey:String,
    o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      //copy the server's player state properties to the player state stored by this client
      var o_sourcePlayerState:FDataBlock = o_messageBlock.getObject(s_playerStateKey) as FDataBlock;
      var as_keys:Array = o_sourcePlayerState.getKeys();
      for each (var s_key:String in as_keys) {
        o_targetPlayerState.setProperty(s_key, o_sourcePlayerState.getObject(s_key));
      }
    }
    
    //==================================================================
    // updatePlayerStateUserData
    //==================================================================
    private function updatePlayerStateUserData(o_targetPlayerState:FDataBlock, o_clientData:NetClientData,
    o_messageBlock:FDataBlock):void {
      var o_userData:UserData = o_targetPlayerState.getObject(PlayerStateKeys.USER_DATA) as UserData;
      if (!o_userData) {
        o_userData = new UserData();
        o_targetPlayerState.setProperty(PlayerStateKeys.USER_DATA, o_userData);
      }
      
      o_userData.name = o_messageBlock.getString(ClientJoinedKeys.NAME);
      o_userData.id = o_messageBlock.getString(ClientJoinedKeys.CLIENT_ID);
    }
  }
}