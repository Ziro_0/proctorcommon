/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  App

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	Base document class client games. Custom projects must extend this class.
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import com.greensock.plugins.TransformMatrixPlugin;
  import com.greensock.plugins.TweenPlugin;
  import engine.common.core.Engine;
  import engine.common.core.I_Engine;
  import engine.common.utils.DisplayClassSwf;
  import engine.common.utils.AppDescriptorData;
  import engine.common.utils.deviceProfiles.DeviceProfile;
  import engine.common.utils.deviceProfiles.DeviceProfiles;
  import flash.desktop.NativeApplication;
  import flash.display.Sprite;
  import flash.display.StageAlign;
  import flash.display.StageScaleMode;
  import flash.events.Event;
  import flash.ui.Multitouch;
  import flash.ui.MultitouchInputMode;
	
  public class App extends Sprite implements I_App {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:Engine;
    private var mo_deviceProfile:DeviceProfile;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function App(o_engineInitData:EngineInitData):void {
      super();
			init(o_engineInitData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // deviceProfile (get)
    //==================================================================
    public function get deviceProfile():DeviceProfile {
      return(mo_deviceProfile);
    }
    
    //==================================================================
    // display (get)
    //==================================================================
    public function get display():Sprite {
      return(this);
    }
    
    //==================================================================
    // getEngine
    //==================================================================
    public function getEngine():I_Engine {
      return(mo_engine);
    }
    
    //==================================================================
    // getTemplateClass
    //==================================================================
    public function getTemplateClass(s_templateClassName:String):Class {
      var s_className:String = parseDeviceProfileId(s_templateClassName);
      var o_templateClass:Class = DisplayClassSwf.GetTemplateSwfClass(this, s_className);
      if (!o_templateClass) {
        trace("App.getTemplateClass. Warning: No class found with name: '" + s_className +
          "' using template class name: " + s_templateClassName);
      }
      return(o_templateClass);
    }
    
    //==================================================================
    // parseDeviceProfileId
    //==================================================================
    public function parseDeviceProfileId(s_name:String):String {
      if (!s_name || !mo_deviceProfile) {
        return("");
      }
      
      s_name = s_name.replace("{d}", mo_deviceProfile.id);
      return(s_name);
    }
    
    //==================================================================
    // version (get)
    //==================================================================
    public function get version():String {
      return(AppDescriptorData.Version);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // initDeviceProfile
    //==================================================================
    protected function initDeviceProfile():void {
      if (mo_deviceProfile) {
        return; //already initialized
      }
      
      var i_screenWidth:int = stage.fullScreenWidth;
      var i_screenHeight:int  = stage.fullScreenHeight;
      trace("App.initDeviceProfile. Stage size: " + i_screenWidth + " x " + i_screenHeight);
      mo_deviceProfile = DeviceProfiles.GetProfile(i_screenWidth, i_screenHeight);
      
      if (!mo_deviceProfile) {
        trace("App.initDeviceProfile. No device profile found for stage size. " +
          "Searching for closest matching profile.");
        mo_deviceProfile = DeviceProfiles.GetClosetsMatchingProfile(i_screenWidth, i_screenHeight);
      }
      
      trace("App.initDeviceProfile. Device profile ID: " + mo_deviceProfile.id);
    }
    
    //==================================================================
    // onInit
    //==================================================================
    protected function onInit():void {
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engineInitData:EngineInitData):void {
      stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, onStageDeactivate);
			focusRect = false;
      
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
      
      TweenPlugin.activate([TransformMatrixPlugin]);
      
      initDeviceProfile();
      initEngine(o_engineInitData);
      onInit();
    }
    
    //==================================================================
    // initEngine
    //==================================================================
    private function initEngine(o_engineInitData:EngineInitData):void {
      if (!mo_engine) {
        var o_engineClass:Class = o_engineInitData.engineClass;
        if (o_engineClass == null) {
          throw(new ArgumentError("App.initEngine. Error: The engineClass parameter must be non-null."));
        }
        
        mo_engine = new o_engineClass(o_engineInitData) as Engine;
        if (!mo_engine) {
          throw(new ArgumentError("App.initEngine. Error: " +
            "The engineClass parameter must resolve to an object of class Engine, or that extends Engine."));
        }
      }
      
      if (mo_engine.display) {
        addChild(mo_engine.display);
      } else {
        trace("App.initEngine. Warning: No display object defined for the main view of the engine.");
      }
    }
    
    //==================================================================
    // onStageDeactivate
    //==================================================================
    private function onStageDeactivate(o_event:Event):void {
      if (mo_engine) {
        mo_engine.deactivate();
      }
    }
    
    //==================================================================
    // uninitEngine
    //==================================================================
    private function uninitEngine():void {
      if (mo_engine) {
        mo_engine.uninit();
        mo_engine = null;
      }
    }
  }
}