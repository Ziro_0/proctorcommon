/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientLeftHandler

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-03-2018
  ActionScript:  3.0
  Description:  
  History:
    05-03-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import engine.common.data.PlayerStatuses;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import flib.utils.FDataBlock;
  
  internal class ClientLeftHandler {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ClientLeftHandler(o_engine:I_Engine) {
      super();
      mo_engine = o_engine;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // run
    //==================================================================
    internal function run(s_clientId:String, b_isBecomingObserver:Boolean):void {
      var o_playerData:FDataBlock = mo_engine.getPlayerData(s_clientId);
      if (!o_playerData) {
        return;
      }
      
      //notify high level code first that a client is leaving
      mo_engine.onClientIsLeaving(s_clientId, b_isBecomingObserver);
      
      //if player is becoming observer, update status, otherwise player is leaving game and data should be removed
      updateOrRemovePlayerData(s_clientId, o_playerData, b_isBecomingObserver);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // updateOrRemovePlayerData
    //==================================================================
    private function updateOrRemovePlayerData(s_clientId:String, o_playerData:FDataBlock,
    b_isBecomingObserver:Boolean):void {
      if (b_isBecomingObserver) {
        //client is leaving active game play to become an observer
        o_playerData.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.OBSERVER);
        return;
      }
      
      //client is leaving game completely
      mo_engine.players.deleteProperty(s_clientId);
    }
  }
}