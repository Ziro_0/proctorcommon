/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  Engine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			03-20-2017
  ActionScript:	3.0
  Description:	Base engine class for Proctor's games. Custom game engines must extend this class.
  History:
    03-20-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import engine.common.audio.AudioPlayer;
  import engine.common.core.I_App;
  import engine.common.data.ClientMessageTypes;
  import engine.common.data.ClientSharedData;
  import engine.common.data.MessageProperties;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.PlayerStatuses;
  import engine.common.data.ServerMessageTypes;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.ClientLeftKeys;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.ui.introView.IntroView;
  import engine.common.ui.mainView.I_MainViewCallbacks;
  import engine.common.ui.mainView.MainView;
  import engine.common.utils.SharedData;
  import engine.common.utils.themeComponentImages.ThemeComponentImagesStates;
  import engine.common.utils.themeComponentImages.I_ThemeComponentImagesCallbacks;
  import engine.common.utils.themeComponentImages.ThemeComponentImages;
  import flash.display.Shape;
  import flash.display.Sprite;
  import flash.events.ErrorEvent;
  import flash.events.Event;
  import flash.system.Capabilities;
  import flib.utils.FDataBlock;
  import flib.utils.timers.FTimerSystem;
  import net.client.NetClientSocket;
  import net.data.NetClientData;
  import net.data.NetSocketData;
  import net.events.NetClientSocketEvent;

  public class Engine implements I_Engine, I_MainViewCallbacks, IDialogManagerCallbacks,
  I_ThemeComponentImagesCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    // This token is replaced with the number of seconds remaining until the next retry attempt
    public static const RETRY_TIME_TOKEN:String = "`t`";
    
    private static const MSG_DEFAULT_CONNECT_ERROR:String = "Error connecting to server." +
      "\nRetrying in " + RETRY_TIME_TOKEN + "...";
    
    private static const MSG_SOCKET_ERROR:String = "Unable to connect to the server." +
      "\nPlease check to make sure the IP address and port are correct, and that the server is available.";
      
    private static const MSG_SOCKET_PREPARING:String = "Attempting to connect...";
    
    private static const SHARED_DATA_NAME:String = "engineSharedData";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    protected var ms_connectErrorMessage:String;
    
    private var mvo_pendingMessages:Vector.<ServerMessagesQueueItem>;
    
    private var mo_app:I_App;
    private var mo_sharedData:ClientSharedData;
    private var mo_initData:EngineInitData;
    private var mo_mainView:MainView;
    private var mo_introView:IntroView;
    private var mo_audioPlayer:AudioPlayer;
    private var mo_socket:NetClientSocket;
    private var mo_dialogManager:DialogManager;
    
    private var mo_themeComponentImages:ThemeComponentImages;
    
    private var mo_timerSystem:FTimerSystem;
    private var mo_serverState:FDataBlock;
    private var mo_thisTempPlayerData:FDataBlock;
    
    private var mo_display:Sprite;
    
    private var mo_playerDataClass:Class;
    
    private var ms_playerId:String;
    
    private var mb_isRequestingThemeImages:Boolean;
    private var mb_isBusyWithMessage:Boolean;
    private var mb_activeGameRequestFromInitialState:Boolean;
    private var mb_activeGameStartedRequestFromInitialState:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function Engine(o_initData:EngineInitData):void {
      super();
      init(o_initData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // app (get)
    //==================================================================
    public function get app():I_App {
      return(mo_app);
    }
    
    //==================================================================
    // audioPlayer (get)
    //==================================================================
    public function get audioPlayer():AudioPlayer {
      return(mo_audioPlayer);
    }
    
    //==================================================================
    // deactivate
    //==================================================================
    public function deactivate():void {
    }
    
    //==================================================================
    // dialogManager (get)
    //==================================================================
    public function get dialogManager():DialogManager {
      return(mo_dialogManager);
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
    }
    
    //==================================================================
    // display (get)
    //==================================================================
    public function get display():Sprite {
      return(mo_display);
    }
    
    //==================================================================
    // exitGame
    //==================================================================
    public function exitGame():void {
      uninitNetwork();
      uninitGamePlay();
      initInitialView();
    }
    
    //==================================================================
    // getActiveClientIds
    //==================================================================
    public function getActiveClientIds():Array {
      var as_activeClientIds:Array = [];
      var as_clientIds:Array = getClientIds();
      for each (var s_clientId:String in as_clientIds) {
        var o_playerData:FDataBlock = getPlayerData(s_clientId);
        if (o_playerData.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.ACTIVE) {
          as_activeClientIds.push(s_clientId);
        }
      }
      
      return(as_activeClientIds);
    }
    
    //==================================================================
    // getClientIds
    //==================================================================
    public function getClientIds(as_clientIds_out:Array = null):Array {
      if (as_clientIds_out) {
        as_clientIds_out.length = 0;
      } else {
        as_clientIds_out = [];
      }
      
      if (!mo_serverState) {
        return(as_clientIds_out);
      }
      
      var o_players:FDataBlock = mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      for (var s_clientId:String in o_players.data) {
        as_clientIds_out.push(s_clientId);
      }
      
      return(as_clientIds_out);
    }
    
    //==================================================================
    // getPlayerData
    //==================================================================
    public function getPlayerData(s_playerId:String):FDataBlock {
      if (!mo_serverState) {
        return(null);
      }
      
      var o_players:FDataBlock = mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      return(o_players.getObject(s_playerId) as FDataBlock);
    }
    
    //==================================================================
    // initData (get)
    //==================================================================
    public function get initData():EngineInitData {
      return(mo_initData);
    }
    
    //==================================================================
    // introViewOnBack
    //==================================================================
    public function introViewOnBack():void {
      cancelConnectionAttempt();
    }
    
    //==================================================================
    // introViewOnComplete
    //==================================================================
    public function introViewOnComplete():void {
      updateDataFromIntroView();
      enableIntroUiWhileConnecting(false);
      connectToServer();
    }
    
    //==================================================================
    // isBusyWithMessage (get)
    //==================================================================
    public function get isBusyWithMessage():Boolean {
      return(mb_isBusyWithMessage);
    }
    
    //==================================================================
    // isBusyWithMessage (set)
    //==================================================================
    public function set isBusyWithMessage(b_value:Boolean):void {
      mb_isBusyWithMessage = b_value;
    }
    
    //==================================================================
    // mainView (get)
    //==================================================================
    public function get mainView():MainView {
      return(mo_mainView);
    }
    
    //==================================================================
    // mainViewOnExitButtonClick
    //==================================================================
    public function mainViewOnExitButtonClick():void {
      exitGame();
    }
    
    //==================================================================
    // mainViewOnJoinWatchToggle
    //==================================================================
    public function mainViewOnJoinWatchToggle(b_isInWatchMode:Boolean):void {
      var u_playerStatus:uint = thisPlayerData.getNumber(PlayerStateKeys.STATUS);
      if (u_playerStatus == PlayerStatuses.OBSERVER) {
        join();
      } else if (u_playerStatus == PlayerStatuses.ACTIVE) {
        watch();
      }
    }
    
    //==================================================================
    // numActivePlayers (get)
    //==================================================================
    public function get numActivePlayers():uint {
      if (!mo_serverState) {
        return(0);
      }
      
      var u_count:uint;
      var o_players:FDataBlock = mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      for (var s_clientId:String in o_players.data) {
        var o_playerData:FDataBlock = o_players.getObject(s_clientId) as FDataBlock;
        if (o_playerData.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.ACTIVE) {
          u_count++;
        }
      }
      
      return(u_count);
    }
    
    //==================================================================
    // onClientIsLeaving
    //==================================================================
    public function onClientIsLeaving(s_clientId:String, b_isBecomingObserver:Boolean):void {
      //This method is designed to be called by the ClientLeftHandler and overridden.
    }
    
    //==================================================================
    // players (get)
    //==================================================================
    public function get players():FDataBlock {
      return(mo_serverState ?
        mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock :
        null);
    }
    
    //==================================================================
    // playerId (get)
    //==================================================================
    public function get playerId():String {
      return(ms_playerId);
    }
    
    //==================================================================
    // sendMessageToServer
    //==================================================================
    public function sendMessageToServer(o_message:Object):Boolean {
      return(mo_socket &&
        mo_socket.sendMessageToServer(o_message));
    }
    
    //==================================================================
    // serverState (get)
    //==================================================================
    public function get serverState():FDataBlock {
      return(mo_serverState);
    }
    
    //==================================================================
    // sharedData (get)
    //==================================================================
    public function get sharedData():ClientSharedData {
      return(mo_sharedData);
    }
    
    //==================================================================
    // themeComponentImages (get)
    //==================================================================
    public function get themeComponentImages():ThemeComponentImages {
      return(mo_themeComponentImages);
    }
    
    //==================================================================
    // themeComponentsImagesOnComplete
    //==================================================================
    public function themeComponentsImagesOnComplete(o_images:ThemeComponentImages):void {
      showLoadingIcon(false);
      
      if (mb_activeGameRequestFromInitialState) {
        handleThemeComponentImagesCompleteWithGame();
        return;
      }
      
      var s_gameId:String = serverState.getString(ServerStateKeys.ACTIVE_GAME_ID);
      if (s_gameId) {
        handleThemeComponentImagesCompleteWithGame();
      } else {
        handleThemeComponentImagesCompleteNoGame();
      }
    }
    
    //==================================================================
    // themeComponentsImagesOnProgress
    //==================================================================
    public function themeComponentsImagesOnProgress(o_images:ThemeComponentImages,
    n_progress:Number):void {
      if (mo_introView) {
        mo_introView.loadingProgress = n_progress;
      } else if (mo_mainView) {
        mo_mainView.loadingProgress = n_progress;
      }
    }
    
    //==================================================================
    // thisPlayerData (get)
    //==================================================================
    public function get thisPlayerData():FDataBlock {
      if (!mo_serverState) {
        return(mo_thisTempPlayerData);
      }
      
      var o_players:FDataBlock = mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      return(o_players.getObject(ms_playerId) as FDataBlock);
    }
    
    //==================================================================
    // timerSystem (get)
    //==================================================================
    public function get timerSystem():FTimerSystem {
      return(mo_timerSystem);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      exitGame();
      mo_app = null;
    }
    
    //==================================================================
    // updateNonGameButtonsStatuses
    //==================================================================
    public function updateNonGameButtonsStatuses():void {
      mo_mainView.exitButtonVisible = isExitButtonVisible();
      
      onUpdateJoinWatchStatus();
      mo_mainView.joinWatchToggleVisible = isJoinWatchButtonVisible();
    }
    
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // handleMessageClientJoined
    //==================================================================
    protected function handleMessageClientJoined(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      var o_handler:ClientJoinHandler = new ClientJoinHandler(this);
      o_handler.run(o_clientData, o_messageBlock);
    }
    
    //==================================================================
    // handleMessageClientLeft
    //==================================================================
    protected function handleMessageClientLeft(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      var o_handler:ClientLeftHandler = new ClientLeftHandler(this);
      o_handler.run(
        o_messageBlock.getString(ClientLeftKeys.CLIENT_ID),
        o_messageBlock.getBoolean(ClientLeftKeys.IS_OBSERVER));
    }
    
    //==================================================================
    // isExitButtonVisible
    //==================================================================
    protected function isExitButtonVisible():Boolean {
      //Currently, exit button is always visible.
      return(true);
    }
    
    //==================================================================
    // isJoinWatchButtonVisible
    //==================================================================
    protected function isJoinWatchButtonVisible():Boolean {
      var u_playerStatus:uint = thisPlayerData.getNumber(PlayerStateKeys.STATUS);
      
      //Button not visible if player is currently joining.
      if (u_playerStatus == PlayerStatuses.JOINING) {
        return(false);
      }
      
      //Visible when player is an observer (button should read "join").
      if (u_playerStatus == PlayerStatuses.OBSERVER) {
        return(true);
      }
      
      //Visible when player is active (button should read "watch"), and there is more than one active player.
      if (u_playerStatus == PlayerStatuses.ACTIVE) {
        return(numActivePlayers > 1);
      }
      
      //code should never reach here if app state is good, but just in case (button is never visible)
      return(false);
    }
    
    //==================================================================
    // isPlayerActive
    //==================================================================
    protected function isPlayerActive(s_clientId:String):Boolean {
      var o_playerState:FDataBlock = getPlayerData(s_clientId);
      return(o_playerState && o_playerState.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.ACTIVE);
    }
    
    //==================================================================
    // isThisPlayerActive
    //==================================================================
    protected function isThisPlayerActive():Boolean {
      return(isPlayerActive(playerId));
    }
    
    //==================================================================
    // loadThemeComponentImages
    //==================================================================
    protected function loadThemeComponentImages(b_requestThemeImageDownload:Boolean):void {
      if (!mo_themeComponentImages) {
        mo_themeComponentImages = new ThemeComponentImages(this, mo_initData.themesBaseUrl,
          mo_initData.customThemesBaseUrl, this);
      }
      
      var o_componentThemeIdsByName:Object =
        serverState.getObject(ServerStateKeys.THEME_IDS_BY_COMP_NAME);
      var s_themeId:String = serverState.getString(ServerStateKeys.THEME_ID);
      var s_gameId:String = serverState.getString(ServerStateKeys.ACTIVE_GAME_ID);
      
      mo_themeComponentImages.load(o_componentThemeIdsByName, s_themeId, s_gameId,
        b_requestThemeImageDownload);
        
      var u_state:uint = mo_themeComponentImages.state;
      if (u_state == ThemeComponentImagesStates.LOADING_IMAGE_FILES ||
      u_state == ThemeComponentImagesStates.REQUESTING_IMAGE_DATA) {
        showLoadingIcon(true);
        mb_isRequestingThemeImages = true;
        isBusyWithMessage = true;
      } else if (u_state == ThemeComponentImagesStates.IDLE) {
        themeComponentsImagesOnComplete(mo_themeComponentImages);
      }
    }
    
    //==================================================================
    // onHandleServerState
    //==================================================================
    protected function onHandleServerState(b_activeGameRequestFromInitialState:Boolean,
    b_activeGameStartedRequestFromInitialState:Boolean):void {
    }
    
    //==================================================================
    // onHandleSocketMessage
    //==================================================================
    protected function onHandleSocketMessage(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
    }
    
    //==================================================================
    // onInit
    //==================================================================
    protected function onInit():void {
    }
    
    //==================================================================
    // onInitGame
    //==================================================================
    protected function onInitGame():void {
    }
    
    //==================================================================
    // initDialogManager
    //==================================================================
    private function initDialogManager():void {
      if (!mo_dialogManager) {
        mo_dialogManager = new DialogManager(app, display, this);
      }
    }
    
    //==================================================================
    // initMainUi
    //==================================================================
    private function initMainUi():void {
      initDialogManager();
      initMainView();
    }
    
    //==================================================================
    // onIntroViewInit
    //==================================================================
    protected function onIntroViewInit():void {
    }
    
    //==================================================================
    // onMainViewInit
    //==================================================================
    protected function onMainViewInit():void {
    }
    
    //==================================================================
    // onSocketRetryTimer
    //==================================================================
    protected function onSocketRetryTimer(o_event:NetClientSocketEvent):void {
      if (!mo_introView) {
        return;
      }
      
      var u_secondsRemaining:uint = uint(o_event.netSocketData.message);
      var o_regExTokenString:RegExp = new RegExp(RETRY_TIME_TOKEN, "g");
      var s_message:String =
        String(ms_connectErrorMessage || MSG_DEFAULT_CONNECT_ERROR).replace(o_regExTokenString,
          u_secondsRemaining.toString());
          
      mo_introView.statusText = s_message;
    }
    
    //==================================================================
    // onUninitGamePlay
    //==================================================================
    protected function onUninitGamePlay():void {
    }
    
    //==================================================================
    // onUpdateJoinWatchStatus
    //==================================================================
    protected function onUpdateJoinWatchStatus():void {
      var u_playerStatus:uint = thisPlayerData.getNumber(PlayerStateKeys.STATUS);
      if (u_playerStatus == PlayerStatuses.OBSERVER) {
        mainView.joinWatchShowJoin();
      } else if (u_playerStatus == PlayerStatuses.ACTIVE) {
        mainView.joinWatchShowWatch();
      }
    }
    
    //==================================================================
    // processNextMessage
    //==================================================================
    protected final function processNextMessage():void {
      if (mb_isBusyWithMessage) {
        return; //still not ready to process next message
      }
      
      var o_item:ServerMessagesQueueItem = mvo_pendingMessages.shift();
      if (o_item) {
        //still busy processing pending messages
        handleSocketMessage(o_item.clientData, o_item.messageBlock);
      } else {
        //done processing pending messages
        mb_isBusyWithMessage = false;
      }
    }
    
    //==================================================================
    // showLoadingIcon
    //==================================================================
    protected function showLoadingIcon(b_isVisible:Boolean):void {
      if (mo_introView) {
        if (b_isVisible) {
          mo_introView.statusText = "Loading graphics from server...";
        }
      }
      
      if (mo_mainView) {
        mo_mainView.showLoadingUi(b_isVisible);
      }
    }
    
    //==================================================================
    // uninitIntroView
    //==================================================================
    protected function uninitIntroView():void {
      if (mo_introView) {
        mo_introView.uninit();
        mo_introView = null;
      }
    }
    
    //==================================================================
    // updateDataFromIntroView
    //==================================================================
    protected function updateDataFromIntroView():void {
      var o_userData:UserData = thisPlayerData.getObject(PlayerStateKeys.USER_DATA) as UserData;
      if (mo_introView) {
        o_userData.name = mo_introView.playerName;
      }
      o_userData.system = Capabilities.version;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // cancelConnectionAttempt
    //==================================================================
    private function cancelConnectionAttempt():void {
      enableIntroUiWhileConnecting(true);
      uninitSocket();
      mo_introView.statusText = null;
    }
    
    //==================================================================
    // connectToServer
    //==================================================================
    private function connectToServer():void {
      mo_socket = new NetClientSocket();
      mo_socket.addEventListener(Event.CONNECT, onSocketConnect, false, 0, true);
      mo_socket.addEventListener(ErrorEvent.ERROR, onSocketError, false, 0, true);
      mo_socket.addEventListener(Event.PREPARING, onSocketPreparing, false, 0, true);
      mo_socket.addEventListener(Event.CLOSE, onSocketClose, false, 0, true);
      mo_socket.addEventListener(NetClientSocketEvent.RETRY_TIMER, onSocketRetryTimer, false, 0, true);
      
      var o_netClientData:NetClientData = new NetClientData();
      var o_userData:UserData = thisPlayerData.getObject(PlayerStateKeys.USER_DATA) as UserData;
      o_netClientData.userData.name = o_userData.name;
      o_netClientData.userData.system = o_userData.system;
      mo_socket.connect(mo_introView.ipAddress, mo_introView.port, o_netClientData);
    }
    
    //==================================================================
    // enableIntroUiWhileConnecting
    //==================================================================
    private function enableIntroUiWhileConnecting(b_isEnabled:Boolean):void {
      if (!mo_introView) {
        return;
      }
      
      mo_introView.enableJoinButton(b_isEnabled);
      mo_introView.enableTextInputs(b_isEnabled);
      mo_introView.loadingIconVisible = !b_isEnabled;
    }
    
    //==================================================================
    // handleServerState
    //==================================================================
    private function handleServerState(o_stateBlock:FDataBlock):void {
      mo_serverState = o_stateBlock;
      
      mb_activeGameRequestFromInitialState =
        Boolean(mo_serverState.getString(ServerStateKeys.ACTIVE_GAME_ID));
      mb_activeGameStartedRequestFromInitialState =
        mo_serverState.getBoolean(ServerStateKeys.IS_GAME_STARTED);
      
      loadThemeComponentImages(true);
    }
    
    //==================================================================
    // handleServerStatePlayers
    //==================================================================
    private function handleServerStatePlayers():void {
      var o_players:FDataBlock = mo_serverState.getObject(ServerStateKeys.PLAYERS) as FDataBlock;
      for (var s_clientId:String in o_players.data) {
        var o_playerData:FDataBlock = o_players.getObject(s_clientId) as FDataBlock;
        if (!o_playerData) {
          continue; //sanity check
        }
        
        var o_userData:UserData = o_playerData.getObject(PlayerStateKeys.USER_DATA) as UserData;
        if (!o_userData) {
          continue; //sanity check
        }
        
        if (s_clientId == playerId) {
          //general ui states specific to this client (not player hand)
          onUpdateJoinWatchStatus();
        }
      }
    }
    
    //==================================================================
    // handleSocketMessage
    //==================================================================
    private function handleSocketMessage(o_clientData:NetClientData, o_messageBlock:FDataBlock):void {
      //handles messages sent from the server
      
      var s_type:String = o_messageBlock.getString(MessageProperties.TYPE);
      
      if (mb_isBusyWithMessage) {
        if (mb_isRequestingThemeImages && s_type == ServerMessageTypes.COMPONENT_THEME_IMAGES) {
          mb_isRequestingThemeImages = false;
          isBusyWithMessage = false;
          mvo_pendingMessages.push(new ServerMessagesQueueItem(o_clientData, o_messageBlock));
          processNextMessage();
          return;
        } else {
          mvo_pendingMessages.push(new ServerMessagesQueueItem(o_clientData, o_messageBlock));
          return;
        }
      }
      
      switch (s_type) {
        case ServerMessageTypes.CLIENT_JOINED:
          handleMessageClientJoined(o_clientData, o_messageBlock);
          break;
          
        case ServerMessageTypes.CLIENT_LEFT:
          handleMessageClientLeft(o_clientData, o_messageBlock);
          break;
      }
      
      onHandleSocketMessage(o_clientData, o_messageBlock);
      
      //will attempt to process any pending messages here. If a previously handled message has caused the
      // engine to still be busy processing it, any pending messages will wait. It is then up to the processing
      // message to set the engine "not busy" by setting the isBusyWithMessage property to false, then calling
      // processNextMessage().
      
      processNextMessage();
    }
    
    //==================================================================
    // handleThemeComponentImagesCompleteWithGame
    //==================================================================
    private function handleThemeComponentImagesCompleteWithGame():void {
      if (mb_activeGameRequestFromInitialState) {
        var b_activeGameRequestFromInitialState:Boolean = mb_activeGameRequestFromInitialState;
        var b_activeGameStartedRequestFromInitialState:Boolean =
          mb_activeGameStartedRequestFromInitialState;
        mb_activeGameRequestFromInitialState = mb_activeGameStartedRequestFromInitialState = false;
        
        //uninitIntroView();
        initMainUi();
        onInitGame();
        onHandleServerState(b_activeGameRequestFromInitialState,
          b_activeGameStartedRequestFromInitialState);
      } else {
        onInitGame();
        isBusyWithMessage = false;
        processNextMessage();
      }
    }
    
    //==================================================================
    // handleThemeComponentImagesCompleteNoGame
    //==================================================================
    private function handleThemeComponentImagesCompleteNoGame():void {
      uninitIntroView();
      initMainUi();
      onHandleServerState(false, false);
      
      isBusyWithMessage = false;
      processNextMessage();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_initData:EngineInitData):void {
      mvo_pendingMessages = new Vector.<ServerMessagesQueueItem>();
      
      mo_initData = o_initData;
      mo_app = o_initData.app;
      mo_timerSystem = new FTimerSystem();
      mo_audioPlayer = new AudioPlayer();
      
      initDisplay();
      initPlayers();
      
      onInit();
      initSharedData();
      initInitialView();
    }
    
    //==================================================================
    // initDisplay
    //==================================================================
    private function initDisplay():void {
      //create engine main display. all display objects get added to this (or somewhere beneath). this display
      // gets added to the app's display
      mo_display = new Sprite();
      
      //create a placeholder display to assume the full size of the device's screen
      var o_shape:Shape = new Shape();
      o_shape.graphics.beginFill(0, 0);
      o_shape.graphics.drawRect(0, 0, mo_app.deviceProfile.assetsWidth, mo_app.deviceProfile.assetsHeight);
      o_shape.graphics.endFill();
      mo_display.addChild(o_shape);
      
      mo_display.width = mo_app.display.stage.fullScreenWidth;
      mo_display.height = mo_app.display.stage.fullScreenHeight;
    }
    
    //==================================================================
    // initInitialView
    //==================================================================
    private function initInitialView():void {
      var o_viewClass:Class = mo_initData.introViewClass;
      if (o_viewClass == null) {
        trace("Engine.initInitialView. Warning: No intro view class specified. Trying main view.");
        initMainView();
        return;
      }
      
      if (mo_introView) {
        return;
      }
      
      mo_introView = new o_viewClass(this) as IntroView;
      if (!mo_introView) {
        throw(new ArgumentError("Engine.initInitialView. Error: " +
          "The introViewClass parameter must resolve to an object of class IntroView, " +
          "or that extends IntroView."));
      }
      
      mo_introView.version = mo_initData.versionText;
      mo_introView.playerName = mo_sharedData.name;
      mo_introView.ipAddress = mo_initData.ipAddress || mo_sharedData.ipAddress;
      mo_introView.port = mo_sharedData.port;
      
      mo_display.addChild(mo_introView.display);
      onIntroViewInit();
    }
    
    //==================================================================
    // initMainView
    //==================================================================
    private function initMainView():void {
      if (mo_mainView) {
        return;
      }
      
      var o_viewClass:Class = mo_initData.mainViewClass;
      if (o_viewClass == null) {
        throw(new ArgumentError("Engine.initMainView. Error: The mainViewClass parameter must be non-null."));
      }
      
      mo_mainView = new o_viewClass(this) as MainView;
      if (!mo_mainView) {
        throw(new ArgumentError("Engine.initMainView. Error: " +
          "The mainViewClass parameter must resolve to an object of class MainView, " +
          "or that extends MainView."));
      }
      
      mo_display.addChild(mo_mainView.display);
      onMainViewInit();
    }
    
    //==================================================================
    // initPlayers
    //==================================================================
    private function initPlayers():void {
      //create a player state block for this player with temp player id. The id will be re-assigned
      // by the official id from the server once this client connects with the server.
      
      ms_playerId = "temp-" + new Date().time;
      mo_thisTempPlayerData = new FDataBlock();
      mo_thisTempPlayerData.setProperty(PlayerStateKeys.GAME_PROGRESS, new PlayerGameProgress());
      mo_thisTempPlayerData.setProperty(PlayerStateKeys.USER_DATA, new UserData());
      mo_thisTempPlayerData.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.NONE);
    }
    
    //==================================================================
    // initSharedData
    //==================================================================
    private function initSharedData():void {
      var o_class:Class = mo_initData.sharedDataClass || SharedData;
      mo_sharedData = new o_class() as ClientSharedData;
      
      if (!mo_sharedData) {
        trace("Engine.initSharedData. Warning: Unable to create ClientSharedData with class " + o_class);
        mo_sharedData = new ClientSharedData();
      }
      
      if (!mo_sharedData.create(SHARED_DATA_NAME)) {
        trace("Engine.initSharedData. Warning: " + mo_sharedData.errorMessage);
      }
    }
    
    //==================================================================
    // join
    //==================================================================
    private function join():void {
      thisPlayerData.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.JOINING);
      updateNonGameButtonsStatuses();
      
      var o_message:Object = { };
      o_message[MessageProperties.TYPE] = ClientMessageTypes.JOIN;
      sendMessageToServer(o_message);
    }
    
    //==================================================================
    // onSocketClose
    //==================================================================
    private function onSocketClose(o_event:Event):void {
      exitGame();
    }
    
    //==================================================================
    // onSocketConnect
    //==================================================================
    private function onSocketConnect(o_event:Event):void {
      mo_socket.removeEventListener(Event.CONNECT, onSocketConnect);
      mo_socket.removeEventListener(Event.PREPARING, onSocketPreparing);
      
      mo_socket.addEventListener(NetClientSocketEvent.STATE, onSocketState, false, 0, true);
      mo_socket.addEventListener(NetClientSocketEvent.MESSAGE, onSocketMesssage, false, 0, true);
      
      //swap this user's temp ID with the one assigned from the server
      var o_playerState:FDataBlock = thisPlayerData;
      ms_playerId = mo_socket.userId;
      
      var o_userData:UserData = o_playerState.getObject(PlayerStateKeys.USER_DATA) as UserData;
      o_userData.copyFrom(mo_socket.clientData.userData);
    }
    
    //==================================================================
    // onSocketError
    //==================================================================
    private function onSocketError(o_event:ErrorEvent):void {
      mo_introView.statusText = MSG_SOCKET_ERROR;
    }
    
    //==================================================================
    // onSocketMesssage
    //==================================================================
    private function onSocketMesssage(o_event:NetClientSocketEvent):void {
      var o_netSocketData:NetSocketData = o_event.netSocketData;
      var o_messageBlock:FDataBlock = new FDataBlock(o_netSocketData.message);
      handleSocketMessage(o_netSocketData.clientData, o_messageBlock);
    }
    
    //==================================================================
    // onSocketPreparing
    //==================================================================
    private function onSocketPreparing(o_event:Event):void {
      mo_introView.statusText = MSG_SOCKET_PREPARING;
    }
    
    //==================================================================
    // onSocketState
    //==================================================================
    private function onSocketState(o_event:NetClientSocketEvent):void {
      var o_netSocketData:NetSocketData = o_event.netSocketData;
      var o_stateBlock:FDataBlock = new FDataBlock(o_netSocketData.clientData.state);
      handleServerState(o_stateBlock);
    }
    
    //==================================================================
    // uninitGamePlay
    //==================================================================
    private function uninitGamePlay():void {
      onUninitGamePlay();
      uninitMainView();
      mo_timerSystem.removeAllTimers();
      mo_audioPlayer.stopAllSounds();
    }
    
    //==================================================================
    // uninitMainView
    //==================================================================
    private function uninitMainView():void {
      if (mo_mainView) {
        mo_mainView.uninit();
        mo_mainView = null;
      }
    }
    
    //==================================================================
    // uninitNetwork
    //==================================================================
    private function uninitNetwork():void {
      uninitSocket();
      mo_serverState = null;
      initPlayers();
    }
    
    //==================================================================
    // uninitSocket
    //==================================================================
    private function uninitSocket():void {
      if (!mo_socket) {
        return;
      }
      
      mo_socket.close();
      mo_socket.removeEventListener(Event.CONNECT, onSocketConnect);
      mo_socket.removeEventListener(ErrorEvent.ERROR, onSocketError);
      mo_socket.removeEventListener(Event.PREPARING, onSocketPreparing);
      mo_socket.removeEventListener(Event.CLOSE, onSocketClose);
      mo_socket.removeEventListener(NetClientSocketEvent.RETRY_TIMER, onSocketRetryTimer);
      mo_socket = null;
    }
    
    //==================================================================
    // watch
    //==================================================================
    private function watch():void {
      //this active player becomes an observer
      var o_handler:ClientLeftHandler = new ClientLeftHandler(this);
      o_handler.run(playerId, true);
      
      //send message to server that this player is now an observer
      var o_message:Object = { };
      o_message[MessageProperties.TYPE] = ClientMessageTypes.WATCH;
      sendMessageToServer(o_message);
    }
  }
}