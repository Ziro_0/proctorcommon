/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_Engine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import engine.common.audio.AudioPlayer;
  import engine.common.core.I_App;
  import engine.common.data.ClientSharedData;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.introView.I_IntroViewCallbacks;
  import engine.common.ui.mainView.MainView;
  import engine.common.utils.themeComponentImages.ThemeComponentImages;
  import flash.display.Sprite;
  import flib.utils.FDataBlock;
  import flib.utils.timers.FTimerSystem;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_Engine extends I_IntroViewCallbacks {
    function get app():I_App;
    function get audioPlayer():AudioPlayer;
    function get dialogManager():DialogManager;
    function get display():Sprite;
    function getActiveClientIds():Array;
    function getClientIds(as_clientIds_out:Array = null):Array;
    function getPlayerData(s_playerId:String):FDataBlock;
    function get initData():EngineInitData;
    function get isBusyWithMessage():Boolean;
    function set isBusyWithMessage(b_value:Boolean):void;
    function get mainView():MainView;
    function onClientIsLeaving(s_clientId:String, b_isBecomingObserver:Boolean):void;
    function get playerId():String;
    function get players():FDataBlock;
    function sendMessageToServer(o_message:Object):Boolean;
    function get serverState():FDataBlock;
    function get sharedData():ClientSharedData;
    function get themeComponentImages():ThemeComponentImages;
    function get thisPlayerData():FDataBlock;
    function get timerSystem():FTimerSystem;
  }
}