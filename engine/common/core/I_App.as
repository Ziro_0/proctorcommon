/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_App

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.core {
  import engine.common.core.I_Engine;
  import engine.common.utils.deviceProfiles.DeviceProfile;
  import flash.display.Sprite;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_App {
    function get deviceProfile():DeviceProfile;
    function get display():Sprite;
    function getEngine():I_Engine;
    function getTemplateClass(s_templateClassName:String):Class;
    function parseDeviceProfileId(s_name:String):String;
    function get version():String;
  }
}