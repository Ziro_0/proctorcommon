/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  BaseView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			03-30-2018
  ActionScript:	3.0
  Description:	
  History:
    03-22-2017:	Started.
    04-05-2017: Added uninitButton method.
    03-30-2018: Added container param to initButton.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.baseView {
  import engine.common.data.Sounds;
  import engine.common.ui.buttons.Button;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flib.ui.FDisplay;
  import flib.ui.button.FButton;
	
  public class BaseView extends FDisplay {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_BaseViewCallbacks;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function BaseView(o_spriteOrSpriteClass:Object, o_callbacks:I_BaseViewCallbacks = null):void {
      super(o_spriteOrSpriteClass);
			callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_BaseViewCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_BaseViewCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // enableView
    //==================================================================
    public function enableView(o_view:BaseView, b_enabled:Boolean):void {
      if (o_view) {
        o_view.enabled = b_enabled;
      }
    }
    
    //==================================================================
    // initButton
    //==================================================================
    public function initButton(s_buttonDisplayName:String, f_clickCallback:Function,
    o_clickSound:Object = false, b_enabled:Boolean = true, o_container:DisplayObjectContainer = null):Button {
      var o_display:Sprite = getSprite(s_buttonDisplayName, o_container);
      if (!o_display) {
        return(null);
      }
      
      if (o_clickSound is Boolean && o_clickSound == false) {
        o_clickSound = Sounds.BUTTON_CLICK;
      }
      
      var o_button:Button = new Button(o_display, f_clickCallback, o_clickSound);
      o_button.enabled = b_enabled;
      return(o_button);
    }
    
    //==================================================================
    // initButton2
    //==================================================================
    public function initButton2(s_buttonDisplayName:String, f_clickCallback2:Function,
    o_clickSound:Object = false, b_enabled:Boolean = true, o_container:DisplayObjectContainer = null):Button {
      var o_display:Sprite = getSprite(s_buttonDisplayName, o_container);
      if (!o_display) {
        return(null);
      }
      
      if (o_clickSound is Boolean && o_clickSound == false) {
        o_clickSound = Sounds.BUTTON_CLICK;
      }
      
      var o_button:Button = new Button(o_display, null, o_clickSound);
      o_button.clickCallback2 = f_clickCallback2;
      o_button.enabled = b_enabled;
      return(o_button);
    }
    
    //==================================================================
    // ShowDisplay
    //==================================================================
    /**
     * Shows or hides a display object.
     * @param o_object The object whose visibility is affected. This can be a DisplayObject,
     * an FDisplay, an InputLabel, or an array of these types.
     * @param b_visible Specifies if the object should be visible or not.
     */
    public static function ShowDisplay(o_object:Object, b_visible:Boolean):void {
      const VISIBLE_PROPERTY_NAME:String = "visible";
      
      if (o_object != null && o_object.hasOwnProperty(VISIBLE_PROPERTY_NAME)) {
        o_object[VISIBLE_PROPERTY_NAME] = b_visible;
      } else if (o_object is Array) {
        for each (var o_arrayObject:Object in o_object) {
          BaseView.ShowDisplay(o_arrayObject, b_visible);
        }
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      callbacks = null;
      
      if (display.parent) {
        display.parent.removeChild(display);
      }
      
      super.uninit();
    }
    
    //==================================================================
    // uninitButton
    //==================================================================
    public function uninitButton(o_button:Button):Button {
      if (o_button) {
        o_button.uninit();
      }
      return(null);
    }
    
    //==================================================================
    // uninitFButton
    //==================================================================
    public function uninitFButton(o_button:FButton):FButton {
      if (o_button) {
        o_button.uninit();
      }
      return(null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}