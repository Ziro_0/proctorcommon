/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  IntroView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-17-2017
  ActionScript:	3.0
  Description:	
  History:
    04-17-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.introView {
  import com.greensock.TweenLite;
  import engine.common.baseView.BaseView;
  import engine.common.core.I_Engine;
  import engine.common.data.ClientSharedData;
  import engine.common.ui.progressBar.ProgressBar;
  import flash.display.MovieClip;
  import flash.display.Sprite;
  import flash.text.TextField;
	
  public class IntroView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const TXT_VERSION_PFX:String = "version: ";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    private var mo_startButtonsPanel:StartButtonsPanel;
    private var mo_joinButtonsPanel:JoinButtonsPanel;
    private var mo_progressBar:ProgressBar;
    private var mo_txfStatus:TextField;
    private var mo_loadingIcon:MovieClip;
    private var mo_dimBack:Sprite;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function IntroView(o_engine:I_Engine):void {
      super(o_engine.app.getTemplateClass("IntroViewDisplay_{d}"), o_engine);
			init(o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableJoinButton
    //==================================================================
    public function enableJoinButton(b_isEnabled:Boolean):void {
      mo_joinButtonsPanel.enableJoinButton(b_isEnabled);
    }
    
    //==================================================================
    // enableTextInputs
    //==================================================================
    public function enableTextInputs(b_isEnabled:Boolean):void {
      mo_joinButtonsPanel.inputTextsEnabled = b_isEnabled;
    }
    
    //==================================================================
    // ipAddress (get)
    //==================================================================
    public function get ipAddress():String {
      return(mo_joinButtonsPanel.ipAddress);
    }
    
    //==================================================================
    // ipAddress (set)
    //==================================================================
    public function set ipAddress(s_value:String):void {
      mo_joinButtonsPanel.ipAddress = s_value;
    }
    
    //==================================================================
    // loadingIconVisible (set)
    //==================================================================
    public function set loadingIconVisible(b_value:Boolean):void {
      mo_loadingIcon.visible = mo_dimBack.visible = b_value;
      mo_progressBar.visible = b_value;
      
      if (b_value) {
        mo_loadingIcon.play();
      } else {
        mo_loadingIcon.stop();
      }
    }
    
    //==================================================================
    // loadingProgress (set)
    //==================================================================
    public function set loadingProgress(n_value:Number):void {
      mo_progressBar.value = n_value;
    }
    
    //==================================================================
    // playerName (get)
    //==================================================================
    public function get playerName():String {
      return(mo_joinButtonsPanel.playerName);
    }
    
    //==================================================================
    // playerName (set)
    //==================================================================
    public function set playerName(s_value:String):void {
      mo_joinButtonsPanel.playerName = s_value;
    }
    
    //==================================================================
    // port (get)
    //==================================================================
    public function get port():uint {
      return(mo_joinButtonsPanel.port);
    }
    
    //==================================================================
    // port (set)
    //==================================================================
    public function set port(u_value:uint):void {
      mo_joinButtonsPanel.port = u_value;
    }
    
    //==================================================================
    // statusText (set)
    //==================================================================
    public function set statusText(s_value:String):void {
      mo_txfStatus.text = s_value || "";
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      mo_startButtonsPanel.uninit();
      mo_joinButtonsPanel.uninit();
      mo_loadingIcon.stop();
      super.uninit();
    }
    
    //==================================================================
    // version (set)
    //==================================================================
    public function set version(s_value:String):void {
      getTextField("txf_version", s_value ? TXT_VERSION_PFX + s_value : "");
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // joinButtonsPanelOnBack
    //==================================================================
    internal function joinButtonsPanelOnBack():void {
      hidePanel(mo_joinButtonsPanel);
      showPanel(mo_startButtonsPanel);
      back();
    }
    
    //==================================================================
    // joinButtonsPanelOnDataChanged
    //==================================================================
    internal function joinButtonsPanelOnDataChanged():void {
      var o_sharedData:ClientSharedData = mo_engine.sharedData;
      o_sharedData.ipAddress = ipAddress;
      o_sharedData.name = playerName;
      o_sharedData.port = port;
    }
    
    //==================================================================
    // joinButtonsPanelOnJoin
    //==================================================================
    internal function joinButtonsPanelOnJoin():void {
      complete();
    }
    
    //==================================================================
    // joinButtonsPanelOnWatch
    //==================================================================
    internal function joinButtonsPanelOnWatch():void {
      complete();
    }
    
    //==================================================================
    // startButtonsPanelOnAbout
    //==================================================================
    internal function startButtonsPanelOnAbout():void {
    }
    
    //==================================================================
    // startButtonsPanelOnStart
    //==================================================================
    internal function startButtonsPanelOnStart():void {
      hidePanel(mo_startButtonsPanel);
      showPanel(mo_joinButtonsPanel);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // back
    //==================================================================
    protected function back():void {
      var o_callbacks:I_IntroViewCallbacks = callbacks as I_IntroViewCallbacks;
      if (o_callbacks) {
        o_callbacks.introViewOnBack();
      }
    }
    
    //==================================================================
    // complete
    //==================================================================
    protected function complete():void {
      var o_callbacks:I_IntroViewCallbacks = callbacks as I_IntroViewCallbacks;
      if (o_callbacks) {
        o_callbacks.introViewOnComplete();
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // hidePanel
    //==================================================================
    private function hidePanel(o_panel:BaseView):void {
      o_panel.enabled = false;
      TweenLite.to(o_panel.display, 0.25, {
        alpha: 0,
        onComplete: onTweenHidePanelComplete,
        onCompleteParams: [ o_panel ]
      });
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine):void {
      mo_engine = o_engine;
      initStartButtonsPanel();
      initJoinButtonsPanel();
      initStatusText();
      initLoadingIcon();
    }
    
    //==================================================================
    // initJoinButtonsPanel
    //==================================================================
    private function initJoinButtonsPanel():void {
      mo_joinButtonsPanel = new JoinButtonsPanel(getSprite("join_buttons_panel_display"), this, mo_engine);
      mo_joinButtonsPanel.visible = false;
    }
    
    //==================================================================
    // initLoadingIcon
    //==================================================================
    private function initLoadingIcon():void {
      mo_loadingIcon = getMovieClip("loading_icon_display");
      mo_dimBack = getSprite("dim_back");
      mo_progressBar = new ProgressBar(getSprite("progress_bar_display"));
      loadingIconVisible = false;
    }
    
    //==================================================================
    // initStartButtonsPanel
    //==================================================================
    private function initStartButtonsPanel():void {
      mo_startButtonsPanel = new StartButtonsPanel(getSprite("start_buttons_panel_display"), this);
    }
    
    //==================================================================
    // initStatusText
    //==================================================================
    private function initStatusText():void {
      mo_txfStatus = getTextField("txf_status", "");
    }
    
    //==================================================================
    // onTweenHidePanelComplete
    //==================================================================
    private function onTweenHidePanelComplete(o_panel:BaseView):void {
      o_panel.visible = false;
    }
    
    //==================================================================
    // onTweenShowPanelComplete
    //==================================================================
    private function onTweenShowPanelComplete(o_panel:BaseView):void {
      o_panel.enabled = true;
    }
    
    //==================================================================
    // showPanel
    //==================================================================
    private function showPanel(o_panel:BaseView):void {
      o_panel.enabled = false;
      o_panel.visible = true;
      TweenLite.to(o_panel.display, 0.25, {
        alpha: 1,
        onComplete: onTweenShowPanelComplete,
        onCompleteParams: [ o_panel ]
      });
    }
  }
}