/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  JoinButtonsPanel

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-13-2017
  ActionScript:	3.0
  Description:	
  History:
    05-13-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.introView {
  import engine.common.baseView.BaseView;
  import engine.common.core.I_Engine;
  import engine.common.ui.buttons.Button;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import flash.events.Event;
  import flash.text.TextField;
  import net.data.NetConnectData;

  internal class JoinButtonsPanel extends BaseView implements I_InputLabelCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const TEXT_COLOR_ENABLED:uint = 0x000000;
    private static const TEXT_BACKGROUND_COLOR_ENABLED:uint = 0xffffff;
    private static const TEXT_COLOR_DISABLED:uint = 0x707070;
    private static const TEXT_BACKGROUND_COLOR_DISABLED:uint = 0xbbbbbb;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_introView:IntroView;
    private var mo_inputLabel:InputLabel;
    private var mo_joinButton:Button;
    private var mo_backButton:Button;
    private var mo_txfIpAddress:TextField;
    private var mo_txfPort:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function JoinButtonsPanel(o_spriteOrSpriteClass:Object, o_introView:IntroView,
    o_engine:I_Engine):void {
      super(o_spriteOrSpriteClass);
			init(o_introView, o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableJoinButton
    //==================================================================
    public function enableJoinButton(o_bIsEnabled:Object = null):void {
      var b_isEnabled:Boolean = resolveNullableBool(o_bIsEnabled);
      mo_joinButton.enabled = b_isEnabled && isNameValid() && isIpAddressValid() && isPortValid();
    }
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
      if (s_inputLabelAction == InputLabelActions.CHANGE) {
        enableJoinButton();
        notifyDataChanged();
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      mo_introView = null;
      mo_inputLabel.uninit();
      mo_joinButton = uninitButton(mo_joinButton);
      mo_backButton = uninitButton(mo_backButton);
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // inputTextsEnabled (set)
    //==================================================================
    internal function set inputTextsEnabled(b_value:Boolean):void {
      var u_textColor:uint;
      var u_backgroundColor:uint;
      
      if (b_value) {
        u_textColor = TEXT_COLOR_ENABLED;
        u_backgroundColor = TEXT_BACKGROUND_COLOR_ENABLED;
      } else {
        u_textColor = TEXT_COLOR_DISABLED;
        u_backgroundColor = TEXT_BACKGROUND_COLOR_DISABLED;
      }
     
      var ao_textFields:Array = [
        mo_inputLabel.textField,
        mo_txfIpAddress,
        mo_txfPort
      ];
      
      for each (var o_textField:TextField in ao_textFields) {
        if (!b_value) {
          if (display.stage.focus == o_textField) {
            display.stage.focus = null;
          }
        }
        
        o_textField.mouseEnabled = b_value;
        o_textField.textColor = u_textColor;
        o_textField.backgroundColor = u_backgroundColor;
      }
    }
    
    //==================================================================
    // ipAddress (get)
    //==================================================================
    internal function get ipAddress():String {
      return(mo_txfIpAddress.text || "");
    }
    
    //==================================================================
    // ipAddress (set)
    //==================================================================
    internal function set ipAddress(s_value:String):void {
      mo_txfIpAddress.text = s_value || "";
      enableJoinButton();
    }
    
    //==================================================================
    // playerName (get)
    //==================================================================
    internal function get playerName():String {
      return(mo_inputLabel.text);
    }
    
    //==================================================================
    // playerName (set)
    //==================================================================
    internal function set playerName(s_value:String):void {
      mo_inputLabel.text = s_value || "";
      enableJoinButton();
    }
    
    //==================================================================
    // port (get)
    //==================================================================
    internal function get port():uint {
      return(parseInt(mo_txfPort.text));
    }
    
    //==================================================================
    // port (set)
    //==================================================================
    internal function set port(u_value:uint):void {
      mo_txfPort.text = u_value.toString();
      enableJoinButton();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_introView:IntroView, o_engine:I_Engine):void {
      showDisabledFilter = false;
      mo_introView = o_introView;
      
      mo_inputLabel = new InputLabel(this, this);
      mo_inputLabel.initSoftKeyboardHandler(o_engine, mo_introView.display);
      
      mo_joinButton = initButton("join_button_display", onJoinButtonClick);
      mo_backButton = initButton("back_button_display", onBackButtonClick);
      initIpAddressTextField();
      initPortTextField();
      enableJoinButton();
    }
    
    //==================================================================
    // initIpAddressTextField
    //==================================================================
    private function initIpAddressTextField():void {
      mo_txfIpAddress = getTextField("txf_ip_address");
      mo_txfIpAddress.restrict = "0-9.";
      mo_txfIpAddress.addEventListener(Event.CHANGE, onTxfIpAddressChange, false, 0, true);
    }
    
    //==================================================================
    // initPortTextField
    //==================================================================
    private function initPortTextField():void {
      mo_txfPort = getTextField("txf_port", NetConnectData.SERVER_PORT.toString());
      mo_txfPort.restrict = "0-9";
      mo_txfPort.addEventListener(Event.CHANGE, onTxfPortChange, false, 0, true);
    }
    
    //==================================================================
    // isIpAddressValid
    //==================================================================
    private function isIpAddressValid():Boolean {
      var regEx:RegExp = /\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b/;
      return(regEx.test(ipAddress));
    }
    
    //==================================================================
    // isNameValid
    //==================================================================
    private function isNameValid():Boolean {
      return(Boolean(mo_inputLabel.text.length > 0));
    }
    
    //==================================================================
    // isPortValid
    //==================================================================
    private function isPortValid():Boolean {
      if (!mo_txfPort.text) {
        return(false);
      }
      return(port < 65536);
    }
    
    //==================================================================
    // notifyDataChanged
    //==================================================================
    private function notifyDataChanged():void {
      mo_introView.joinButtonsPanelOnDataChanged();
    }
    
    //==================================================================
    // onBackButtonClick
    //==================================================================
    private function onBackButtonClick(b_isShift:Boolean):void {
      mo_introView.joinButtonsPanelOnBack();
    }
    
    //==================================================================
    // onJoinButtonClick
    //==================================================================
    private function onJoinButtonClick(b_isShift:Boolean):void {
      mo_introView.joinButtonsPanelOnJoin();
    }
    
    //==================================================================
    // onTxfIpAddressChange
    //==================================================================
    private function onTxfIpAddressChange(o_event:Event):void {
      enableJoinButton();
      notifyDataChanged();
    }
    
    //==================================================================
    // onTxfPortChange
    //==================================================================
    private function onTxfPortChange(o_event:Event):void {
      enableJoinButton();
      notifyDataChanged();
    }
    
    //==================================================================
    // resolveNullableBool
    //==================================================================
    private function resolveNullableBool(o_bIsEnabled:Object, b_defaultIfNull:Boolean = true):Boolean {
      return(o_bIsEnabled is Boolean ? o_bIsEnabled as Boolean : b_defaultIfNull);
    }
  }
}