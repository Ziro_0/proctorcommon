/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_IntroViewCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-17-2017
  ActionScript:	3.0
  Description:	
  History:
    04-17-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.introView {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_IntroViewCallbacks extends I_BaseViewCallbacks {
    function introViewOnBack():void;
    function introViewOnComplete():void;
  }
}