/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  LoadingView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-17-2017
  ActionScript:	3.0
  Description:	Display class for a loading bar with status text.
  History:
    04-17-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.loadingView {
	import engine.common.baseView.BaseView;
	import engine.common.baseView.I_BaseViewCallbacks;
  import flash.display.Sprite;
  import flash.text.TextField;
	
  public class LoadingView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
		private var mo_display:Sprite;
		private var mo_loaderMask:Sprite;
		private var mo_bar:Sprite;
		private var mo_txfStatus:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function LoadingView(o_spriteOrSpriteClass:Object, o_callbacks:I_BaseViewCallbacks = null):void {
      super(o_spriteOrSpriteClass, o_callbacks);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
		//==================================================================
		// progress (set)
		//==================================================================
		public function set progress(n_uValue:Number):void {
			if (isNaN(n_uValue) || (n_uValue < 0)) {
				n_uValue = 0;
			} else if (n_uValue > 1.0) {
				n_uValue = 1.0;
			}
			
			mo_loaderMask.scaleX = n_uValue;
		}
		
		//==================================================================
		// status (set)
		//==================================================================
		public function set status(s_text:String):void {
			mo_txfStatus.text = s_text || "";
			mo_txfStatus.visible = (mo_txfStatus.text.length > 0);
		}
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
			var o_preloaderBarDisplay:Sprite = getSprite("preloader_bar_display");
			mo_loaderMask = getSprite("loader_mask", o_preloaderBarDisplay);
			mo_bar = getSprite("preloader_bar_fill_display", o_preloaderBarDisplay);
			mo_bar.mask = mo_loaderMask;
			mo_txfStatus = getTextField("txf_status");
    }
  }
}