/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SimpleTextView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			03-27-2017
  ActionScript:	3.0
  Description:	Display class that includes a text field named "txf_value".
  History:
    03-27-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.simple {
	import engine.common.baseView.BaseView;
	import engine.common.baseView.I_BaseViewCallbacks;
  import flash.text.TextField;
  import flib.utils.FThousandsSeparator;

  public class SimpleTextView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    protected var mo_txfValue:TextField;
    
    private var mo_thousandsSeparator:FThousandsSeparator;
    private var mb_useThousandsSeparator:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SimpleTextView(o_spriteOrSpriteClass:Object):void {
      super(o_spriteOrSpriteClass);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // useThousandsSeparator (get)
    //==================================================================
    public function get useThousandsSeparator():Boolean {
      return(mb_useThousandsSeparator);
    }
    
    //==================================================================
    // useThousandsSeparator (set)
    //==================================================================
    public function set useThousandsSeparator(b_value:Boolean):void {
      mb_useThousandsSeparator = b_value;
    }
    
    //==================================================================
    // useTrailingZeros (get)
    //==================================================================
    public function get useTrailingZeros():Boolean {
      return(mo_thousandsSeparator.useTrailingZeros);
    }
    
    //==================================================================
    // useTrailingZeros (set)
    //==================================================================
    public function set useTrailingZeros(b_value:Boolean):void {
      mo_thousandsSeparator.useTrailingZeros = b_value;
    }
    
    //==================================================================
    // value (set)
    //==================================================================
    public function set value(o_value:Object):void {
      if (!mo_txfValue) {
        return; //sanity check
      }
      
      if (o_value is int) {
        mo_txfValue.text = parseIntValue(o_value as int);
      } else if (o_value != null) {
        setStringValue(o_value.toString());
      } else {
        mo_txfValue.text = "";
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    protected function init():void {
      mo_txfValue = getTextField("txf_value", "");
      mo_thousandsSeparator = new FThousandsSeparator();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // parseIntValue
    //==================================================================
    private function parseIntValue(i_value:int):String {
      if (mb_useThousandsSeparator) {
        return(mo_thousandsSeparator.parse(i_value));
      }
      return(i_value.toString());
    }
    
    //==================================================================
    // setStringValue
    //==================================================================
    private function setStringValue(s_value:String):void {
      mo_txfValue.text = s_value || "";
    }
  }
}