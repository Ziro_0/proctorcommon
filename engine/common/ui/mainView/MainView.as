/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  MainView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.mainView {
  import engine.common.baseView.BaseView;
  import engine.common.core.I_Engine;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.buttons.I_ToggleButton;
  import engine.common.ui.buttons.I_ToggleButtonCallbacks;
  import engine.common.ui.buttons.ToggleButton;
  import engine.common.ui.progressBar.ProgressBar;
  import flash.display.DisplayObject;
  import flash.display.MovieClip;
  import flash.display.Sprite;
  import flib.ui.FDisplay;
  import flib.ui.button.FButton;

  public class MainView extends BaseView implements I_ToggleButtonCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    
    private var mo_exitButton:Button;
    private var mo_joinWatchToggle:ToggleButton;
    private var mo_loadingBarDisplay:ProgressBar;
    
    private var mo_loadingIconDisplay:MovieClip;
    private var mo_loadingBackDisplay:Sprite;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function MainView(o_spriteOrSpriteClass:Object, o_engine:I_Engine):void {
      super(o_spriteOrSpriteClass, o_engine);
			init(o_engine);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // exitButtonVisible (set)
    //==================================================================
    public function set exitButtonVisible(b_value:Boolean):void {
      showDisplay(mo_exitButton, b_value);
    }
    
    //==================================================================
    // getEngine
    //==================================================================
    public function getEngine():I_Engine {
      return(mo_engine);
    }
    
    //==================================================================
    // isInWatchMode (get)
    //==================================================================
    public function get isInWatchMode():Boolean {
      return(mo_joinWatchToggle && mo_joinWatchToggle.isSelected == false);
    }
    
    //==================================================================
    // isInWatchMode (set)
    //==================================================================
    public function set isInWatchMode(b_value:Boolean):void {
      if (mo_joinWatchToggle) {
        mo_joinWatchToggle.isSelected = b_value;
      }
    }
    
    //==================================================================
    // loadingProgress (set)
    //==================================================================
    public function set loadingProgress(n_value:Number):void {
      if (mo_loadingBarDisplay) {
        mo_loadingBarDisplay.value = n_value;
      }
    }
    
    //==================================================================
    // joinWatchShowJoin
    //==================================================================
    public function joinWatchShowJoin():void {
      if (mo_joinWatchToggle) {
        mo_joinWatchToggle.isSelected = false;
      }
    }
    
    //==================================================================
    // joinWatchShowWatch
    //==================================================================
    public function joinWatchShowWatch():void {
      if (mo_joinWatchToggle) {
        mo_joinWatchToggle.isSelected = true;
      }
    }
    
    //==================================================================
    // joinWatchToggleVisible (set)
    //==================================================================
    public function set joinWatchToggleVisible(b_value:Boolean):void {
      showDisplay(mo_joinWatchToggle, b_value);
    }
    
    //==================================================================
    // showLoadingUi
    //==================================================================
    public function showLoadingUi(b_visible:Boolean):void {
      if (mo_loadingIconDisplay) {
        if (b_visible) {
          mo_loadingIconDisplay.play();
        } else {
          mo_loadingIconDisplay.stop();
        }
      }
      
      loadingProgress = 0;
      
      BaseView.ShowDisplay([ mo_loadingBarDisplay, mo_loadingBackDisplay, mo_loadingIconDisplay ],
        b_visible);
    }
    
    //==================================================================
    // toggleButtonOnSelected
    //==================================================================
    public function toggleButtonOnSelected(o_button:I_ToggleButton, b_isSelected:Boolean):void {
      var o_callbacks:I_MainViewCallbacks = callbacks as I_MainViewCallbacks;
      if (!o_callbacks) {
        return;
      }
      
      if (o_button && o_button == mo_joinWatchToggle) {
        o_callbacks.mainViewOnJoinWatchToggle(isInWatchMode);
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      showLoadingUi(false);
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableButton
    //==================================================================
    protected function enableButton(o_buttonFButtonOrToggle:Object, b_enabled:Boolean):void {
      if (o_buttonFButtonOrToggle is Button) {
        var o_button:Button = o_buttonFButtonOrToggle as Button;
        o_button.enabled = b_enabled;
      } else if (o_buttonFButtonOrToggle is FButton) {
        var o_fbutton:FButton = o_buttonFButtonOrToggle as FButton;
        o_fbutton.enabled = b_enabled;
      } else if (o_buttonFButtonOrToggle is ToggleButton) {
        var o_tbutton:ToggleButton = o_buttonFButtonOrToggle as ToggleButton;
        o_tbutton.enabled = b_enabled;
      }
    }
    
    //==================================================================
    // onExitButtonClick
    //==================================================================
    private function onExitButtonClick(b_isShift:Boolean):void {
      var o_callbacks:I_MainViewCallbacks = callbacks as I_MainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnExitButtonClick();
      }
    }
    
    //==================================================================
    // onInit
    //==================================================================
    protected function onInit():void {
    }
    
    //==================================================================
    // showDisplay
    //==================================================================
    protected function showDisplay(o_displayOrFdisplay:Object, b_visible:Boolean):void {
      if (o_displayOrFdisplay is DisplayObject) {
        var o_display:DisplayObject = o_displayOrFdisplay as DisplayObject;
        o_display.visible = b_visible;
      } else if (o_displayOrFdisplay is FDisplay) {
        var o_fdisplay:FDisplay = o_displayOrFdisplay as FDisplay;
        o_fdisplay.visible = b_visible;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine):void {
      showDisabledFilter = false;
      
      mo_engine = o_engine;
      
      initButtons();
      initLoadingUi();
      
      onInit();
    }
    
    //==================================================================
    // initLoadingUi
    //==================================================================
    private function initLoadingUi():void {
      mo_loadingBarDisplay = new ProgressBar(getSprite("progress_bar_display"));
      mo_loadingBackDisplay = getSprite("dim_back");
      mo_loadingIconDisplay = getMovieClip("loading_icon_display");
      showLoadingUi(false);
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_exitButton = initButton("button_exit_display", onExitButtonClick);
      exitButtonVisible = false;
      
      initJoinWatchToggle();
    }
    
    //==================================================================
    // initJoinWatchToggle
    //==================================================================
    private function initJoinWatchToggle():void {
      var o_display:Sprite = getSprite("toggle_join_watch_display");
      if (!o_display) {
        return;
      }
      
      mo_joinWatchToggle = new ToggleButton(o_display, this);
      mo_joinWatchToggle.visible = false;
    }
  }
}