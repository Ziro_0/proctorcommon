/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_MainViewCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.mainView {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_MainViewCallbacks extends I_BaseViewCallbacks {
    function mainViewOnExitButtonClick():void;
    function mainViewOnJoinWatchToggle(b_isInWatchMode:Boolean):void;
  }
}