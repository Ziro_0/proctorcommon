/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  DialogManager

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.dialogs {
  CONFIG::IS_CLIENT {
    import engine.common.core.I_App;
  }
  
  import engine.common.ui.dialogs.BaseDialog;
  import flash.display.DisplayObjectContainer;
	
  public class DialogManager implements IBaseDialogCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    CONFIG::IS_CLIENT {
      private var mo_app:I_App;
    }
    
    private var mo_callbacks:IDialogManagerCallbacks;
    private var mo_dialog:BaseDialog;
    private var mo_dialogDisplayContainer:DisplayObjectContainer;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    CONFIG::IS_CLIENT {
      public function DialogManager(o_app:I_App, o_dialogDisplayContainer:DisplayObjectContainer,
      o_callbacks:IDialogManagerCallbacks):void {
        mo_app = o_app;
        mo_dialogDisplayContainer = o_dialogDisplayContainer;
        mo_callbacks = o_callbacks;
      }
    }
    
    CONFIG::IS_SERVER {
      public function DialogManager(o_dialogDisplayContainer:DisplayObjectContainer,
      o_callbacks:IDialogManagerCallbacks):void {
        mo_dialogDisplayContainer = o_dialogDisplayContainer;
        mo_callbacks = o_callbacks;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // baseDialogOnAction
    //==================================================================
    public function baseDialogOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction == DialogActions.COMPLETE || s_dialogAction == DialogActions.CANCEL) {
        closeDialog();
      }
      
      if (mo_callbacks) {
        mo_callbacks.dialogManagerOnAction(s_dialogId, s_dialogAction, o_data);
      }
    }
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():IDialogManagerCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:IDialogManagerCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // closeDialog
    //==================================================================
    public function closeDialog():void {
      if (mo_dialog) {
        mo_dialog.uninit();
        mo_dialog = null;
      }
    }
    
    //==================================================================
    // dialog (get)
    //==================================================================
    public function get dialog():BaseDialog {
      return(mo_dialog);
    }
    
    //==================================================================
    // dialogDisplayContainer (get)
    //==================================================================
    public function get dialogDisplayContainer():DisplayObjectContainer {
      return(mo_dialogDisplayContainer);
    }
    
    //==================================================================
    // openDialog
    //==================================================================
    public function openDialog(o_dialog:BaseDialog):BaseDialog {
      if (!o_dialog) {
        return(null);
      }
      
      closeDialog();
      mo_dialog = o_dialog;
      addDialogDisplay();
      return(o_dialog);
    }
    
    //==================================================================
    // setDialogData
    //==================================================================
    public function setDialogData(o_data:Object):void {
      if (mo_dialog) {
        mo_dialog.data = o_data;
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      closeDialog();
      callbacks = null;
      mo_dialogDisplayContainer = null;
      
      CONFIG::IS_CLIENT {
        mo_app = null;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getDialog
    //==================================================================
    protected function getDialog():BaseDialog {
      return(mo_dialog);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addDialogDisplay
    //==================================================================
    private function addDialogDisplay():void {
      if (mo_dialog && mo_dialogDisplayContainer) {
        mo_dialogDisplayContainer.addChild(mo_dialog.display);
      }
    }
  }
}