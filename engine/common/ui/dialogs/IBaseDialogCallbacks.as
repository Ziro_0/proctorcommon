/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  IBaseDialogCallbacks

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-15-2018
  ActionScript:  3.0
  Description:  
  History:
    04-15-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.dialogs {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  public interface IBaseDialogCallbacks extends I_BaseViewCallbacks {
    function baseDialogOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void;
  }
}