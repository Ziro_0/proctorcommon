/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SoftKeyboardTextInputHandlerCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			06-06-2018
  ActionScript:	3.0
  Description:	
  History:
    06-06-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.softKeyboardTextInputHandler {
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_SoftKeyboardTextInputHandlerCallbacks {
    function inputTextHandlerOnSoftKybdChange(o_handler:SoftKeyboardTextInputHandler, n_offset:Number):void;
    function inputTextHandlerOnSoftKybdComplete(o_handler:SoftKeyboardTextInputHandler):void;
  }
}