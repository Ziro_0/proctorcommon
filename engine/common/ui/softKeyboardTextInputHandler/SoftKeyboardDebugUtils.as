/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SoftKeyboardDebugUtils

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          11-28-2018
  ActionScript:  3.0
  Description:  
  History:
    11-28-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.softKeyboardTextInputHandler {
  import flash.display.Shape;
  import flash.display.Stage;

  public final class SoftKeyboardDebugUtils {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const DISPLAY_OBJECT_NAME_KEYBOARD:String = "skdu_kybd";
    private static const DISPLAY_OBJECT_NAME_TEXTFIELD:String = "skdu_text";
    private static const DISPLAY_OBJECT_NAME_INTERSECTION:String = "skdu_int";
    
    private static const KEYBOARD_COLOR:uint = 0x0000ff;
    private static const TEXT_COLOR:uint = 0x00ff00;
    private static const INTERSECTION_COLOR:uint = 0xff0000;
    
    private static const DISPLAY_ALPHA:Number = 0.4;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SoftKeyboardDebugUtils() {
      super();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // drawShapes
    //==================================================================
    public function drawShapes(o_stage:Stage, o_metrics:SoftKeyboardMetrics):void {
      if (!o_stage || !o_metrics) {
        return;
      }
      
      drawKeyboardRect(o_stage, o_metrics);
      drawTextfieldRect(o_stage, o_metrics);
      drawIntersectionRect(o_stage, o_metrics);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // drawIntersectionRect
    //==================================================================
    private function drawIntersectionRect(o_stage:Stage, o_metrics:SoftKeyboardMetrics):void {
      var o_shape:Shape = o_stage.getChildByName(DISPLAY_OBJECT_NAME_INTERSECTION) as Shape;
      if (!o_shape) {
        o_shape = new Shape();
        o_shape.name = DISPLAY_OBJECT_NAME_INTERSECTION;
        o_shape.graphics.beginFill(INTERSECTION_COLOR, DISPLAY_ALPHA);
        o_shape.graphics.drawRect(0, 0, o_metrics.intersectionRect.width, o_metrics.intersectionRect.height);
        o_shape.graphics.endFill();
        o_shape.x = o_metrics.intersectionRect.x;
        o_shape.y = o_metrics.intersectionRect.y;
        o_stage.addChild(o_shape);
      }
    }
    
    //==================================================================
    // drawKeyboardRect
    //==================================================================
    private function drawKeyboardRect(o_stage:Stage, o_metrics:SoftKeyboardMetrics):void {
      var o_shape:Shape = o_stage.getChildByName(DISPLAY_OBJECT_NAME_KEYBOARD) as Shape;
      if (!o_shape) {
        o_shape = new Shape();
        o_shape.name = DISPLAY_OBJECT_NAME_KEYBOARD;
        o_shape.graphics.beginFill(KEYBOARD_COLOR, DISPLAY_ALPHA);
        o_shape.graphics.drawRect(0, 0, o_stage.stageWidth, o_metrics.keyboardRect.height);
        o_shape.graphics.endFill();
        o_shape.x = o_metrics.keyboardRect.x;
        o_shape.y = o_metrics.keyboardRect.y;
        o_stage.addChild(o_shape);
      }
    }
    
    //==================================================================
    // drawTextfieldRect
    //==================================================================
    private function drawTextfieldRect(o_stage:Stage, o_metrics:SoftKeyboardMetrics):void {
      var o_shape:Shape = o_stage.getChildByName(DISPLAY_OBJECT_NAME_TEXTFIELD) as Shape;
      if (!o_shape) {
        o_shape = new Shape();
        o_shape.name = DISPLAY_OBJECT_NAME_TEXTFIELD;
        o_shape.graphics.beginFill(TEXT_COLOR, DISPLAY_ALPHA);
        o_shape.graphics.drawRect(0, 0, o_metrics.textRect.width, o_metrics.textRect.height);
        o_shape.graphics.endFill();
        o_shape.x = o_metrics.textRect.x;
        o_shape.y = o_metrics.textRect.y - o_metrics.offset;
        o_stage.addChild(o_shape);
      }
    }
  }
}