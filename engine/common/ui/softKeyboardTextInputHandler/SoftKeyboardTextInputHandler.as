/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SoftKeyboardTextInputHandler

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			06-06-2018
  ActionScript:	3.0
  Description:	Soft keyboard handler currently for Android only.
  History:
    06-06-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.softKeyboardTextInputHandler {
  import engine.common.core.I_Engine;
  import flash.desktop.NativeApplication;
  import flash.display.DisplayObject;
  import flash.events.KeyboardEvent;
  import flash.events.SoftKeyboardEvent;
  import flash.geom.Rectangle;
  import flash.text.TextField;
  import flib.input.FKey;
  import flib.utils.framesDelayer.FFramesDelayer;
  import flib.utils.framesDelayer.IFFramesDelayerCallbacks;

  public class SoftKeyboardTextInputHandler implements IFFramesDelayerCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    private var mo_callbacks:I_SoftKeyboardTextInputHandlerCallbacks;
    private var mo_textField:TextField;
    private var mo_targetDisplay:DisplayObject;
    private var mb_showSoftKybdShapes:Boolean;
    private var mb_isActivating:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SoftKeyboardTextInputHandler(o_engine:I_Engine, o_textField:TextField,
    o_targetDisplay:DisplayObject, o_callbacks:I_SoftKeyboardTextInputHandlerCallbacks = null):void {
      super();
      init(o_engine, o_textField, o_targetDisplay, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_SoftKeyboardTextInputHandlerCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_SoftKeyboardTextInputHandlerCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // frameDelayerOnComplete
    //==================================================================
    public function frameDelayerOnComplete(u_id:uint, o_data:Object):void {
      CONFIG::IS_ANDROID {
        var o_metrics:SoftKeyboardMetrics = new SoftKeyboardMetrics(mo_engine, o_data as TextField);
        
        if (!o_metrics.isKeyboardVisible) {
          if (mb_isActivating) {
            beginAdjustDisplayForSoftKeyboard();
            return;
          }
        }
        
        if (mb_showSoftKybdShapes) {
          debugShowShapes(o_metrics);
        }
        
        if (mo_targetDisplay) {
          mo_targetDisplay.y = -o_metrics.offset / calcScale();
        }
        
        captureSoftKeyInput(o_metrics.offset != 0);
        
        if (mo_callbacks) {
          mo_callbacks.inputTextHandlerOnSoftKybdChange(this, o_metrics.offset);
        }
      }
    }
    
    //==================================================================
    // showSoftKybdShapes (set)
    //==================================================================
    public function set showSoftKybdShapes(b_value:Boolean):void {
      mb_showSoftKybdShapes = b_value;
    }
    
    //==================================================================
    // text (get)
    //==================================================================
    public function get text():String {
      return(mo_textField ? mo_textField.text : "");
    }
    
    //==================================================================
    // text (set)
    //==================================================================
    public function set text(s_value:String):void {
      if (mo_textField) {
        mo_textField.text = s_value || "";
      }
    }
    
    //==================================================================
    // textField (get)
    //==================================================================
    public function get textField():TextField {
      return(mo_textField);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      uninitTextField();
      mo_engine = null;
    }
    
    //==================================================================
    // visible (set)
    //==================================================================
    public function set visible(b_value:Boolean):void {
      if (mo_textField) {
        mo_textField.visible = b_value;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // beginAdjustDisplayForSoftKeyboard
    //==================================================================
    private function beginAdjustDisplayForSoftKeyboard():void {
      const FRAMES_DELAYER_ID:uint = 1;
      new FFramesDelayer(FRAMES_DELAYER_ID, this, mo_engine.app.display.stage.frameRate, mo_textField);
    }
    
    //==================================================================
    // calcScale
    //==================================================================
    private function calcScale():Number {
      var n_screenHeight:Number = mo_engine.app.display.stage.fullScreenHeight;
      return(n_screenHeight / Number(mo_engine.app.deviceProfile.assetsHeight));
    }
    
    //==================================================================
    // captureSoftKeyInput
    //==================================================================
    private function captureSoftKeyInput(b_isSoftKeyboardActive:Boolean):void {
      if (b_isSoftKeyboardActive) {
        NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, onNativeAppKeyDown,
          false, 0, true);
      } else {
        NativeApplication.nativeApplication.removeEventListener(KeyboardEvent.KEY_DOWN, onNativeAppKeyDown);
      }
    }
    
    //==================================================================
    // complete
    //==================================================================
    private function complete():void {
      if (mo_callbacks) {
        mo_callbacks.inputTextHandlerOnSoftKybdComplete(this);
      }
    }
    
    //==================================================================
    // debugShowShapes
    //==================================================================
    private function debugShowShapes(o_metrics:SoftKeyboardMetrics):void {
      var o_debugUtils:SoftKeyboardDebugUtils = new SoftKeyboardDebugUtils();
      o_debugUtils.drawShapes(mo_engine.app.display.stage, o_metrics);
    }
    
    //==================================================================
    // handleSoftKybdChange
    //==================================================================
    private function handleSoftKybdChange():void {
      //With adobe air for android, the screen does not pan when the soft keyboard appears. This would
      // ensure that the input textfield that just got focus is not covered by the soft keyboard.
      //This kludge addresses that issue.
      
      //More details here:
      // "Handling application display changes"
      // http://help.adobe.com/en_US/as3/dev/WSfffb011ac560372f6bc38fcc12e0166e73b-7ffe.html
      beginAdjustDisplayForSoftKeyboard();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_Engine, o_textField:TextField, o_targetDisplay:DisplayObject,
    o_callbacks:I_SoftKeyboardTextInputHandlerCallbacks):void {
      if (!o_engine) {
        trace("InputTextHandler.init. Warning: o_engine param is required.");
        return;
      } else if (!o_textField) {
        trace("InputTextHandler.init. Warning: o_textField param is required.");
        return;
      }
      
      mo_engine = o_engine;
      mo_textField = o_textField;
      mo_targetDisplay = o_targetDisplay;
      callbacks = o_callbacks;
      
      mo_textField.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATE, onTxfSoftKybdActivate); 
      mo_textField.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE, onTxfSoftKybdDeactivate); 
    }
    
    //==================================================================
    // onNativeAppKeyDown
    //==================================================================
    private function onNativeAppKeyDown(o_event:KeyboardEvent):void {
      switch (o_event.keyCode) {
        case FKey.ENTER:
          complete();
          break;
      }
    }
    
    //==================================================================
    // onTxfSoftKybdActivate
    //==================================================================
    private function onTxfSoftKybdActivate(o_event:SoftKeyboardEvent):void {
      mb_isActivating = true;
      handleSoftKybdChange();
    }
    
    //==================================================================
    // onTxfSoftKybdDeactivate
    //==================================================================
    private function onTxfSoftKybdDeactivate(o_event:SoftKeyboardEvent):void {
      mb_isActivating = false;
      handleSoftKybdChange();
    }
    
    //==================================================================
    // scaleRect
    //==================================================================
    private function scaleRect(r_area_in_out:Rectangle, n_scale:Number):void {
      r_area_in_out.x *= n_scale;
      r_area_in_out.y *= n_scale;
      r_area_in_out.width *= n_scale;
      r_area_in_out.height *= n_scale;
    }
    
    //==================================================================
    // uninitTextField
    //==================================================================
    private function uninitTextField():void {
      if (!mo_textField) {
        return;
      }
      
      mo_textField.removeEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATE, onTxfSoftKybdActivate);
      mo_textField.removeEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE,
        onTxfSoftKybdDeactivate);
      mo_textField = null;
    }
  }
}