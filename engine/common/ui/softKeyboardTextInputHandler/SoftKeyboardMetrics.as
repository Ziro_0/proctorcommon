/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SoftKeyboardMetrics

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com/
  Date:          11-27-2018
  ActionScript:  3.0
  Description:  Adobe AIR soft keyboard rect on the stage doesn't report the correct values.
    Found an ANE called MeasureKeyboard that does this for Android.
    https://github.com/freshplanet/ANE-KeyboardSize
  History:
    11-27-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.softKeyboardTextInputHandler {
  CONFIG::IS_ANDROID {
    import com.freshplanet.ane.KeyboardSize.MeasureKeyboard;
  }
  
  import engine.common.core.I_Engine;
  import flash.geom.Rectangle;
  import flash.text.TextField;

  public final class SoftKeyboardMetrics {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_Engine;
    
    private var mr_keyboard:Rectangle;
    private var mr_text:Rectangle;
    private var mr_intersection:Rectangle;
    private var mn_offset:Number;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SoftKeyboardMetrics(o_engine:I_Engine, o_textField:TextField = null) {
      super();
      mo_engine = o_engine;
      update(o_textField);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // intersectionRect (get)
    //==================================================================
    public function get intersectionRect():Rectangle {
      return(mr_intersection);
    }
    
    //==================================================================
    // isKeyboardVisible (get)
    //==================================================================
    public function get isKeyboardVisible():Boolean {
      return(mr_keyboard && !mr_keyboard.isEmpty());
    }
    
    //==================================================================
    // keyboardRect (get)
    //==================================================================
    public function get keyboardRect():Rectangle {
      return(mr_keyboard);
    }
    
    //==================================================================
    // offset (get)
    //==================================================================
    public function get offset():Number {
      return(mn_offset);
    }
    
    //==================================================================
    // textRect (get)
    //==================================================================
    public function get textRect():Rectangle {
      return(mr_text);
    }
    
    //==================================================================
    // update
    //==================================================================
    public function update(o_textField:TextField):void {
      CONFIG::IS_ANDROID {
        if (!mo_engine || !o_textField) {
          return;
        }
        
        mr_keyboard = calcKeyboardSize();
        mr_text = getTextFieldRect(o_textField);
        mr_intersection = mr_keyboard.intersection(mr_text);
        mn_offset = calcKeyboardOffset();
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // calcKeyboardOffset
    //==================================================================
    private function calcKeyboardOffset():Number {
      if (mr_intersection.isEmpty()) {
        return(0);
      }
      
      const MARGIN:int = 8;
      return(mr_text.y - (mr_keyboard.y - mr_text.height) + MARGIN);
    }
    
    //==================================================================
    // calcKeyboardSize
    //==================================================================
    private function calcKeyboardSize():Rectangle {
      var r_size:Rectangle;
      
      CONFIG::IS_ANDROID {
        var o_measureKeyboard:MeasureKeyboard = MeasureKeyboard.getInstance();
        o_measureKeyboard.setKeyboardAdjustDefault();
        
        var n_kybdHeight:Number = o_measureKeyboard.getKeyboardHeight() as Number;
        var n_yKybd:Number = o_measureKeyboard.getKeyboardY() as Number;
        r_size = new Rectangle(0, n_yKybd, mo_engine.app.display.stage.fullScreenWidth, n_kybdHeight);
      }
      
      return(r_size);
    }
    
    //==================================================================
    // getTextFieldRect
    //==================================================================
    private function getTextFieldRect(o_textField:TextField):Rectangle {
      return(o_textField.getRect(mo_engine.display.stage));
    }
  }
}