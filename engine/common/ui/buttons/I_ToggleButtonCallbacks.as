/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_ToggleButtonCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-05-2017
  ActionScript:	3.0
  Description:	
  History:
    04-05-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.buttons {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_ToggleButtonCallbacks extends I_BaseViewCallbacks {
    function toggleButtonOnSelected(o_button:I_ToggleButton, b_isSelected:Boolean):void;
  }
}