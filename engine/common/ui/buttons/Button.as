/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  Button

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			03-22-2017
  ActionScript:	3.0
  Description:	Display class for simple buttons with an option static text label.
  History:
    03-22-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.buttons {
  import engine.common.baseView.BaseView;
  import flash.events.MouseEvent;
  import flash.text.TextField;
  import flib.audio.FSound;
  import flib.ui.button.FButton;

  public class Button extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const TEXT_COLOR_DISABLED:uint = 0x666666;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_button:FButton;
    private var mo_txfLabel:TextField;
    private var mf_clickCallback:Function;
    private var mf_clickCallback2:Function;
    private var mu_enabledTextColor:uint;
    private var mo_clickSound:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function Button(o_spriteOrSpriteClass:Object, f_clickCallback:Function,
    o_simpleButtonOrDisplayName:Object = null, o_clickSound:Object = null):void {
      super(o_spriteOrSpriteClass);
			init(f_clickCallback, o_clickSound, o_simpleButtonOrDisplayName);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clickCallback (get)
    //==================================================================
    public function get clickCallback():Function {
      return(mf_clickCallback);
    }
    
    //==================================================================
    // clickCallback (set)
    //==================================================================
    /**
     * Specifies a function that is called when this button is clicked. The function takes these paremeters:
     * isShift: Boolean. True if the shift key was help while the button was clicked.
     */
    public function set clickCallback(f_value:Function):void {
      mf_clickCallback = f_value;
    }
    
    //==================================================================
    // clickCallback2 (get)
    //==================================================================
    public function get clickCallback2():Function {
      return(mf_clickCallback2);
    }
    
    //==================================================================
    // clickCallback2 (set)
    //==================================================================
    /**
     * Specifies a function that is called when this button is pressed. The function takes these parameters:
     * button: Button. The button that was pressed
     * event: Event. Either a MouseEvent if the device is not mobile, or TouchEvent if it is.
     * ExtUtils.isMobile() is used to determine which event type is used.
     * @see ExtUtils.isMobile
     */
    public function set clickCallback2(f_value:Function):void {
      mf_clickCallback2 = f_value;
    }
    
    //==================================================================
    // clickSound (get)
    //==================================================================
    public function get clickSound():Object {
      return(mo_clickSound);
    }
    
    //==================================================================
    // clickSound (set)
    //==================================================================
    public function set clickSound(o_value:Object):void {
      mo_clickSound = o_value;
    }
    
    //==================================================================
    // label (get)
    //==================================================================
    public function get label():TextField {
      return(mo_txfLabel);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      if (mo_button) {
        mo_button.uninit();
      }
      
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // playClickSound
    //==================================================================
    protected function playClickSound():void {
      FSound.Play(mo_clickSound);
    }
    
    //==================================================================
    // updateDisabledDisplay
    //==================================================================
    protected override function updateDisabledDisplay():void {
      if (mo_txfLabel) {
        mo_txfLabel.textColor = enabled ? mu_enabledTextColor : TEXT_COLOR_DISABLED;
      } else {
        super.updateDisabledDisplay();
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(f_clickCallback:Function, o_clickSound:Object,
    o_simpleButtonOrDisplayName:Object):void {
      mf_clickCallback = f_clickCallback;
      mo_clickSound = o_clickSound;
      
      if (o_simpleButtonOrDisplayName == null) {
        o_simpleButtonOrDisplayName = "btn";
      }
      
      mo_button = FButton.CreateFromSimpleButton(o_simpleButtonOrDisplayName, null, onButtonClick, display);
      initLabel();
    }
    
    //==================================================================
    // initLabel
    //==================================================================
    private function initLabel():void {
      mo_txfLabel = getTextField("txf_label");
      if (!mo_txfLabel) {
        return;
      }
      
      mu_enabledTextColor = mo_txfLabel.textColor;
      mo_txfLabel.mouseEnabled = false;
    }
    
    //==================================================================
    // onButtonClick
    //==================================================================
    private function onButtonClick(o_event:Object):void {
      playClickSound();
      
      if (mf_clickCallback is Function) {
        mf_clickCallback(o_event.shiftKey);
      }
      
      if (mf_clickCallback2 is Function) {
        mf_clickCallback2(this, o_event);
      }
    }
  }
}