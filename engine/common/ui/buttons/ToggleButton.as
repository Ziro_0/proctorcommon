/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ToggleButton

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			07-10-2018
  ActionScript:	3.0
  Description:	
  History:
    04-04-2017:	Started.
    07-10-2018: Added isRadioButtonLike property.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.buttons {
  import engine.common.baseView.BaseView;
  import flash.display.SimpleButton;
  import flash.display.Sprite;
  import flib.ui.button.FButton;
	
  public class ToggleButton extends BaseView implements I_ToggleButton {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mb_isSelected:Boolean;
    private var mb_isRadioButtonLike:Boolean;
    private var mo_buttonOn:Object;
    private var mo_buttonOff:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ToggleButton(o_spriteOrSpriteClass:Object,
    o_callbacks:I_ToggleButtonCallbacks = null):void {
      super(o_spriteOrSpriteClass, o_callbacks);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isRadioButtonLike (get)
    //==================================================================
    public function get isRadioButtonLike():Boolean {
      return(mb_isRadioButtonLike);
    }
    
    //==================================================================
    // isRadioButtonLike (set)
    //==================================================================
    public function set isRadioButtonLike(b_value:Boolean):void {
      mb_isRadioButtonLike = b_value;
    }
    
    //==================================================================
    // isSelected (get)
    //==================================================================
    public function get isSelected():Boolean {
      return(mb_isSelected);
    }
    
    //==================================================================
    // isSelected (set)
    //==================================================================
    public function set isSelected(b_value:Boolean):void {
      mb_isSelected = b_value;
      setButtonVisible(mo_buttonOff, !mb_isSelected);
      setButtonVisible(mo_buttonOn, mb_isSelected);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      if (mo_buttonOff is FButton) {
        mo_buttonOff = uninitFButton(mo_buttonOff as FButton);
      } else if (mo_buttonOff is Button) {
        mo_buttonOff = uninitButton(mo_buttonOff as Button);
      }
      
      if (mo_buttonOn is FButton) {
        mo_buttonOn = uninitFButton(mo_buttonOn as FButton);
      } else if (mo_buttonOn is Button) {
        mo_buttonOn = uninitButton(mo_buttonOn as Button);
      }
      
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      for (var i_index:int = 0; i_index < display.numChildren; i_index++) {
        processFButtonFromSimple(i_index) ||
          processButton(i_index);
      }
      
      isSelected = false;
    }
    
    //==================================================================
    // notifySelected
    //==================================================================
    private function notifySelected():void {
      var o_callbacks:I_ToggleButtonCallbacks = callbacks as I_ToggleButtonCallbacks;
      if (o_callbacks) {
        o_callbacks.toggleButtonOnSelected(this, mb_isSelected);
      }
    }
    
    //==================================================================
    // onBtnOffClick
    //==================================================================
    private function onBtnOffClick(o_event:Object):void {
      isSelected = true;
      notifySelected();
    }
    
    //==================================================================
    // onBtnOnClick
    //==================================================================
    private function onBtnOnClick(o_event:Object):void {
      if (isRadioButtonLike) {
        return; //radio buttons cant be turned off via click
      }
      
      isSelected = false;
      notifySelected();
    }
    
    //==================================================================
    // processButton
    //==================================================================
    private function processButton(i_index:int):Boolean {
      var o_display:Sprite = display.getChildAt(i_index) as Sprite;
      if (!o_display) {
        return(false); //sanity check
      }
      
      var s_name:String = o_display.name;
      if (s_name.indexOf("button_off_") == 0) {
        mo_buttonOff = new Button(o_display, onBtnOffClick);
      } else if (s_name.indexOf("button_on_") == 0) {
        mo_buttonOn = new Button(o_display, onBtnOnClick);
      }
      
      return(true);
    }
    
    //==================================================================
    // processFButtonFromSimple
    //==================================================================
    private function processFButtonFromSimple(i_index:int):Boolean {
      var o_simpleButton:SimpleButton = display.getChildAt(i_index) as SimpleButton;
      if (!o_simpleButton) {
        return(false); //sanity check
      }
      
      var s_name:String = o_simpleButton.name;
      if (s_name.indexOf("btn_off_") == 0) {
        mo_buttonOff = FButton.CreateFromSimpleButton(o_simpleButton);
        mo_buttonOff.initClickableObject(mo_buttonOff.display, onBtnOffClick);
      } else if (s_name.indexOf("btn_on_") == 0) {
        mo_buttonOn = FButton.CreateFromSimpleButton(o_simpleButton);
        mo_buttonOn.initClickableObject(mo_buttonOn.display, onBtnOnClick);
      }
      
      return(true);
    }
    
    //==================================================================
    // setButtonVisible
    //==================================================================
    private function setButtonVisible(o_button:Object, b_visible:Boolean):void {
      if (o_button && "visible" in o_button) {
        o_button.visible = b_visible;
      }
    }
  }
}