/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  LabelToggleButton

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-05-2017
  ActionScript:	3.0
  Description:	
  History:
    04-05-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.ui.buttons {
	import engine.common.ui.simple.SimpleTextView;
  import flash.display.Sprite;
	
  public class LabelToggleButton extends SimpleTextView implements I_ToggleButton,
  I_ToggleButtonCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_toggleButton:ToggleButton;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function LabelToggleButton(o_spriteOrSpriteClass:Object, s_text:String = null,
    o_callbacks:I_ToggleButtonCallbacks = null):void {
      super(o_spriteOrSpriteClass);
			
      callbacks = o_callbacks;
      if (s_text != null) {
        value = s_text;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isSelected (get)
    //==================================================================
    public function get isSelected():Boolean {
      return(mo_toggleButton && mo_toggleButton.isSelected);
    }
    
    //==================================================================
    // isSelected (set)
    //==================================================================
    public function set isSelected(b_value:Boolean):void {
      if (mo_toggleButton) {
        mo_toggleButton.isSelected = b_value;
      }
    }
    
    //==================================================================
    // toggleButtonOnSelected
    //==================================================================
    public function toggleButtonOnSelected(o_button:I_ToggleButton, b_isSelected:Boolean):void {
      if (!mo_toggleButton || o_button != mo_toggleButton) {
        return; //sanity checks
      }
      
      var o_callbacks:I_ToggleButtonCallbacks = callbacks as I_ToggleButtonCallbacks;
      if (o_callbacks) {
        o_callbacks.toggleButtonOnSelected(this, b_isSelected);
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public override function uninit():void {
      if (mo_toggleButton) {
        mo_toggleButton.uninit();
      }
      
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    protected override function init():void {
      super.init();
      
      var o_toggleDisplay:Sprite = getSprite("card_toggle_button");
      if (o_toggleDisplay) {
        mo_toggleButton = new ToggleButton(o_toggleDisplay, this);
      }
      
      if (mo_txfValue) {
        mo_txfValue.mouseEnabled = false;
      }
    }
  }
}