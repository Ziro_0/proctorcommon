/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_BinaryImagesLoaderCallbacks

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          08-14-2018
  Description:  
  History:
    08-14-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.binaryImagesLoader {
  
  public interface I_BinaryImagesLoaderCallbacks {
    function binaryImagesLoaderComplete(o_loader:BinaryImagesLoader):void;
  }
}