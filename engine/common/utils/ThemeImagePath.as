/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ThemeImagePath

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-27-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils {
  import flash.filesystem.File;

  public final class ThemeImagePath {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FILE_IMAGE_EXT:String = ".png";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var ms_baseThemesUrl:String;
    private var ms_deviceProfileId:String;
    private var ms_fileExt:String;
    private var mb_isApplicationStorageMode:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ThemeImagePath(s_baseThemesUrl:String, s_deviceProfileId:String,
    s_fileExt:String = null) {
      ms_baseThemesUrl = s_baseThemesUrl || "";
      ms_deviceProfileId = s_deviceProfileId || "";
      ms_fileExt = s_fileExt || FILE_IMAGE_EXT;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createFileUrl
    //==================================================================
    public function createFileUrl(s_themeId:String, s_gameId:String,
    s_themeComponentName:String):String {
      var s_path:String = createFileUrlPath(s_themeId, s_themeComponentName);
      var s_fileName:String = s_gameId + "_" + ms_deviceProfileId;
      return(s_path + s_fileName + ms_fileExt);
    }
    
    //==================================================================
    // createGameOrNonGameFileUrl
    //==================================================================
    public function createGameOrNonGameFileUrl(s_themeId:String, s_gameId:String,
    s_themeComponentName:String):String {
      return(s_gameId ?
        createFileUrl(s_themeId, s_gameId, s_themeComponentName) :
        createNonGameFileUrl(s_themeId, s_themeComponentName));
    }
    
    //==================================================================
    // createNonGameFileUrl
    //==================================================================
    public function createNonGameFileUrl(s_themeId:String, s_themeComponentName:String):String {
      var s_path:String = createFileUrlPath(s_themeId, s_themeComponentName);
      var s_fileName:String = s_themeComponentName + "_" + ms_deviceProfileId;
      return(s_path + s_fileName + ms_fileExt);
    }
    
    //==================================================================
    // isApplicationStorageMode (get)
    //==================================================================
    public function get isApplicationStorageMode():Boolean {
      return(mb_isApplicationStorageMode);
    }
    
    //==================================================================
    // isApplicationStorageMode (set)
    //==================================================================
    public function set isApplicationStorageMode(b_value:Boolean):void {
      mb_isApplicationStorageMode = b_value;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // appendTrailingPathDelimiter
    //==================================================================
    private function appendTrailingPathDelimiter(s_path:String):String {
      var i_lastIndex:int = s_path.length - 1;
      if (s_path.lastIndexOf("\\") != i_lastIndex &&
      s_path.lastIndexOf("/") != i_lastIndex) {
        s_path = s_path.concat("/");
      }
      return(s_path);
    }
    
    //==================================================================
    // createBasePath
    //==================================================================
    private function createBasePath():String {
      var o_file:File = mb_isApplicationStorageMode ?
        File.applicationStorageDirectory.resolvePath(ms_baseThemesUrl) :
        File.applicationDirectory.resolvePath(ms_baseThemesUrl);
        
      var s_path:String = appendTrailingPathDelimiter(o_file.url);
      return(s_path);
    }
    
    //==================================================================
    // createFileUrlPath
    //==================================================================
    private function createFileUrlPath(s_themeId:String, s_themeComponentName:String):String {
      const BASE_PATH:String = createBasePath();
      const THEME_ID_PATH:String = s_themeId + "/";
      const THEME_COMPONENT_PATH:String = s_themeComponentName + "/";
      return(BASE_PATH + THEME_ID_PATH + THEME_COMPONENT_PATH);
    }
  }
}