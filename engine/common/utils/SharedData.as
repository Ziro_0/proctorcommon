/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SharedData

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-02-2018
  ActionScript:  3.0
  Description:  
  History:
    05-02-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils {
  import flash.net.SharedObject;
  import net.data.NetConnectData;
  
  public class SharedData {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const PROP_IP_ADDRESS:String = "ipAddress"
    private static const PROP_PORT:String = "port";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_sharedObject:SharedObject;
    private var ms_errorMessage:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SharedData() {
      super();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // create
    //==================================================================
    public function create(s_name:String):Boolean {
      ms_errorMessage = null;
      
      try {
        mo_sharedObject = SharedObject.getLocal(s_name);
      } catch (o_error:Error) {
        ms_errorMessage = o_error.message;
        trace("SharedData.create. Error: " + ms_errorMessage);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // errorMessage (get)
    //==================================================================
    public function get errorMessage():String {
      return(ms_errorMessage);
    }
    
    //==================================================================
    // ipAddress (get)
    //==================================================================
    public function get ipAddress():String {
      return(getString(PROP_IP_ADDRESS));
    }
    
    //==================================================================
    // ipAddress (set)
    //==================================================================
    public function set ipAddress(s_value:String):void {
      return(setProperty(PROP_IP_ADDRESS, s_value));
    }
    
    //==================================================================
    // port (get)
    //==================================================================
    public function get port():uint {
      return(getUint(PROP_PORT, NetConnectData.SERVER_PORT));
    }
    
    //==================================================================
    // port (set)
    //==================================================================
    public function set port(u_value:uint):void {
      return(setProperty(PROP_PORT, u_value));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getString
    //==================================================================
    protected function getString(s_property:String, s_default:String = ""):String {
      if (!mo_sharedObject || !s_property) {
        return(s_default);
      }
      
      if (s_property in mo_sharedObject.data) {
        return(mo_sharedObject.data[s_property] as String);
      }
      
      mo_sharedObject.data[s_property] = s_default;
      return(s_default);
    }
    
    //==================================================================
    // getUint
    //==================================================================
    protected function getUint(s_property:String, u_default:uint = 0):uint {
      if (!mo_sharedObject || !s_property) {
        return(u_default);
      }
      
      if (s_property in mo_sharedObject.data) {
        return(mo_sharedObject.data[s_property] as uint);
      }
      
      setProperty(s_property, u_default);
      return(u_default);
    }
    
    //==================================================================
    // setProperty
    //==================================================================
    protected function setProperty(s_property:String, o_value:Object):void {
      if (mo_sharedObject && s_property) {
        mo_sharedObject.data[s_property] = o_value;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}