/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_InputLabelViewCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-15-2017
  ActionScript:	3.0
  Description:	
  History:
    05-15-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.inputLabel {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_InputLabelCallbacks {
    function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void;
  }
}