/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  InputLabel

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:         05-13-2017
  ActionScript:  3.0
  Description:  
  History:
    05-13-2017:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.inputLabel {
  import engine.common.baseView.BaseView;
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.core.I_Engine;
  import engine.common.ui.softKeyboardTextInputHandler.I_SoftKeyboardTextInputHandlerCallbacks;
  import engine.common.ui.softKeyboardTextInputHandler.SoftKeyboardTextInputHandler;
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.FocusEvent;
  import flash.geom.Rectangle;
  import flash.text.TextField;
  
  public class InputLabel {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const DEFAULT_INPUT_TEXTFIELD_NAME:String = "txf_input";
    private static const DEFAULT_INSTRUCTIONS_TEXTFIELD_NAME:String = "txf_instructions";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_InputLabelCallbacks;
    private var mo_softKybdTextInputHandler:SoftKeyboardTextInputHandler;
    private var mo_txfInput:TextField;
    private var mo_txfInstructions:TextField;
    private var mo_container:DisplayObjectContainer;
    private var mb_visible:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function InputLabel(o_displayObjectContainerOrBaseView:Object,
    o_callbacks:I_InputLabelCallbacks = null, s_defaultText:String = null,
    o_txfInputNameOrField:Object = null, o_txfInstructionsNameOrField:Object = null):void {
      super();
      init(o_displayObjectContainerOrBaseView, s_defaultText,
        o_txfInputNameOrField || DEFAULT_INPUT_TEXTFIELD_NAME,
        o_txfInstructionsNameOrField || DEFAULT_INSTRUCTIONS_TEXTFIELD_NAME);
      mo_callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_InputLabelCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_InputLabelCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // debugDrawSoftKeyboardShapesEnabled (set)
    //==================================================================
    public function set debugDrawSoftKeyboardShapesEnabled(b_value:Boolean):void {
      if (mo_softKybdTextInputHandler) {
        mo_softKybdTextInputHandler.showSoftKybdShapes = b_value;
      }
    }
    
    //==================================================================
    // initSoftKeyboardHandler
    //==================================================================
    public function initSoftKeyboardHandler(o_engine:I_Engine, o_targetDisplay:DisplayObject,
    o_softKybdCallbacks:I_SoftKeyboardTextInputHandlerCallbacks = null):void {
      if (o_engine && !mo_softKybdTextInputHandler) {
        mo_softKybdTextInputHandler = new SoftKeyboardTextInputHandler(o_engine, mo_txfInput,
          o_targetDisplay, o_softKybdCallbacks);
      }
    }
    
    //==================================================================
    // text (get)
    //==================================================================
    public function get text():String {
      return(mo_txfInput ? mo_txfInput.text : "");
    }
    
    //==================================================================
    // text (set)
    //==================================================================
    public function set text(s_value:String):void {
      if (!mo_txfInput) {
        return;
      }
      
      mo_txfInput.text = s_value || "";
      updateInstructionsTextVisibility();
    }
    
    //==================================================================
    // textField (get)
    //==================================================================
    public function get textField():TextField {
      return(mo_txfInput);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      uninitSoftKeyboardHandler();
      uninitInputTextField();
    }
    
    //==================================================================
    // uninitSoftKeyboardHandler
    //==================================================================
    public function uninitSoftKeyboardHandler():void {
      if (mo_softKybdTextInputHandler) {
        mo_softKybdTextInputHandler.uninit();
        mo_softKybdTextInputHandler = null;
      }
    }
    
    //==================================================================
    // visible (set)
    //==================================================================
    public function set visible(b_value:Boolean):void {
      mb_visible = b_value;
      
      if (mo_txfInput) {
        mo_txfInput.visible = b_value;
      }
      
      updateInstructionsTextVisibility();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getTextField
    //==================================================================
    private function getTextField(o_txfNameOrField:Object, s_defaultText:String = null):TextField {
      var o_textField:TextField;
      if (o_txfNameOrField is TextField) {
        o_textField = o_txfNameOrField as TextField;
        if (o_textField && s_defaultText != null) {
          o_textField.text = s_defaultText;
        }
      } else if (o_txfNameOrField is String) {
        o_textField = mo_container.getChildByName(o_txfNameOrField as String) as TextField;
        if (o_textField && s_defaultText != null) {
          o_textField.text = s_defaultText;
        }
      }
      
      return(o_textField);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_displayObjectContainerOrBaseView:Object, s_defaultText:String,
    o_txfInputNameOrField:Object, o_txfInstructionsNameOrField:Object):void {
      initContainer(o_displayObjectContainerOrBaseView);
      initInputTextField(s_defaultText, o_txfInputNameOrField);
      initInstructionsTextField(o_txfInstructionsNameOrField);
    }
    
    //==================================================================
    // initContainer
    //==================================================================
    private function initContainer(o_displayObjectContainerOrBaseView:Object):void {
      var o_container:DisplayObjectContainer =
        o_displayObjectContainerOrBaseView as DisplayObjectContainer;
        
      if (!o_container) {
        if (o_displayObjectContainerOrBaseView is BaseView) {
          o_container = (o_displayObjectContainerOrBaseView as BaseView).display;
          if (!o_container) {
            return;
          }
        }
      }
      
      mo_container = o_container;
      if (!mo_container.stage) {
        mo_container.addEventListener(Event.ADDED_TO_STAGE, onContainerAddedToStage, false, 0, true);
      }
    }
    
    //==================================================================
    // initInputTextField
    //==================================================================
    private function initInputTextField(s_defaultText:String, o_txfInputNameOrField:Object):void {
      mo_txfInput = getTextField(o_txfInputNameOrField, s_defaultText);
      if (!mo_txfInput) {
        return;
      }
      
      mo_txfInput.addEventListener(FocusEvent.FOCUS_IN, onTxfInputFocusIn, false, 0, true);
      mo_txfInput.addEventListener(FocusEvent.FOCUS_OUT, onTxfInputFocusOut, false, 0, true);
      mo_txfInput.addEventListener(Event.CHANGE, onTxfInputChange, false, 0, true);
    }
    
    //==================================================================
    // initInstructionsTextField
    //==================================================================
    private function initInstructionsTextField(o_txfInstructionsNameOrField:Object):void {
      mo_txfInstructions = getTextField(o_txfInstructionsNameOrField);
      if (mo_txfInstructions) {
        mo_txfInstructions.mouseEnabled = false;
      }
      
      updateInstructionsTextVisibility();
    }
    
    //==================================================================
    // onContainerAddedToStage
    //==================================================================
    private function onContainerAddedToStage(o_event:Event):void {
      mo_container.removeEventListener(Event.ADDED_TO_STAGE, onContainerAddedToStage);
      updateInstructionsTextVisibility();
    }
    
    //==================================================================
    // onTxfInputChange
    //==================================================================
    private function onTxfInputChange(o_event:Event):void {
      if (mo_callbacks != null) {
        mo_callbacks.inputLabelOnAction(this, InputLabelActions.CHANGE);
      }
    }
    
    //==================================================================
    // onTxfInputFocusIn
    //==================================================================
    private function onTxfInputFocusIn(o_event:FocusEvent):void {
      updateInstructionsTextVisibility();
      if (mo_callbacks != null) {
        mo_callbacks.inputLabelOnAction(this, InputLabelActions.FOCUS_IN);
      }
    }
    
    //==================================================================
    // onTxfInputFocusOut
    //==================================================================
    private function onTxfInputFocusOut(o_event:FocusEvent):void {
      updateInstructionsTextVisibility();
      if (mo_callbacks != null) {
        mo_callbacks.inputLabelOnAction(this, InputLabelActions.FOCUS_OUT);
      }
    }
    
    //==================================================================
    // uninitInputTextField
    //==================================================================
    private function uninitInputTextField():void {
      if (!mo_txfInput) {
        return;
      }
      
      mo_txfInput.removeEventListener(FocusEvent.FOCUS_IN, onTxfInputFocusIn);
      mo_txfInput.removeEventListener(FocusEvent.FOCUS_OUT, onTxfInputFocusOut);
      
      if (mo_txfInput.stage && mo_txfInput.stage.focus == mo_txfInput) {
        mo_txfInput.stage.focus = null;
      }
    }
    
    //==================================================================
    // updateInstructionsTextVisibility
    //==================================================================
    private function updateInstructionsTextVisibility():void {
      if (!mo_txfInstructions || !mo_txfInput || !mo_container || !mo_container.stage) {
        return; //must all be valid
      }
      
      if (mo_container.stage.focus == mo_txfInput) {
        //if the input field has the focus, hide the instructions text. it is assumed that the instructions text
        // also occupies the space of the input text
        mo_txfInstructions.visible = false;
      } else {
        //the input field does not have focus, so only show the instructions text if the input is empty
        mo_txfInstructions.visible = mb_visible && Boolean(mo_txfInput.text.length == 0);
      }
    }
  }
}