/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  DeviceProfiles

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:         11-25-2015
  ActionScript:  3.0
  Description:  
  History:
    11-25-2015:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.deviceProfiles {

  public final class DeviceProfiles {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    public static const DEFAULT_ID:String = "3_4";
    
    private static const TABLE:Array = [
      new DeviceProfile({
        id: DEFAULT_ID,
        sizes: [
          [ 1536, 2048 ],
          [ 768, 1024 ],
          [ 600, 800 ]
        ],
        
        assetsSize: [ 1536, 2048 ],
        ratio: [ 3, 4 ]
      }),
      
      new DeviceProfile({
        id: "9_16",
        sizes: [
          [  2160, 3840 ],
          [  1080, 1920 ],
          [  720, 1280 ],
          [ 640, 1136 ],
          [ 750, 1334 ],
          [ 540, 960 ]
        ],
        
        assetsSize: [ 1080, 1920 ],
        ratio: [ 9, 16 ],
        templateScale: 2
      }),
      
      new DeviceProfile({
        id: "2_3",
        sizes: [
          [  2560, 3840 ],
          [  1280, 1920 ],
          [  800, 1205 ],
          [  640, 960 ],
          [  320, 480 ]
        ],
        
        assetsSize: [ 1280, 1920 ],
        ratio: [ 2, 3 ],
        templateScale: 2
      })
    ];
    
    public static const DEFAULT_PROFILE:DeviceProfile = TABLE[0] as DeviceProfile;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // GetClosetsMatchingProfile
    //==================================================================
    public static function GetClosetsMatchingProfile(u_width:uint, u_height:uint):DeviceProfile {
      var n_targetAspectRatio:Number = u_width / u_height;
      if (isNaN(n_targetAspectRatio) || n_targetAspectRatio <= 0) {
        return(null);
      }
      
      var o_closestMatchingProfile:DeviceProfile;
      var n_closestDelta:Number;
      for each (var o_profile:DeviceProfile in TABLE) {
        var n_profileAspectRatio:Number = o_profile.ratioWidth / o_profile.ratioHeight;
        var n_delta:Number = Math.abs(n_targetAspectRatio - n_profileAspectRatio);
        if (o_closestMatchingProfile == null) {
          o_closestMatchingProfile = o_profile;
          n_closestDelta = n_delta;
        } else {
          if (n_delta < n_closestDelta) {
            o_closestMatchingProfile = o_profile;
            n_closestDelta = n_delta;
          }
        }
      }
      
      return(o_closestMatchingProfile);
    }
    
    //==================================================================
    // GetIds
    //==================================================================
    public static function GetIds():Array {
      var as_ids:Array = [ ];
      
      for each (var o_profile:DeviceProfile in TABLE) {
        as_ids.push(o_profile.id);
      }
      
      return(as_ids);
    }
    
    //==================================================================
    // GetProfile
    //==================================================================
    public static function GetProfile(u_width:uint, u_height:uint):DeviceProfile {
      for each (var o_profile:DeviceProfile in TABLE) {
        if (isMatchingProfile(o_profile, u_width, u_height)) {
          return(o_profile);
        }
      }
      
      return(null);
    }
    
    //==================================================================
    // GetProfileById
    //==================================================================
    public static function GetProfileById(s_deviceProfileId:String):DeviceProfile {
      for each (var o_profile:DeviceProfile in TABLE) {
        if (o_profile.id == s_deviceProfileId) {
          return(o_profile);
        }
      }
      
      return(null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isMatchingProfile
    //==================================================================
    private static function isMatchingProfile(o_profile:DeviceProfile, u_width:uint,
    u_height:uint):Boolean {
      var au_sizes:Array = o_profile.sizes;
      for each (var au_dimensions:Array in au_sizes) {
        var u_dimWidth:uint = uint(au_dimensions[0]);
        if (u_dimWidth != u_width) {
          continue;
        }
        
        var u_dimHeight:uint = uint(au_dimensions[1]);
        if (u_dimHeight == u_height) {
          return(true);
        }
      }
      
      return(false);
    }
  }
}