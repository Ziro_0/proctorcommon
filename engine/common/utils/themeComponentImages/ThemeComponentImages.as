/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ThemeComponentImages

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          01-09-2019
  ActionScript:  3.0
  Description:  
  History:
    01-09-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.themeComponentImages {
  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.ImageLoader;
  import com.greensock.loading.LoaderMax;
  import com.greensock.loading.LoaderStatus;
  import com.greensock.loading.core.LoaderCore;
  import engine.common.core.I_Engine;
  import engine.common.data.CustomThemePath;
  import engine.common.utils.ThemeImagePath;
  import engine.common.utils.themeComponentImages.I_ThemeComponentImagesCallbacks;
  import engine.common.utils.themeImagesDownloader.ThemeImagesDownloader;
  import flash.display.Bitmap;
  import flash.filesystem.File;
  import flib.utils.FDataBlock;

  public class ThemeComponentImages {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_ThemeComponentImagesCallbacks;
    private var mo_engine:I_Engine;
    
    private var mo_imagesPath:ThemeImagePath;
    private var mo_customImagesPath:ThemeImagePath;
    
    private var mo_downloader:ThemeImagesDownloader;
    
    private var mo_maxLoader:LoaderMax;
    
    //data: ThemeComponentImagesStates (enum-like)
    private var mu_state:uint;
    
    //key: String (theme id + component name + game id)
    //data: Bitmap
    private var mo_imagesByKey:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ThemeComponentImages(o_engine:I_Engine, s_baseThemesUrl:String,
    s_customBaseThemesUrl:String, o_callbacks:I_ThemeComponentImagesCallbacks = null) {
      super();
      mo_imagesByKey = {};
      callbacks = o_callbacks;
      
      mo_imagesPath = new ThemeImagePath(s_baseThemesUrl, o_engine.app.deviceProfile.id);
      mo_imagesPath.isApplicationStorageMode = true;
      
      mo_customImagesPath = new ThemeImagePath(s_customBaseThemesUrl, o_engine.app.deviceProfile.id);
      mo_customImagesPath.isApplicationStorageMode = true;
      
      mo_downloader = new ThemeImagesDownloader(o_engine, s_baseThemesUrl, s_customBaseThemesUrl);
      
      mu_state = ThemeComponentImagesStates.IDLE;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_ThemeComponentImagesCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_ThemeComponentImagesCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // dispose
    //==================================================================
    public function dispose():void {
      if (mo_maxLoader) {
        mo_maxLoader.dispose(true);
        mo_maxLoader = null;
      }
      
      mo_imagesByKey = {};
      
      mu_state = ThemeComponentImagesStates.IDLE;
    }
    
    //==================================================================
    // getImage
    //==================================================================
    public function getImage(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):Bitmap {
      var s_key:String = getImageKey(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName);
      return(s_key in mo_imagesByKey ? mo_imagesByKey[s_key] as Bitmap : null);
    }
    
    //==================================================================
    // imageExists
    //==================================================================
    public function imageExists(s_themeId:String,  s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):Boolean {
      return(getImage(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName) != null);  
    }
    
    //==================================================================
    // load
    //==================================================================
    public function load(o_componentThemeIdsByName:Object, s_themeId:String, s_gameId:String,
    b_requestDownload:Boolean):void {
      if (b_requestDownload) {
        if (downloadFiles(o_componentThemeIdsByName, s_themeId, s_gameId)) {
          mu_state = ThemeComponentImagesStates.REQUESTING_IMAGE_DATA;
          return;
        }
      }
      
      createMaxLoader();
      
      for (var s_themeComponentName:String in o_componentThemeIdsByName) {
        var s_componentThemeId:String = o_componentThemeIdsByName[s_themeComponentName] as String;
        if (!s_componentThemeId) {
          trace("ThemeComponentImages.load. Warning: No theme ID found for theme component " +
            s_themeComponentName);
          continue;
        }
        
        if (imageExists(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName)) {
          continue;
        }
        
        var o_imageLoader:ImageLoader = createImageLoader(s_themeId, s_componentThemeId, s_gameId,
          s_themeComponentName);
        if (o_imageLoader) {
          mo_maxLoader.append(o_imageLoader);
        }
      }
      
      mo_maxLoader.load();
      
      mu_state = isLoading() ?
        ThemeComponentImagesStates.LOADING_IMAGE_FILES :
        ThemeComponentImagesStates.IDLE;
    }
    
    //==================================================================
    // state (get)
    //==================================================================
    public function get state():uint {
      return(mu_state);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      dispose();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createImageLoader
    //==================================================================
    private function createImageLoader(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):ImageLoader {
      var o_imagesPath:ThemeImagePath;
      var s_fetchingThemeId:String;
      
      if (CustomThemePath.IsCustom(s_componentThemeId)) {
        o_imagesPath = mo_customImagesPath;
        s_fetchingThemeId = s_themeId;
      } else {
        o_imagesPath  = mo_imagesPath;
        s_fetchingThemeId = s_componentThemeId;
      }
      
      var s_imageLoaderUrl:String = o_imagesPath.createFileUrl(s_fetchingThemeId, s_gameId,
        s_themeComponentName);
        
      if (!fileExists(s_imageLoaderUrl)) {
        s_imageLoaderUrl= o_imagesPath.createNonGameFileUrl(s_fetchingThemeId, s_themeComponentName);
        
        if (!fileExists(s_imageLoaderUrl)) {
          trace("ThemeComponentImages.createImageLoader. Warning: Unable to create loader for: " +
            "theme id: " + s_themeId +
            "; game id: " + s_gameId +
            "; theme compoonent: " + s_themeComponentName +
            "; compoonent theme id: " + s_componentThemeId);
          return(null);
        }
      }
      
      var o_vars:Object = {
        name: getImageKey(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName),
        noCache: true,
        
        //custom props
        gameId: s_gameId,
        themeId: s_themeId,
        themeComponentName: s_themeComponentName,
        themeComponentId: s_componentThemeId
      };
      
      return(new ImageLoader(s_imageLoaderUrl, o_vars));
    }
    
    //==================================================================
    // createMaxLoader
    //==================================================================
    private function createMaxLoader():void {
      if (mo_maxLoader) {
        //loader already created
        return;
      }
      
      var o_vars:Object = {
        onComplete: onLoaderMaxComplete,
        onError: onLoaderMaxError,
        onChildComplete: onLoaderMaxChildComplete,
        onChildFail: onLoaderMaxChildFail,
        onProgress: onLoaderMaxProgress
      };
      
      mo_maxLoader = new LoaderMax(o_vars);
    }
    
    //==================================================================
    // downloadFiles
    //==================================================================
    private function downloadFiles(o_componentThemeIdsByName:Object, s_themeId:String,
    s_gameId:String):Boolean {
      if (!mo_downloader.load(o_componentThemeIdsByName, s_themeId, s_gameId)) {
        return(false);
      }
      
      removeImages(o_componentThemeIdsByName, s_themeId, s_gameId);
      return(true);
    }
    
    //==================================================================
    // fileExists
    //==================================================================
    private function fileExists(s_url:String):Boolean {
      var o_file:File = new File(s_url);
      return(o_file.exists);
    }
    
    //==================================================================
    // getImageKey
    //==================================================================
    private function getImageKey(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):String {
      var s_key:String =
        resolveImageKeyComp(s_themeId) +
        resolveImageKeyComp(s_componentThemeId) +
        resolveImageKeyComp(s_gameId) +
        resolveImageKeyComp(s_themeComponentName);
      
      return(s_key);
    }
    
    //==================================================================
    // getImageKeyFromLoaderVars
    //==================================================================
    private function getImageKeyFromLoaderVars(o_loaderVars:Object):String {
      var o_block:FDataBlock = new FDataBlock(o_loaderVars);
      var s_imageKey:String = getImageKey(
        o_block.getString("themeId", ""),
        o_block.getString("themeComponentId", ""),
        o_block.getString("gameId", ""),
        o_block.getString("themeComponentName", ""));
      return(s_imageKey);
    }
    
    //==================================================================
    // isLoading
    //==================================================================
    private function isLoading():Boolean {
      for (var i_index:int = 0; i_index < mo_maxLoader.numChildren; i_index++) {
        var o_loader:LoaderCore = mo_maxLoader.getChildAt(i_index);
        if (o_loader.status == LoaderStatus.LOADING ||
        o_loader.status == LoaderStatus.READY) {
          return(true);
        }
      }
      
      return(false);
    }
    
    //==================================================================
    // onLoaderMaxChildComplete
    //==================================================================
    private function onLoaderMaxChildComplete(o_event:LoaderEvent):void {
      var o_loader:ImageLoader = o_event.target as ImageLoader;
      var s_imageKey:String = getImageKeyFromLoaderVars(o_loader.vars);
      if (s_imageKey) {
        mo_imagesByKey[s_imageKey] = o_loader.rawContent as Bitmap;
      }
    }
    
    //==================================================================
    // onLoaderMaxChildFail
    //==================================================================
    private function onLoaderMaxChildFail(o_event:LoaderEvent):void {
      var o_loader:ImageLoader = o_event.target as ImageLoader;
      var s_imageKey:String = getImageKeyFromLoaderVars(o_loader.vars);
      trace("ThemeComponentImages.onLoaderMaxChildFail: " + o_event + "; image key: " + s_imageKey);
    }
    
    //==================================================================
    // onLoaderMaxComplete
    //==================================================================
    private function onLoaderMaxComplete(o_event:LoaderEvent):void {
      mu_state = ThemeComponentImagesStates.IDLE;
      
      if (mo_callbacks) {
        mo_callbacks.themeComponentsImagesOnComplete(this);
      }
    }
    
    //==================================================================
    // onLoaderMaxError
    //==================================================================
    private function onLoaderMaxError(o_event:LoaderEvent):void {
      trace("ThemeComponentImages.onLoaderMaxError: " + o_event);
    }
    
    //==================================================================
    // onLoaderMaxProgress
    //==================================================================
    private function onLoaderMaxProgress(o_event:LoaderEvent):void {
      if (mo_callbacks) {
        mo_callbacks.themeComponentsImagesOnProgress(this, mo_maxLoader.progress);
      }
    }
    
    //==================================================================
    // removeImage
    //==================================================================
    private function removeImage(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):void {
      var s_key:String = getImageKey(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName);
      if (s_key in mo_imagesByKey) {
        delete mo_imagesByKey[s_key];
      }
      
      if (!mo_maxLoader) {
        return;
      }
      
      var o_loader:ImageLoader = mo_maxLoader.getLoader(s_key);
      if (o_loader) {
        mo_maxLoader.remove(o_loader);
        o_loader.dispose(true);
      }
    }
    
    //==================================================================
    // removeImages
    //==================================================================
    private function removeImages(o_componentThemeIdsByName:Object, s_themeId:String,
    s_gameId:String):void {
      for (var s_themeComponentName:String in o_componentThemeIdsByName) {
        var s_componentThemeId:String = o_componentThemeIdsByName[s_themeComponentName] as String;
        removeImage(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName);
      }
    }
    
    //==================================================================
    // resolveImageKeyComp
    //==================================================================
    private function resolveImageKeyComp(s_keyComp:String):String {
      return(s_keyComp || "");
    }
  }
}