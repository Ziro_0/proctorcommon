/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_ThemeComponentImagesCallbacks

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          01-10-2019
  History:
    01-10-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.themeComponentImages {
  import engine.common.utils.themeComponentImages.ThemeComponentImages;
  
  public interface I_ThemeComponentImagesCallbacks {
    function themeComponentsImagesOnComplete(o_images:ThemeComponentImages):void;
    function themeComponentsImagesOnProgress(o_images:ThemeComponentImages, n_progress:Number):void;
  }
}