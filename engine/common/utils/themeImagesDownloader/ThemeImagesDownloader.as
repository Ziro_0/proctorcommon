/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ThemeImagesDownloader

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-21-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.themeImagesDownloader {
  import engine.common.core.I_Engine;
  import engine.common.data.ClientMessageTypes;
  import engine.common.data.CustomThemePath;
  import engine.common.data.MessageProperties;
  import engine.common.data.messageKeys.RequestComponentThemeImageDataKeys;
  import engine.common.utils.ThemeImagePath;
  import flash.filesystem.File;

  public class ThemeImagesDownloader {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_ThemeImagesDownloaderCallbacks;
    private var mo_engine:I_Engine;
    private var mo_imagePath:ThemeImagePath;
    private var mo_customImagePath:ThemeImagePath;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function ThemeImagesDownloader(o_engine:I_Engine, s_baseThemesUrl:String,
    s_customBaseThemesUrl:String, s_fileExt:String = null) {
      mo_engine = o_engine;
      
      mo_imagePath = new ThemeImagePath(s_baseThemesUrl, mo_engine.app.deviceProfile.id, s_fileExt);
      mo_customImagePath = new ThemeImagePath(s_customBaseThemesUrl, mo_engine.app.deviceProfile.id,
        s_fileExt);
        
      mo_imagePath.isApplicationStorageMode = mo_customImagePath.isApplicationStorageMode = true;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_ThemeImagesDownloaderCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_ThemeImagesDownloaderCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // load
    //==================================================================
    public function load(o_componentThemeIdsByName:Object, s_themeId:String, s_gameId:String):Boolean {
      var o_message:Object = { };
      o_message[MessageProperties.TYPE] = ClientMessageTypes.REQUEST_COMPONENT_THEME_IMAGE_DATA;
      o_message[RequestComponentThemeImageDataKeys.DEVICE_PROFILE_ID] = mo_engine.app.deviceProfile.id;
      
      var ao_requests:Array = [];
      o_message[RequestComponentThemeImageDataKeys.REQUESTS] = ao_requests;
      
      for (var s_themeComponentName:String in o_componentThemeIdsByName) {
        var s_themeComponentId:String = o_componentThemeIdsByName[s_themeComponentName] as String;
        if (!s_themeComponentId) {
          trace("ThemeImagesDownloader.load. Warning: No theme ID found for theme component " +
            s_themeComponentName);
          continue;
        }
        
        var o_imagePath:ThemeImagePath = getThemeImagePath(s_themeComponentId);
        var s_fileUrl:String = o_imagePath.createGameOrNonGameFileUrl(s_themeId, s_gameId,
          s_themeComponentName);
        
        var o_request:Object = { };
        o_request[RequestComponentThemeImageDataKeys.REQ_THEME_ID] = s_themeId;
        o_request[RequestComponentThemeImageDataKeys.REQ_GAME_ID] = s_gameId;
        o_request[RequestComponentThemeImageDataKeys.REQ_THEME_COMPONENT_NAME] = s_themeComponentName;
        o_request[RequestComponentThemeImageDataKeys.REQ_FILE_SIZE] = getFileSize(s_fileUrl);
        
        ao_requests.push(o_request);
      }
      
      if (!ao_requests.length) {
        return(false);
      }
      
      mo_engine.sendMessageToServer(o_message);
      return(true);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      mo_engine = null;
      callbacks = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getFileSize
    //==================================================================
    private function getFileSize(s_fileUrl:String):Number {
      var o_file:File = new File(s_fileUrl);
      return(o_file.exists ? o_file.size : 0);
    }
    
    //==================================================================
    // getThemeImagePath
    //==================================================================
    private function getThemeImagePath(s_themeComponentId:String):ThemeImagePath {
      return(CustomThemePath.IsCustom(s_themeComponentId) ?
        mo_imagePath :
        mo_customImagePath);
    }
  }
}