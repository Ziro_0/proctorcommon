/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_ThemeImagesDownloaderCallbacks

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:          05-23-2019
  ActionScript:  3.0
  Description:  
  History:
    05-23-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.utils.themeImagesDownloader {
  
  public interface I_ThemeImagesDownloaderCallbacks {
    
  }
}