/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  AudioPlayer

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-14-2017
  ActionScript:	3.0
  Description:	Simple audio player with completion callback support.
  History:
    04-14-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.audio {
  import flash.events.Event;
  import flash.media.SoundChannel;
  import flash.utils.Dictionary;
  import flib.audio.FSound;

  public class AudioPlayer {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: SoundChannel
    //data: {
    //  id: uint (sound id)
    //  onComplete: Function
    //}
    private var mo_soundDataByChannel:Dictionary;
    
    //key: uint (sound id)
    //data: SoundChannel
    private var mo_soundChannelById:Dictionary;
    
    private var mu_nextId:uint;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function AudioPlayer():void {
      super();
      mo_soundDataByChannel = new Dictionary();
      mo_soundChannelById = new Dictionary();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // playSound
    //==================================================================
    public function playSound(o_soundSourceClsOrArray:Object, f_sndDone:Function = null,
    p_volume:Number = 1.0, i_loops:int = 0, n_startTime:Number = 0):uint {
      var o_channel:SoundChannel = FSound.Play(o_soundSourceClsOrArray, p_volume, 0, i_loops,
        onSoundComplete, n_startTime);
        
      if (!o_channel) {
        return(0);
      }
      
      var u_id:uint = ++mu_nextId;
      var o_data:Object = {
        id: u_id,
        onComplete: f_sndDone
      };
      
      mo_soundDataByChannel[o_channel] = o_data;
      mo_soundChannelById[u_id] = o_channel;
      return(u_id);
    }
    
    //==================================================================
    // stopAllSounds
    //==================================================================
    public function stopAllSounds():void {
      for (var o_key:Object in mo_soundChannelById) {
        stopSound(o_key as uint);
      }
    }
    
    //==================================================================
    // stopSound
    //==================================================================
    public function stopSound(u_soundId:uint):uint {
      if (!(u_soundId in mo_soundChannelById)) {
        return(0);
      }
      
      var o_channel:SoundChannel = mo_soundChannelById[u_soundId] as SoundChannel;
      FSound.Stop(o_channel);
      
      delete mo_soundDataByChannel[o_channel];
      delete mo_soundChannelById[u_soundId];
      return(0);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onSoundComplete
    //==================================================================
    private function onSoundComplete(o_event:Event):void {
      var o_channel:SoundChannel = o_event.currentTarget as SoundChannel;
      var o_data:Object = mo_soundDataByChannel[o_channel];
      delete mo_soundDataByChannel[o_channel];
      
      var u_id:uint = o_data.id as uint;
      delete mo_soundChannelById[u_id];
      
      var f_onComplete:Function = o_data.onComplete as Function;
      if (f_onComplete is Function) {
        f_onComplete(u_id);
      }
    }
  }
}