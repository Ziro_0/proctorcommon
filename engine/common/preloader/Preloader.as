/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  Preloader

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			04-17-2017
  ActionScript:	3.0
  Description:	
  History:
    04-17-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.preloader {
  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.ImageLoader;
  import com.greensock.loading.LoaderMax;
  import com.greensock.loading.MP3Loader;
  import com.greensock.loading.SWFLoader;
  import com.greensock.loading.XMLLoader;
  import flash.system.ApplicationDomain;
  import flash.system.LoaderContext;

  public class Preloader {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_PreloaderCallbacks;
    private var mo_loader:LoaderMax;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function Preloader(o_callbacks:I_PreloaderCallbacks = null):void {
      super();
      mo_callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // load
    //==================================================================
    public function load(vo_assets:Vector.<PreloaderAsset>):void {
      if (!vo_assets) {
        complete();
      }
      
      var o_vars:Object = {
        onComplete: onLoaderMaxComplete,
        onProgress: onLoaderMaxProgress,
        onError: onLoaderMaxError
      };
      
      mo_loader = new LoaderMax(o_vars);
      enqueueAssets(vo_assets);
      
      if (mo_loader.numChildren == 0) {
        complete();
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      mo_callbacks = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // completeCallback
    //==================================================================
    private function completeCallback(o_data:Object = null):void {
      if (mo_callbacks) {
        mo_callbacks.preloaderOnCallback(this, PreloaderActions.COMPLETE, o_data);
      }
    }
    
    //==================================================================
    // enqueueAssets
    //==================================================================
    private function enqueueAssets(vo_assets:Vector.<PreloaderAsset>):void {
      for each (var o_asset:PreloaderAsset in vo_assets) {
        if (!o_asset.url) {
          continue;
        }
        
        switch (o_asset.type) {
          case PreloaderAssetTypes.AUDIO:
            mo_loader.append(new MP3Loader(o_asset.url, {
              name: o_asset.name
            }));
            break;
          
          case PreloaderAssetTypes.IMAGE:
            mo_loader.append(new ImageLoader(o_asset.url, {
              name: o_asset.name,
              autoPlay: false
            }));
          
          case PreloaderAssetTypes.SWF:
            mo_loader.append(new SWFLoader(o_asset.url, {
              name: o_asset.name,
              autoPlay: false,
              context: new LoaderContext(false, ApplicationDomain.currentDomain, null)
            }));
          
          case PreloaderAssetTypes.Xml:
            mo_loader.append(new XMLLoader(o_asset.url, {
              name: o_asset.name
            }));
        }
      }
    }
    
    //==================================================================
    // errorCallback
    //==================================================================
    private function errorCallback(s_message:String):void {
      if (mo_callbacks) {
        mo_callbacks.preloaderOnCallback(this, PreloaderActions.ERROR, s_message);
      }
    }
    
    //==================================================================
    // onLoaderMaxComplete
    //==================================================================
    private function onLoaderMaxComplete(o_error:LoaderEvent):void {
      complete();
    }
    
    //==================================================================
    // onLoaderMaxError
    //==================================================================
    private function onLoaderMaxError(o_error:LoaderEvent):void {
      errorCallback(o_error.text);
    }
    
    //==================================================================
    // onLoaderMaxProgress
    //==================================================================
    private function onLoaderMaxProgress(o_error:LoaderEvent):void {
      progressCallback();
    }
    
    //==================================================================
    // progressCallback
    //==================================================================
    private function progressCallback():void {
      if (mo_callbacks) {
        mo_callbacks.preloaderOnCallback(this, PreloaderActions.PROGRESS, mo_loader.progress);
      }
    }
  }
}