/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ServerMessageTypes

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-04-2018
  ActionScript: 3.0
  Description:  Message types of all messages that the server sends to clients.
  History:
    04-04-2018: Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {
  
  public final class ServerMessageTypes {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /**
     * The active game ID has changed. The type of game has been set or cleared.
     * @see GameIdChangedKeys for the format of the data object.
     */
    public static const ACTIVE_GAME_ID_CHANGED:String = "activeGameIdChanged";
    
    /**
     * A new client has joined the game. All clients exept the client joining will receive this message.
     * @see ClientJoinedKeys for the format of the data object.
     */
    public static const CLIENT_JOINED:String = "clientJoined";
    
    /**
     * A client has left the game. All clients exept the client joining will receive this message.
     * @see ClientLeftKeys for the format of the data object.
     */
    public static const CLIENT_LEFT:String = "clientLeft";
    
    /**
     * The server is sending component theme images to a client.
     * @see ComponentThemeImageKeys for the format of the data object.
     */
    public static const COMPONENT_THEME_IMAGES:String = "componentThemeImages";
    
    /**
     * The admin has set a new entry for the active game. This data for this message depends on the active game.
     * @see GameSetEntryKeys for the format of the data object, and the how this message is used for each game.
     */
    public static const GAME_SET_ENTRY:String = "gameSetEntry";
    
    /**
     * State of the game has changed. It has either started or stopped.
     * @see GameStateChangedKeys for the data of each object.
     */
    public static const GAME_STATE_CHANGED:String = "gameStateChanged";
    
    /**
     * The game round timer has been updated.
     * @see RoundTimerUpdateKeys for the format of the data object.
     */
    public static const ROUND_TIMER_UPDATE:String = "roundTimerUpdate";
    
    /**
     * The game round has ended and the winner has been determined.
     * @see RoundWinnerKeys for the format of the data object.
     */
    public static const ROUND_WINNER:String = "roundWinner";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}