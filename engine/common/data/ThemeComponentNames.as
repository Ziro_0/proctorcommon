/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ThemeComponentNames

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          12-20-2018
  ActionScript:  3.0
  Description:  
  History:
    12-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {

  public final class ThemeComponentNames {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const BACK:String = "back";
    public static const BANNER:String = "banner";
    public static const PANEL:String = "panel";
    public static const RADIO_BUTTON_OFF:String = "radioButtonOff";
    public static const RADIO_BUTTON_ON:String = "radioButtonOn";
    public static const TITLE:String = "title";
    public static const LOBBY_NO_GAME_SELECTED:String = "lobbyNoGameSelected";
    public static const LOBBY_BABY_GAME_SELECTED:String = "lobbyBabyGameSelected";
    public static const LOBBY_BRIDAL_GAME_SELECTED:String = "lobbyBridalGameSelected";
    public static const RESULTS_BACK:String = "resultsBack";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // All
    //==================================================================
    public static function All():Vector.<String> {
      return(new <String>[
        BACK,
        BANNER,
        PANEL,
        RADIO_BUTTON_OFF,
        RADIO_BUTTON_ON,
        TITLE,
        LOBBY_NO_GAME_SELECTED,
        LOBBY_BABY_GAME_SELECTED,
        LOBBY_BRIDAL_GAME_SELECTED,
        RESULTS_BACK
      ]);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}