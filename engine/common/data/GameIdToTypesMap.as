/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  GameIdToTypesMap

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          03-23-2019
  ActionScript:  3.0
  Description:  
  History:
    03-23-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {

  public final class GameIdToTypesMap {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (game id)
    //data: String (game type)
    private static var smo_gameTypesById:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // GetGameTypeFromId
    //==================================================================
    public static function GetGameTypeFromId(s_gameId:String):String {
      if (!smo_gameTypesById) {
        InitMap();
      }
      
      return(smo_gameTypesById[s_gameId] as String);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // InitMap
    //==================================================================
    private static function InitMap():void {
      smo_gameTypesById = { };
      smo_gameTypesById[GameIds.FILL_IN_BLANK_BABY] = GameTypes.FILL_IN_BLANK;
      smo_gameTypesById[GameIds.FILL_IN_BLANK_BRIDAL] = GameTypes.FILL_IN_BLANK;
      smo_gameTypesById[GameIds.LETTERS_BABY] = GameTypes.LETTERS;
      smo_gameTypesById[GameIds.LETTERS_BRIDAL] = GameTypes.LETTERS;
      smo_gameTypesById[GameIds.MULTIPLE_CHOICE_BABY] = GameTypes.MULTIPLE_CHOICE;
      smo_gameTypesById[GameIds.MULTIPLE_CHOICE_BRIDAL] = GameTypes.MULTIPLE_CHOICE;
      smo_gameTypesById[GameIds.UNSCRAMBLE_BABY] = GameTypes.UNSCRAMBLE;
      smo_gameTypesById[GameIds.UNSCRAMBLE_BRIDAL] = GameTypes.UNSCRAMBLE;
    }
  }
}