/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientMessageTypes

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-04-2018
  ActionScript:  3.0
  Description:  Message types of all messages that clients server sends to the server.
  History:
    04-04-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {
  
  public final class ClientMessageTypes {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /**
     * Client is an observer and requested to join a game as an active player.
     */
    public static const JOIN:String = "join";
    
    /**
     * Client has moved onto the next round (game entry).
     * @see ClientNextKeys for the format of the data.
     */
    public static const NEXT:String = "next";
    
    /**
     * Client is ready to start game. When the server has received this message from all clients,
     * it will start the game.
     */
    public static const READY:String = "ready";
    
    /**
     * Client is requesting a component theme image data from the server.
     * @see RequestComponentThemeImageDataKeys for the format of the data.
     */
    public static const REQUEST_COMPONENT_THEME_IMAGE_DATA:String = "requestComponentThemeImageData";
    
    /**
     * Client is an active player and requested to leave play and watch as an observer.
     */
    public static const WATCH:String = "watch";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}