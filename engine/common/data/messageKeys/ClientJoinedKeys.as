/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientJoinedKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-17-2018
  ActionScript:  3.0
  Description:  
  History:
    04-17-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {
  import flib.utils.FDataBlock;
  
  public final class ClientJoinedKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * String. Key of the client that joined.
     */
    public static const CLIENT_ID:String = "clientId";
    
    /*
     * String. The name of the client that joined.
     */
    public static const NAME:String = "name";
    
    /*
     * FDataObject. Player state of the client that joined. This data is sent all clients, except the client
     * that joined. This is because there is sensitive data that only the joning client should see.
     */
    public static const OTHER_PLAYER_STATE:String = "otherPlayerState";
    
    /*
     * FDataObject. Player state of the client that joined. This data is only sent to the client that joined;
     * all other clients do not receive this.
     */
    public static const PLAYER_STATE:String = "playerState";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}