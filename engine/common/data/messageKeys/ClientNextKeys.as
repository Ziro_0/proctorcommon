/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ClientNextKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          09-03-2018
  ActionScript:  3.0
  Description:  
  History:
    09-03-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {
  import flib.utils.FDataBlock;
  
  public final class ClientNextKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * String. Key of the client that moved to the next round.
     */
    public static const CLIENT_ID:String = "clientId";
    
    /*
     * Object. Contains the client's response to the round. The format of the object depends on the active game.
     * unscramble:
     *  - String. The unscrambled word the client used.
     * 
     * fill-in-blank:
     *  - String. The word the client used to fill in the blank.
     * 
     * letters:
     *  - Array of String. The words the client used for each letter of the presenter's word.
     * 
     * multiple-choice:
     *  - int. The zero-based index of the choice that the client selected.
     */
    public static const RESPONSE:String = "response";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}