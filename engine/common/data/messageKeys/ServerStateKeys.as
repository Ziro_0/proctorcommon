/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ServerStateKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-04-2018
  ActionScript:  3.0
  Description:  
  History:
    04-04-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {
  
  public class ServerStateKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /**
     * String. Id of the currently active game. Null if no game is active.
     */
    public static const ACTIVE_GAME_ID:String = "activeGameId";
    
    /**
     * String. Title of the active game, or null if no game is active.
     */
    //public static const ACTIVE_GAME_TITLE:String = "activeGameTitle";
    
    /**
     * Object. Specific settings of the currently active mini-game.
     */
    public static const ACTIVE_GAME_SETTINGS:String = "activeGameSettings";
    
    /**
     * Boolean. True if a game has started.
     */
    public static const IS_GAME_STARTED:String = "isGameStarted";
    
    /**
     * FDataBlock of key/values of the state objects of each player connected to the server.
     * @param key: string. client id
     * @param data: FDataBlock. Player state object for each player in the game.
     * @see PlayerStateKeys for the keys that represent the data for each player state object.
     */
    public static const PLAYERS:String = "players";
    
    /**
     * String. The ID of the currently active theme,
     */
    public static const THEME_ID:String = "themeId";
    
    /**
     * Object of key/values of the theme components of the active theme selected by the server.
     * Key: String (component name)
     * Data: String (component theme id)
     */
    public static const THEME_IDS_BY_COMP_NAME:String = "themeIdsByCompName";
    
    /**
     * Boolean that is true when a game is first started, and the server is waiting for all
     * players to acknowledge that they are ready to proceed with game play (and countdown).
     */
    public static const WAITING_FOR_PLAYERS:String = "waitingForPlayers";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}