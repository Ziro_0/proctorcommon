/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  PlayerStateKeys

  Author:       Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:         04-03-2018
  ActionScript: 3.0
  Description:  Property keys for the player state data (not including the dealer)
  History:
    04-03-2018: Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {
  
  public final class PlayerStateKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /**
     * PlayerGameProgress. Specifies the current progress of the player during an active game.
     * Format:
     * - round. uint. Zero-based index of the current round the player is on.
     * - entry. Object. The format of this object depends on the current mini-game active.
     *    - unscramble:
     *      - String. The unscrambled word the client used.
     * 
     *    - fill-in-blank:
     *      - String. The word the client used to fill in the blank.
     * 
     *    - letters:
     *      - Array of String. The words the client used for each letter of the presenter's word.
     */
    public static const GAME_PROGRESS:String = "gameProgress";
    
    /**
     * uint. Specifies one of the values in PlayerStatuses.
     */
    public static const STATUS:String = "status";
    
    /**
     * UserData. Player's user data.
     */
    public static const USER_DATA:String = "userData";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}