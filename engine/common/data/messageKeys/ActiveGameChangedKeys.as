/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  ActiveGameChangedKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-26-2018
  ActionScript:  3.0
  Description:  
  History:
    05-26-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {

  public final class ActiveGameChangedKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * String. Id of the active game, or null if no active game has been set
     */
    public static const GAME_ID:String = "gameId";
    
    /**
     * Object. Specific settings of the current mini-game.
     */
    public static const GAME_SETTINGS:String = "gameSettings";
    
    /*
     * String. The title of the the active game, or null of no active game has been set.
     */
    //public static const TITLE:String = "title";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}