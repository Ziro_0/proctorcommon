/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  RequestComponentThemeImageDataKeys

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-23-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {

  public final class RequestComponentThemeImageDataKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /**
     * String. Specifies the device profile id of the client device requesting the theme image data.
     */
    public static const DEVICE_PROFILE_ID:String = "deviceProfileId";
    
    /**
     * String. Ifthe file already exists on the client device, this specifies the file's size. The server
     * uses this to compare the specified size to its own local file size. If they differ, the server will
     * send image data back about this file to the client. If they are the same, the server assumes the
     * its local file and the client's file are the same, and will not send image data back.
     */
    public static const REQ_FILE_SIZE:String = "fileSize";
    
    /**
     * String. Specifies the game id of the requested component theme image data.
     */
    public static const REQ_GAME_ID:String = "gameId";
    
    /**
     * String. Specifies the name of the theme component of the requested component theme image data.
     */
    public static const REQ_THEME_COMPONENT_NAME:String = "themeComponentName";
    
    /**
     * String. Specifies the theme id of the requested component theme image data.
     */
    public static const REQ_THEME_ID:String = "themeId";
    
    /**
     * Array. Specifies all the image files the client is requesting. Each element is an object whose
     * properties are are named beginning with the "REQ_xxx" keys.
     */
    public static const REQUESTS:String = "requests";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}