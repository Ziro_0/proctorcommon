/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  RoundTimerUpdateKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-01-2018
  ActionScript:  3.0
  Description:  
  History:
    06-01-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {

  public final class RoundTimerUpdateKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * uint. Number of seconds elapsed since the timer started.
     */
    public static const ELAPSED:String = "elapsed";
    
    /*
     * uint. Max number of seconds on the timer
     */
    public static const MAX:String = "max";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}