/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  GameSetEntryKeys

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-29-2018
  ActionScript:  3.0
  Description:  
  History:
    05-29-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data.messageKeys {

  public final class GameSetEntryKeys {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * Object. The entry data being sent to the client game. The format of the data depends on the currently
     * active game.
     * 
     * -  [unscramble subgame engines]
     *    {
     *      index: uint. The zero-based nth entry that has been presented.
     *      word: String. The word to present to the client.
     *      scramble: String. The scramble word to present to the client.
     *    }
     * 
     * -  [multiple choice subgame engines]
     *    {
     *      index: uint. The zero-based nth entry that has been presented.
     *      question: String. The question to present to the client.
     *      choices: Array. The array of choices to present.
     *    }
     * 
     * -  [letters subgame engines]
     *    {
     *      index: uint. The zero-based nth entry that has been presented.
     *      word: String. The word to present to the client.
     *    }
     * 
     * -  [fill in blank subgame engines]
     *    {
     *      index: uint. The zero-based nth entry that has been presented.
     *      question: String. The question to present to the client.
     *      answer: String. The answer to the question.
     *    }
     */
    public static const DATA:String = "data";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}