/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  GameIds

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-10-2018
  ActionScript:  3.0
  Description:  
  History:
    05-10-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {
  
  public final class GameIds {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const FILL_IN_BLANK_BRIDAL:String = "fill-in-blank-bridal";
    public static const FILL_IN_BLANK_BABY:String = "fill-in-blank-baby";
    public static const LETTERS_BABY:String = "letters-baby";
    public static const LETTERS_BRIDAL:String = "letters-bridal";
    public static const MULTIPLE_CHOICE_BABY:String = "multiple-choice-baby";
    public static const MULTIPLE_CHOICE_BRIDAL:String = "multiple-choice-bridal";
    public static const UNSCRAMBLE_BRIDAL:String = "unscramble-bridal";
    public static const UNSCRAMBLE_BABY:String = "unscramble-baby";
    
    public static const ALL:Array = [
      UNSCRAMBLE_BRIDAL,
      UNSCRAMBLE_BABY,
      MULTIPLE_CHOICE_BABY,
      MULTIPLE_CHOICE_BRIDAL,
      LETTERS_BABY,
      LETTERS_BRIDAL,
      FILL_IN_BLANK_BRIDAL,
      FILL_IN_BLANK_BABY
    ];
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}