/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  PlayerStatuses

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-20-2018
  ActionScript:  3.0
  Description:  
  History:
    04-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.common.data {
  public final class PlayerStatuses {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
     * Placeholder value.
     */
    public static const NONE:uint = 0;
    
    /*
     * Player is an observer.
     */
    public static const OBSERVER:uint = 1;
    
    /*
     * Player is an active player.
     */
    public static const ACTIVE:uint = 2;
    
    /*
     * Player is in process of joining game.
     */
    public static const JOINING:uint = 3;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}